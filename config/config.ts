// config/config.ts
require('dotenv').config();

const config = {
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    pool: {
      min: 0,
      max: 4,
    },
    host: process.env.DB_HOST,
    port: process.env.DB_PORT || 3306,
    certDomain: process.env.CERT_DOMAIN,
    dialect: 'mysql',
    dialectOptions: {
      charset: 'utf8mb4',
    },
    logging: false,
  },
  qa: {
    username: process.env.DB_USERNAME_QA,
    password: process.env.DB_PASSWORD_QA,
    database: process.env.DB_NAME_QA,
    pool: {
      min: 0,
      max: 4,
    },
    host: process.env.DB_HOST_QA,
    port: process.env.DB_PORT_QA || 3306,
    certDomain: process.env.CERT_DOMAIN_QA,
    dialect: 'mysql',
    dialectOptions: {
      charset: 'utf8mb4',
    },
    logging: false,
  },
  development: {
    username: process.env.DB_USERNAME_DEV,
    password: process.env.DB_PASSWORD_DEV,
    database: process.env.DB_NAME_DEV,
    pool: {
      min: 0,
      max: 4,
    },
    host: process.env.DB_HOST_DEV,
    port: process.env.DB_PORT_DEV || 3306,
    certDomain: process.env.CERT_DOMAIN,
    dialect: 'mysql',
    dialectOptions: {
      charset: 'utf8mb4',
    },
    logging: false,
  },
  test: {
    username: process.env.DB_USERNAME_TEST,
    password: process.env.DB_PASSWORD_TEST,
    database: process.env.DB_NAME_TEST,
    pool: {
      min: 0,
      max: 4,
    },
    host: process.env.DB_HOST_TEST,
    port: process.env.DB_PORT_TEST || 3306,
    certDomain: process.env.CERT_DOMAIN_TEST,
    dialect: 'mysql',
    dialectOptions: {
      charset: 'utf8mb4',
    },
    logging: false,
  },
};

// For ES6/TypeScript
export default config;

// For Sequelize
module.exports = config;
