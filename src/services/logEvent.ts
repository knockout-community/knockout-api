import knex from './knex';

export default class LogEvent {
  static ENTITY_POST = 'post';

  static ENTITY_THREAD = 'thread';

  static ENTITY_RATING = 'rating';

  static ACTION_CREATE = 'create';

  static ACTION_UPDATE = 'update';

  static ACTION_DELETE = 'delete';

  private entity: string;

  private action: string;

  private success: boolean = false;

  private message: string | null = null;

  private properties: object | null = null;

  private userId: number | null = null;

  constructor(entity: string, action: string) {
    this.entity = entity;
    this.action = action;
  }

  public setSuccess(success: boolean) {
    this.success = success;
    return this;
  }

  public setMessage(message: string) {
    this.message = message;
    return this;
  }

  public setProperties(properties: object) {
    this.properties = properties;
    return this;
  }

  public setUserId(userId: number) {
    this.userId = userId;
    return this;
  }

  public async log() {
    try {
      await knex('LogEvents').insert({
        entity: this.entity,
        action: this.action,
        success: this.success,
        message: this.message,
        properties: JSON.stringify(this.properties),
        user_id: this.userId,
      });
    } catch (e) {
      console.log(e);
    }
  }
}
