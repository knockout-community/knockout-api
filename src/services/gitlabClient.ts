import { Gitlab } from '@gitbeaker/node';

const gitlabClient = new Gitlab({
  token: process.env.GITLAB_TOKEN,
});

export default gitlabClient;
