import { Redis } from 'ioredis';

import { REDIS_PORT, REDIS_HOST, REDIS_PASSWORD } from '../../config/server';

const PORT = Number(REDIS_PORT);
const HOST = REDIS_HOST;
const PASSWORD = REDIS_PASSWORD;

const client = new Redis({
  host: HOST,
  port: PORT,
  password: PASSWORD || undefined,
  enableAutoPipelining: true,
});

client.on('connect', () => {
  console.log('=========== Redis is connected ===========');
});

client.on('error', (err) => {
  console.log(`Redis Error: ${err}`);
});

export default client;
