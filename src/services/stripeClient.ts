import Stripe from 'stripe';
import { STRIPE_SECRET_KEY } from '../../config/server';

const stripeClient = new Stripe(STRIPE_SECRET_KEY!, {
  apiVersion: '2020-08-27',
});

export default stripeClient;
