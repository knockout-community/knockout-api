import { Client } from 'minio';
import { S3_ENDPOINT, S3_ACCESS_KEY, S3_SECRET_KEY } from '../../config/server';

const s3Client =
  process.env.NODE_ENV === 'production'
    ? new Client({
        endPoint: S3_ENDPOINT!,
        accessKey: S3_ACCESS_KEY!,
        secretKey: S3_SECRET_KEY!,
      })
    : null;

export default s3Client;
