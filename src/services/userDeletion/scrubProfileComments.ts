import ProfileComment from '../../models/profileComment';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Scrubbing ProfileComments...', userId);
    await ProfileComment.update(
      {
        content: 'This profile comment is from a deleted user and is no longer directly available.',
      },
      { where: { author: userId } }
    );
  } catch (e) {
    logError('ProfileComments', userId, e.message);
  }
};
