import IpAddress from '../../models/ipAddress';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Deleting IpAddresses...', userId);
    await IpAddress.destroy({ where: { user_id: userId } });
  } catch (e) {
    logError('IpAddresses', userId, e.message);
  }
};
