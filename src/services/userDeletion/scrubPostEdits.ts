import DELETED_USER_ID from '../../constants/deletedUser';
import PostEdit from '../../models/postEdit';
import { logError, logInfo } from './log';

export const wipePostEdits = async (userId: number) => {
  try {
    logInfo('Scrubbing PostEdits...', userId);
    await PostEdit.update(
      {
        content: "This post edit's content has been removed by a moderator.",
        editReason: null,
      },
      { where: { userId } }
    );
  } catch (e) {
    logError('PostEdits', userId, e.message);
  }
};

export default async (userId: number) => {
  try {
    logInfo('Anonymizing PostEdits...', userId);
    await PostEdit.update(
      {
        userId: DELETED_USER_ID,
        editReason: null,
      },
      { where: { userId } }
    );
  } catch (e) {
    logError('PostEdits', userId, e.message);
  }
};
