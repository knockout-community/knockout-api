import Message from '../../models/message';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Scrubbing Messages...', userId);
    await Message.update(
      {
        content: 'This message is from a deleted user and is no longer available.',
      },
      { where: { user_id: userId } }
    );
  } catch (e) {
    logError('Messages', userId, e.message);
  }
};
