import DELETED_USER_ID from '../../constants/deletedUser';
import Post from '../../models/post';
import sequelize from '../../models/sequelize';
import { logError, logInfo } from './log';

export const wipePosts = async (userId: number) => {
  try {
    logInfo('Scrubbing Posts...', userId);
    await Post.update(
      { content: "This post's content has been removed by a moderator." },
      { where: { user_id: userId } }
    );

    await sequelize.query(
      `
    DELETE pr FROM PostResponses pr
    INNER JOIN Posts p ON p.id = pr.response_post_id
    WHERE p.user_id = :userId
`,
      { replacements: { userId } }
    );
  } catch (e) {
    logError('Posts', userId, e.message);
  }
};

export default async (userId: number) => {
  try {
    logInfo('Anonymizing Posts...', userId);
    await Post.update(
      { user_id: DELETED_USER_ID, country_name: null, app_name: null },
      { where: { user_id: userId } }
    );
  } catch (e) {
    logError('Posts', userId, e.message);
  }
};
