import ExternalAccount from '../../models/externalAccount';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Deleting ExternalAccounts...', userId);
    await ExternalAccount.destroy({ where: { userId } });
  } catch (e) {
    logError('ExternalAccounts', userId, e.message);
  }
};
