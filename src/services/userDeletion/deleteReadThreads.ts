import ReadThread from '../../models/readThread';
import { logError, logInfo } from './log';

export default async (userId: number) => {
  try {
    logInfo('Deleting ReadThreads...', userId);
    await ReadThread.destroy({ where: { user_id: userId } });
  } catch (e) {
    logError('ReadThreads', userId, e.message);
  }
};
