/* eslint-disable class-methods-use-this */
import { Request } from 'express';
import {
  JsonController,
  UseBefore,
  Req,
  Post,
  Param,
  Delete,
  Get,
  HttpCode,
} from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { OpenAPIParam, Thread } from 'knockout-schema';
import httpStatus from 'http-status';
import { authentication } from '../middleware/auth';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import UserRetriever from '../retriever/user';
import { userHiddenThreadIds } from '../helpers/user';
import HiddenThread from '../models/hiddenThread';

@OpenAPI({ tags: ['HiddenThreads'] })
@JsonController('/hidden-threads')
export default class HiddenThreadController {
  @Post('/:id')
  @HttpCode(httpStatus.CREATED)
  @UseBefore(authentication.required)
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  async hideThread(@Param('id') id: number, @Req() request: Request) {
    await HiddenThread.upsert({
      userId: request.user!.id,
      threadId: id,
    });

    await new UserRetriever(request.user!.id).invalidateHiddenThreads();

    return { message: 'Thread hidden.' };
  }

  @Get('/')
  @UseBefore(authentication.required)
  @ResponseSchema(Thread, { isArray: true })
  async getHiddenThreads(@Req() request: Request) {
    const hiddenThreadIds = await userHiddenThreadIds(request.user!.id);

    const threads = await new ThreadRetriever(hiddenThreadIds, [
      ThreadFlag.RETRIEVE_SHALLOW,
    ]).getObjectArray();

    return threads;
  }

  @Delete('/:id')
  @UseBefore(authentication.required)
  @OpenAPIParam('id', { description: 'The id of the thread.' })
  async unhideThread(@Param('id') id: number, @Req() request: Request) {
    await HiddenThread.destroy({
      where: {
        userId: request.user!.id,
        threadId: id,
      },
    });

    await new UserRetriever(request.user!.id).invalidateHiddenThreads();

    return { message: 'Thread unhidden.' };
  }
}
