import { POST_CHARACTER_LIMIT } from 'knockout-schema';
import { Transaction } from 'sequelize';
import validateLength from './validateLength';
import Thread from '../models/thread';

/**
 * @returns validation passed?
 */
const validatePostLength = (content) => validateLength(content, POST_CHARACTER_LIMIT);

/**
 * async function (requires await) that returns a bool if a thread can be posted on
 * @returns validation passed?
 */
const validateThreadStatus = async (threadId: number, transaction?: Transaction) => {
  if (!threadId) {
    return false;
  }

  try {
    const thread = await Thread.findOne({
      attributes: ['locked', 'deletedAt'],
      where: { id: threadId },
      transaction,
    });

    return thread && !thread.locked && !thread.deletedAt;
  } catch (error) {
    console.log(error);

    return false;
  }
};

const VALID_APPS = ['knockout.chat', 'lite.knockout.chat', 'kopunch', 'knocky'];

const validateAppName = (clientName: string) => {
  if (VALID_APPS.includes(clientName)) {
    return true;
  }

  return false;
};

export { validatePostLength, validateThreadStatus, validateAppName };
