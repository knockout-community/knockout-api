import { MESSAGE_CHARACTER_LIMIT } from 'knockout-schema';
import validateLength from './validateLength';

/**
 * @returns validation passed?
 */
const validateMessageLength = (content) => validateLength(content, MESSAGE_CHARACTER_LIMIT);

export default validateMessageLength;
