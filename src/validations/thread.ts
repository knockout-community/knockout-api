const validateThreadTitleLength = (title: string) => {
  if (title.length < 3) {
    return false;
  }
  if (title.length > 140) {
    return false;
  }

  const escaped = title.replace(/[\n\r\t]/g, '').replace(' ', '');

  if (escaped.length < 3) {
    return false;
  }
  return true;
};

const validateThreadTagCount = tagIds => {
  if(!tagIds) {
    return true;
  }
  return tagIds.length <= 3
};

export { validateThreadTitleLength, validateThreadTagCount };
