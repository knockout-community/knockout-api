/* eslint-disable import/prefer-default-export */

import { NextFunction, Request, Response } from 'express';
import { ForbiddenError, UnauthorizedError } from 'routing-controllers';
import ms from 'ms';
import { PermissionCode } from 'knockout-schema';
import { Op } from 'sequelize';
import { RAID_MODE_KEY } from '../constants/adminSettings';
import authorize from '../helpers/authorize';

import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import LogEvent from '../services/logEvent';
import redis from '../services/redisClient';
import Ripe from '../services/ripe';
import Scamalytics from '../services/scamalytics';
import { isLimitedUser } from '../validations/user';
import UserRetriever from '../retriever/user';
import { RoleCode } from '../helpers/permissions';
import BanRetriever from '../retriever/ban';
import Post from '../models/post';
import Ban from '../models/ban';

export const getPostsAndCount = async (req: Request, res: Response, next: NextFunction) => {
  const thread: any = await new ThreadRetriever(req.params.id, [
    ThreadFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();

  const { subforumId, user: threadUserId } = thread;

  // users must have -view permission if the thread isn't their own
  if (threadUserId !== req.user?.id) {
    await authorize(req.user?.id, [`subforum-${subforumId}-view`]);
  }

  // user must have permission to view if the thread is deleted
  if (thread.deleted) {
    await authorize(req.user?.id, [`subforum-${subforumId}-view-deleted-threads`]);
  }

  return next();
};

export const store = async (req: Request, res: Response, next: NextFunction) => {
  if (!req.user) {
    throw new UnauthorizedError('User is unauthorized.');
  }

  const limitedUser = await isLimitedUser(req.user?.id);

  const logEvent = new LogEvent(LogEvent.ENTITY_POST, LogEvent.ACTION_CREATE)
    .setUserId(req.user?.id)
    .setProperties({ ip: req.ipInfo })
    .setSuccess(false);

  // if the user is limited:
  // - check IP
  // - if raid mode, prevent from posting
  // - if last post made within 5 mins, prevent from posting
  if (limitedUser) {
    if (await new Scamalytics(req.ipInfo).isRisky()) {
      logEvent.setMessage('VPN detected').log();
      throw new ForbiddenError('You appear to be using a proxy/VPN.');
    }

    // Do we have a ban in place for their netname or parent ASN?
    if ((await new Ripe(req.ipInfo).isBanned()) || (await new Ripe(req.ipInfo).isParentBanned())) {
      logEvent.setMessage('Banned ISP detected').log();
      throw new ForbiddenError('You appear to be using an ISP which we have banned.');
    }

    const raidModeEnabled = await redis.get(RAID_MODE_KEY);

    if (raidModeEnabled) {
      throw new ForbiddenError('Our servers are under heavy load, try posting again later.');
    }

    const recentPost = await Post.findOne({
      attributes: ['created_at'],
      where: { user_id: req.user?.id },
      order: [['created_at', 'DESC']],
    });

    if (recentPost) {
      const now = new Date();
      const { createdAt } = recentPost;
      const diff = now.getTime() - createdAt.getTime();

      if (diff < ms('5 minutes')) {
        throw new ForbiddenError(`Please wait ${ms(ms('5 minutes') - diff)} before posting again.`);
      }
    }
  }

  await authorize(req.user?.id, [`subforum-${req.body.subforum_id}-thread-create`]);

  // if user has an active ban when creating this thread, and the ban has appeal_thread_created set,
  // prevent them from making another thread; otherwise set the flag to true to mark the ban as appealed
  const isBannedUser =
    (await new UserRetriever(req.user?.id).getSingleObject()).role.code === RoleCode.BANNED_USER;
  if (isBannedUser) {
    const activeBan = await Ban.findOne({
      attributes: ['id', 'appealThreadCreated'],
      where: { user_id: req.user?.id, expires_at: { [Op.gt]: new Date() } },
      order: [['created_at', 'desc']],
    });
    if (activeBan?.appealThreadCreated) {
      throw new ForbiddenError('You can only make one appeal thread per active mute.');
    } else if (activeBan) {
      await activeBan.update({ appealThreadCreated: true });
      await new BanRetriever(activeBan.id).invalidate();
    }
  }

  if (req.body.background_url || req.body.background_type) {
    // if the user has the subforum thread-background-update permission, let them add a bg
    await authorize(req.user?.id, [`subforum-${req.body.subforum_id}-thread-background-update`]);
  }

  logEvent.setSuccess(true).log();

  return next();
};

export const update = async (req: Request, res: Response, next: NextFunction) => {
  const threadId = req.params?.id || req.body.id;
  const thread: any = await new ThreadRetriever(threadId, [
    ThreadFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();
  const { subforumId, user: threadUserId } = thread;

  const isOwnThread = threadUserId === req.user?.id;

  // user must still be able to create threads as well (e.g. banned users trying to update their thread titles)
  const requiredPermissionCodes: PermissionCode[] = [`subforum-${subforumId}-thread-create`];

  // if the thread does not belong to the user, see if user has subforum
  // thread edit permission
  if (!isOwnThread) {
    requiredPermissionCodes.push(`subforum-${subforumId}-thread-update`);
  }

  // if the request includes a background, validate they have the subforum thread-background-update permission
  if (req.body.background_url || req.body.background_type) {
    requiredPermissionCodes.push(`subforum-${subforumId}-thread-background-update`);
  }

  // if the user is moving the thread, they must have the subforum thread-move permission
  if (req.body.subforum_id) {
    requiredPermissionCodes.push(`subforum-${subforumId}-thread-move`);
  }

  await authorize(req.user?.id, requiredPermissionCodes);
  return next();
};

export const updateTags = async (req: Request, res: Response, next: NextFunction) => {
  const threadId = req.params?.id || req.body.threadId;
  const thread: any = await new ThreadRetriever(threadId, [
    ThreadFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();
  const { subforumId } = thread;

  await authorize(req.user?.id, [`subforum-${subforumId}-thread-update`]);

  return next();
};
