/* eslint-disable import/prefer-default-export */
import { NextFunction, Request, Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import ms from 'ms';
import { Op } from 'sequelize';
import { getBannedUserRoleId, getLimitedUserRoleId } from '../helpers/role';
import authorize from '../helpers/authorize';
import UserRetriever from '../retriever/user';
import Ban from '../models/ban';
import PreviousUserRole from '../models/previousUserRole';

export const update = async (req: Request, res: Response, next: NextFunction) => {
  if (Number(req.params.id) !== req.user?.id || req.user.role_id === (await getBannedUserRoleId()))
    throw new ForbiddenError();
  return next();
};

export const deleteAccount = async (req: Request, res: Response, next: NextFunction) => {
  if (
    Number(req.user?.id) !== Number(req.params.id) ||
    (req.user && Date.now() - req.user.createdAt!.getTime() < ms('2 days'))
  ) {
    throw new ForbiddenError();
  }

  if (req.user?.role_id === (await getBannedUserRoleId())) {
    const userPermabanCount = await Ban.count({
      where: {
        expires_at: { [Op.gt]: new Date(2090, 1, 1) },
        user_id: req.user.id,
      },
    });

    if (userPermabanCount > 0) {
      throw new ForbiddenError();
    }
  }
  return next();
};

export const wipeAccount = async (req: Request, res: Response, next: NextFunction) => {
  // if the user is trying to wipe their own account, throw forbidden
  if (Number(req.user?.id) === Number(req.params.id)) {
    throw new ForbiddenError();
  }

  const targetUser = await new UserRetriever(req.params.id).getSingleObject();
  const targetUserRoleId = targetUser.role.id;
  const limitedUserRoleId = await getLimitedUserRoleId();
  const bannedUserRoleId = await getBannedUserRoleId();

  const isTargetUserLimited = targetUserRoleId === limitedUserRoleId;
  const isTargetUserBanned = targetUserRoleId === bannedUserRoleId;

  // if the user is trying to wipe an account that is not limited
  // and not banned, throw forbidden
  if (!isTargetUserLimited && !isTargetUserBanned) {
    throw new ForbiddenError();
  }

  // if the user is trying to wipe an account that is banned,
  // make sure their previous role is was limited
  if (isTargetUserBanned) {
    const previousUserRole = await PreviousUserRole.findOne({
      attributes: ['role_id'],
      where: { user_id: req.params.id },
      order: [['created_at', 'DESC']],
    });

    if (previousUserRole?.role_id !== limitedUserRoleId) {
      throw new ForbiddenError();
    }
  }

  await authorize(req.user?.id, ['user-archive']);
  return next();
};
