/* eslint-disable import/prefer-default-export */

import * as banControllerPolicy from './banControllerPolicy';
import * as imageControllerPolicy from './imageControllerPolicy';
import * as conversationControllerPolicy from './conversationControllerPolicy';
import * as messageOfTheDayControllerPolicy from './messageOfTheDayControllerPolicy';
import * as messageControllerPolicy from './messageControllerPolicy';
import * as reportCreatePolicy from './report/createPolicy';
import * as reportGetAllPolicy from './report/getAllPolicy';
import * as reportGetOpenCountPolicy from './report/getOpenCountPolicy';
import * as reportGetPolicy from './report/getPolicy';
import * as reportResolvePolicy from './report/resolvePolicy';
import * as ruleControllerPolicy from './ruleControllerPolicy';
import * as tagControllerPolicy from './tagControllerPolicy';
import * as moderationControllerPolicy from './moderationControllerPolicy';
import * as postControllerPolicy from './postControllerPolicy';
import * as threadControllerPolicy from './threadControllerPolicy';
import * as subforumGetThreadsPolicy from './subforum/getThreadsPolicy';
import * as userProfileControllerPolicy from './userProfileControllerPolicy';
import * as calendarEventControllerPolicy from './calendarEventControllerPolicy';
import * as userControllerPolicy from './userControllerPolicy';

export {
  banControllerPolicy,
  messageOfTheDayControllerPolicy,
  conversationControllerPolicy,
  imageControllerPolicy,
  messageControllerPolicy,
  reportCreatePolicy,
  reportGetAllPolicy,
  reportGetOpenCountPolicy,
  reportGetPolicy,
  reportResolvePolicy,
  ruleControllerPolicy,
  tagControllerPolicy,
  moderationControllerPolicy,
  postControllerPolicy,
  threadControllerPolicy,
  subforumGetThreadsPolicy,
  userProfileControllerPolicy,
  calendarEventControllerPolicy,
  userControllerPolicy,
};
