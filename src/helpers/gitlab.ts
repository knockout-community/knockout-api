import { KnockoutCommit } from 'knockout-schema';
import gitlabClient from '../services/gitlabClient';
import gitlabUserMapping from './gitlabUserMapping.json';

const COMMITS_PER_PAGE_PER_PROJECT = 30;
const BACKEND_GITLAB_PROJECT_ID = '20651753';
const FRONTEND_GITLAB_PROJECT_ID = '20651370';

const projectNameFromProjectId = (projectId: string) => {
  switch (projectId) {
    case BACKEND_GITLAB_PROJECT_ID:
      return 'API';
    case FRONTEND_GITLAB_PROJECT_ID:
      return 'Frontend';
    default:
      return 'Unknown Project';
  }
};

const userIdFromAuthorName = (authorName: string): number | undefined =>
  gitlabUserMapping.mappings.find((mapping) => mapping.gitlabAuthorName === authorName)?.userId;

const getProjectCommitList = async (
  projectId: string,
  page: number = 1
): Promise<KnockoutCommit[]> => {
  try {
    const commits = await gitlabClient.Commits.all(projectId, {
      page,
      perPage: COMMITS_PER_PAGE_PER_PROJECT,
      ref_name: 'master',
    });

    return commits.map((commit) => ({
      id: commit.id,
      projectName: projectNameFromProjectId(projectId),
      title: commit.title,
      authorName: commit.author_name,
      userId: userIdFromAuthorName(commit.author_name),
      date: new Date(commit.committed_date!).toISOString(),
      url: commit.web_url,
    }));
  } catch (err) {
    console.error(err);
    return [];
  }
};

// eslint-disable-next-line import/prefer-default-export
export const getCommitsForAllProjects = async (page: number = 1): Promise<KnockoutCommit[]> => {
  const backendCommits = await getProjectCommitList(BACKEND_GITLAB_PROJECT_ID, page);
  const frontendCommits = await getProjectCommitList(FRONTEND_GITLAB_PROJECT_ID, page);

  return [...backendCommits, ...frontendCommits].sort((commitA, commitB) =>
    commitB.date.localeCompare(commitA.date)
  );
};
