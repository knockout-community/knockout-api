/* eslint-disable class-methods-use-this */
/* eslint-disable max-classes-per-file */
import { Response } from 'express';
import httpStatus from 'http-status';

interface Result {
  [key: string]: any;
  message?: string;
}

interface Options {
  page: number;
}

abstract class ResponseStrategy<T = Result> {
  result: T;

  constructor(result: T) {
    this.result = result;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  sendAction(res: Response, options?: Options) {
    throw Error('Response strategy not defined.');
  }
}

class ErrorResponse extends ResponseStrategy {
  sendAction(res: Response) {
    switch (this.result.message) {
      case 'Validation Error':
        res.status(httpStatus.UNPROCESSABLE_ENTITY);
        res.json({ message: 'Unprocessable entity.' });
        break;
      default:
        res.status(httpStatus.SERVICE_UNAVAILABLE);
        res.json({ message: 'Service unavailable.' });
    }
  }
}

class NotFoundResponse extends ResponseStrategy<Result | null> {
  sendAction(res: Response) {
    res.status(httpStatus.NOT_FOUND);
    return res.json({
      message: 'Resource not found.',
    });
  }
}

class ResponseOK extends ResponseStrategy {
  sendAction(res: Response) {
    res.status(httpStatus.OK);
    return res.json(this.result);
  }
}

class PaginatedResponse extends ResponseStrategy {
  sendAction(res: Response, options?: Options) {
    if (options?.page === 1) {
      res.status(httpStatus.OK);
      return res.json(this.result);
    }
    return new NotFoundResponse(this.result).sendAction(res);
  }
}

const createResponseStrategy = () => {
  const selectStrategy = (result: Result | null) => {
    if (result === null) {
      return new NotFoundResponse(result);
    }
    if (typeof result === 'object' && 'list' in result) {
      return new ResponseOK(result);
    }
    if (result instanceof Error) {
      return new ErrorResponse(result);
    }
    if ('list' in result) {
      return new PaginatedResponse(result);
    }
    return new ResponseOK(result);
  };

  return {
    send(response: Response, result: Result | null, options?: Options) {
      selectStrategy(result).sendAction(response, options);
    },
  };
};

export default createResponseStrategy();
