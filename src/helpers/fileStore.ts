/* eslint-disable no-underscore-dangle */

import fs from 'fs';
import path from 'path';
import { NODE_ENV, S3_BUCKET } from '../../config/server';
import s3Client from '../services/s3Client';

const storeBufferRemotely = async (buffer, filePath, mimeType) => {
  if (!s3Client) return;

  const metaData = {
    'x-amz-acl': 'public-read',
    'Content-Type': mimeType,
  };
  s3Client.putObject(S3_BUCKET!, filePath, buffer, undefined!, metaData, (e) => {
    if (e) {
      console.log(`S3 upload failure: ${filePath}`);
    } else {
      console.log(`S3 upload complete: ${filePath}`);
    }
  });
};

const storeBufferLocally = async (buffer, filePath) => {
  const localPath = path.join(`${global.__basedir}/static/${filePath}`);
  fs.writeFile(localPath, buffer, (e) => {
    if (e) {
      console.log(`Local upload failure: ${localPath}`);
    } else {
      console.log(`Local upload complete: ${localPath}`);
    }
  });
};

const deleteFileRemotely = async (filePath) => {
  if (!s3Client) return;

  s3Client.removeObject(S3_BUCKET!, filePath, (e) => {
    if (e) {
      console.log(`S3 delete failure: ${filePath}`);
    } else {
      console.log(`S3 delete complete: ${filePath}`);
    }
  });
};

const deleteFileLocally = async (filePath) => {
  const localPath = path.join(`${global.__basedir}/static/${filePath}`);
  fs.unlink(localPath, (e) => {
    if (e) {
      console.log(`Local delete failure: ${localPath}`);
    } else {
      console.log(`Local delete complete: ${localPath}`);
    }
  });
};

export default {
  storeBuffer: async (buffer, filePath, mimeType) => {
    if (NODE_ENV === 'production') {
      await storeBufferRemotely(buffer, filePath, mimeType);
    } else {
      await storeBufferLocally(buffer, filePath);
    }
  },
  deleteFile: async (filePath) => {
    if (NODE_ENV === 'production') {
      await deleteFileRemotely(filePath);
    } else {
      await deleteFileLocally(filePath);
    }
  },
};
