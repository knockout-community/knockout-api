import { DELETED_USER_NAME } from '../constants/deletedUser';
import { jobError, jobInfo } from '../log';
import UserRetriever from '../retriever/user';
import deleteIpAddresses from '../services/userDeletion/deleteIpAddresses';
import deleteUser from '../services/userDeletion/deleteUser';
import scrubExternalAccount from '../services/userDeletion/scrubExternalAccount';

interface DeleteUserOptions {
  userId: number;
}

const log = (message: string) => jobInfo(`[jobs/delete-user] ${message}`);
const error = (message: string) => jobError(`[jobs/delete-user] ${message}`);

export default async (options: DeleteUserOptions) => {
  const { userId } = options;
  log(`Running job with userId ${userId}...`);

  try {
    // dont delete already deleted users
    const { username } = await new UserRetriever(userId).getSingleObject();
    if (username === DELETED_USER_NAME) {
      log(`User ${userId} already deleted. Exiting...`);
      return;
    }

    await scrubExternalAccount(userId);
    await deleteIpAddresses(userId);
    await deleteUser(userId);
    log(`Deleted user ${userId}`);
  } catch (e) {
    error(`Failed to run job: ${e.message}`);
  }
};
