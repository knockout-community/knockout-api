import { DELETED_USER_NAME } from '../constants/deletedUser';
import { jobError, jobInfo } from '../log';
import UserRetriever from '../retriever/user';
import deleteRatings from '../services/userDeletion/deleteRatings';
import deleteUser from '../services/userDeletion/deleteUser';
import invalidateCache from '../services/userDeletion/invalidateCache';
import scrubMessages from '../services/userDeletion/scrubMessages';
import { wipePostEdits } from '../services/userDeletion/scrubPostEdits';
import { wipePosts } from '../services/userDeletion/scrubPosts';
import scrubProfileComments from '../services/userDeletion/scrubProfileComments';
import scrubUser from '../services/userDeletion/scrubUser';
import scrubUserProfiles from '../services/userDeletion/scrubUserProfiles';

interface WipeUserOptions {
  userId: number;
}

const log = (message: string) => jobInfo(`[jobs/wipe-user] ${message}`);
const error = (message: string) => jobError(`[jobs/wipe-user] ${message}`);

export default async (options: WipeUserOptions) => {
  const { userId } = options;
  log(`Running job with userId ${userId}...`);

  try {
    // dont delete already deleted users
    const { username } = await new UserRetriever(userId).getSingleObject();
    if (username === DELETED_USER_NAME) {
      log(`User ${userId} already deleted. Exiting...`);
      return;
    }

    await wipePosts(userId);
    await wipePostEdits(userId);
    await scrubMessages(userId);
    await scrubProfileComments(userId);
    await scrubUserProfiles(userId);
    await scrubUser(userId);
    await deleteRatings(userId);
    await invalidateCache(userId);
    await deleteUser(userId);

    log(`Wiped user ${userId}`);
  } catch (e) {
    error(`Failed to run job: ${e.message}`);
  }
};
