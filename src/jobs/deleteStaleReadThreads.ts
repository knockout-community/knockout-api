/* eslint-disable no-await-in-loop */

import { Op } from 'sequelize';
import knex from '../services/knex';
import ReadThread from '../models/readThread';
import Thread from '../models/thread';

const BATCH_SIZE = 10000;
// Don't delete more than 10x the batch size, so jobs aren't too expensive if stale read thread count is high
const MAX_BATCHES_TO_DELETE = 10;

export const READ_THREAD_CUTOFF_DATE_MONTHS = 3;

const staleReadThreadsExist = async (cutoffDate) =>
  Boolean(
    await ReadThread.findOne({
      attributes: ['user_id'],
      where: { lastSeen: { [Op.lt]: cutoffDate } },
      include: {
        model: Thread,
        where: { pinned: false },
        required: true,
      },
    })
  );

export default async () => {
  console.log('[jobs/delete-read-threads] Running job...');

  try {
    // delete all read threads where lastSeen < READ_THREAD_CUTOFF_DATE_MONTHS months
    // process in batches of BATCH_SIZE

    const cutoffDate = new Date();
    cutoffDate.setMonth(cutoffDate.getMonth() - READ_THREAD_CUTOFF_DATE_MONTHS);

    let batchesDeleted = 0;

    while ((await staleReadThreadsExist(cutoffDate)) && batchesDeleted < MAX_BATCHES_TO_DELETE) {
      await knex('ReadThreads')
        .join('Threads', 'Threads.id', 'ReadThreads.thread_id')
        .where('Threads.pinned', false)
        .andWhere('ReadThreads.last_seen', '<', cutoffDate.toISOString())
        .limit(BATCH_SIZE)
        .del();

      batchesDeleted += 1;
    }

    console.log(
      `[jobs/delete-read-threads] Job complete! ~${
        batchesDeleted * BATCH_SIZE
      } read threads deleted.`
    );
    return null;
  } catch (e) {
    console.error(`[jobs/delete-read-threads] Failed to run job: ${e.message}`);
    return null;
  }
};
