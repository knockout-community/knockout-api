import { jobError, jobInfo } from '../log';
import calculateSiteStats from './calculateSiteStats';

export enum StatJob {
  CALCULATE_SITE_STATS = 'calculateSiteStats',
}

const log = (msg: string) => {
  jobInfo(`[jobs/stat-job-processor ${msg}`);
};

const error = (msg: string) => {
  jobError(`[jobs/stat-job-processor ${msg}`);
};

export default async (job) => {
  const { type } = job.data;
  try {
    switch (type) {
      case StatJob.CALCULATE_SITE_STATS:
        await calculateSiteStats();
        break;
      default:
        log(`Unhandled Job type encountered: ${type}`);
    }
    log(`Processed job with data ${JSON.stringify(job.data)}`);
    return Promise.resolve({ status: 'complete' });
  } catch (e) {
    error(`Failed to process job ${type}: ${e.message}`);
    // Throw the error again so that Bull's automatic failure retry can kick in
    throw e;
  }
};
