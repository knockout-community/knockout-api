import Queue from 'bull';

import { REDIS_HOST, REDIS_PORT, REDIS_PASSWORD } from '../../config/server';
import processUserJobs from './processUserJobs';
import { jobError, jobInfo } from '../log';
import processStatJobs, { StatJob } from './processStatJobs';

const queueOptions = {
  redis: { port: Number(REDIS_PORT), host: REDIS_HOST, password: REDIS_PASSWORD ?? undefined },
};

const defaultJobOptions = { removeOnComplete: true, removeOnFail: true };

// Run site stat calculation job every month on the 1st at 01:00
const CALCULATE_SITE_STATS_CRON = '0 1 1 * *';

const CALCULATE_SITE_STATS_JOB_NAME = 'calculateSiteStats';

// Separate queues based on domain

// Queue for user jobs
export const userQueue = new Queue('userQueue', {
  ...queueOptions,
  defaultJobOptions,
});

// Queue for bulk stat calculation
export const statQueue = new Queue('statQueue', {
  ...queueOptions,
  defaultJobOptions,
});

const handleError = (err) => {
  jobError(`Error running job: ${err}`);
};

const handleActive = (job) => {
  jobInfo(`Job started: ${JSON.stringify(job)}`);
};

const handleStalled = (job) => {
  jobError(`Job is stalled: ${JSON.stringify(job)}`);
};

const handleLockExtensionFailed = (job, err) => {
  jobError(`Job has failed to extend a lock: ${err}\nJob data: ${JSON.stringify(job)}`);
};

const handleCompleted = (job, result) => {
  jobInfo(`Job completed with result ${JSON.stringify(result)}\n Job data: ${JSON.stringify(job)}`);
};

const attachQueueEventHandlers = (queue) => {
  queue.on('error', handleError);
  queue.on('active', handleActive);
  queue.on('stalled', handleStalled);
  queue.on('lock-extension-failed', handleLockExtensionFailed);
  queue.on('completed', handleCompleted);
};

export const initializeQueues = () => {
  userQueue.process('*', processUserJobs);
  attachQueueEventHandlers(userQueue);
  statQueue.process('*', processStatJobs);
  attachQueueEventHandlers(statQueue);
};

export const initializeScheduledJobs = () => {
  const siteStatJobOptions = {
    repeat: { cron: CALCULATE_SITE_STATS_CRON },
    jobId: 'calculateSiteStatsJob',
  };
  statQueue.add(
    CALCULATE_SITE_STATS_JOB_NAME,
    { type: StatJob.CALCULATE_SITE_STATS },
    siteStatJobOptions
  );
};
