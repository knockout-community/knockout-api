import { Request, Response } from 'express';
import httpStatus from 'http-status';

import { Op } from 'sequelize';
import ms from 'ms';
import redis from '../services/redisClient';
import errorHandler from '../services/errorHandler';
import MessageOfTheDay from '../models/messageOfTheDay';

const MOTD_KEY = 'motd';

// Create a new MOTD rather than update
// so we can see MOTD histories
export const create = async (req: Request, res: Response) => {
  try {
    const { message, buttonName, buttonLink } = req.body;
    const userId = req.user!.id;

    await MessageOfTheDay.create({
      message: message.trim(),
      buttonName: buttonName.trim(),
      buttonLink: buttonLink.trim(),
      createdBy: userId,
    });

    redis.del(MOTD_KEY);

    res.status(httpStatus.CREATED);
    res.json({ message: 'MOTD updated.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const latest = async (req: Request, res: Response) => {
  try {
    const cachedData = await redis.get(MOTD_KEY);

    if (cachedData) {
      const result = JSON.parse(cachedData);

      res.status(httpStatus.OK);
      return res.json(result);
    }

    const latestMotd = await MessageOfTheDay.findOne({
      where: { createdAt: { [Op.gte]: Date.now() - ms('7 days') } },
      order: [['createdAt', 'DESC']],
      raw: true,
    });

    const result = latestMotd || {};
    delete result['createdBy'];

    redis.setex(MOTD_KEY, ms('7 days') / 1000, JSON.stringify(result));

    res.status(httpStatus.OK);
    return res.json(result);
  } catch (exception) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
