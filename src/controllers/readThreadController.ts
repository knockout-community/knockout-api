import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { AlertsResponse } from 'knockout-schema';
import errorHandler from '../services/errorHandler';
import { getUserPermissionCodes } from '../helpers/user';
import { alertsPerPage } from '../constants/pagination';
import ReadThread from '../models/readThread';
import ReadThreadRetriever from '../retriever/readThread';

export const store = async (
  userId: number,
  threadId: number,
  lastPostNumber: number,
  lastSeen: Date = new Date(),
  isSubscription: boolean = false
) =>
  (
    await ReadThread.upsert({
      user_id: userId,
      thread_id: threadId,
      lastSeen,
      lastPostNumber,
      isSubscription,
    })
  )[0].toJSON();

export const storeReadThread = async (
  userId: number,
  threadId: number,
  lastPostNumber: number,
  lastSeen: Date = new Date(),
  isSubscription: boolean = false
) =>
  (
    await ReadThread.upsert(
      {
        user_id: userId,
        thread_id: threadId,
        lastSeen,
        lastPostNumber,
        isSubscription,
      },
      { fields: ['lastSeen', 'lastPostNumber'] }
    )
  )[0].toJSON();

export const createReadThread = async (req: Request, res: Response) => {
  try {
    await storeReadThread(
      req.user!.id,
      req.body.threadId,
      req.body.lastPostNumber,
      new Date(req.body.lastSeen)
    );
    res.status(httpStatus.CREATED);
    res.send(`read thread #${req.body.threadId}`);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const createAlert = async (req: Request, res: Response) => {
  try {
    await store(
      req.user!.id,
      req.body.threadId,
      req.body.lastPostNumber,
      req.body.lastSeen ? new Date(req.body.lastSeen) : new Date(),
      true
    );

    res.status(httpStatus.CREATED);
    res.json({ message: `Created an alert for thread #${req.body.threadId}` });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const destroyAlerts = async (userId: number, threadIds: number[]) =>
  ReadThread.update(
    {
      isSubscription: false,
    },
    {
      where: {
        user_id: userId,
        thread_id: threadIds,
      },
    }
  );

export const destroyAlert = async (userId: number, threadId: number) =>
  destroyAlerts(userId, [threadId]);

export const destroy = async (userId: number, threadId: number) =>
  ReadThread.destroy({
    where: {
      user_id: userId,
      thread_id: threadId,
    },
  });

export const deleteReadThread = async (req: Request, res: Response) => {
  try {
    await destroy(req.user!.id, req.body.threadId);

    res.status(httpStatus.OK);
    res.json({ message: `Deleted readThread for thread #${req.body.threadId}` });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const get = async (
  userId: number,
  page: number = 1,
  hideNsfw: boolean = false,
  isSubscription: boolean = false
): Promise<AlertsResponse> => {
  const permissionCodes = await getUserPermissionCodes(userId);

  const query = await ReadThread.scope({
    method: ['byUser', userId, permissionCodes, hideNsfw],
  }).findAll({ attributes: ['thread_id'], where: { isSubscription } });
  const threads = await new ReadThreadRetriever(
    query.map((item) => item.thread_id),
    userId,
    isSubscription
  ).getObjectArray();

  const result = {
    alerts: threads,
    totalAlerts: threads.length,
    ids: isSubscription ? threads.map((thread) => thread.id) : [],
  };

  result.alerts.sort((alertA, alertB) => {
    if (alertA.unreadPosts > alertB.unreadPosts) return -1;
    if (alertA.unreadPosts < alertB.unreadPosts) return 1;

    if (alertA.thread.lastPost.createdAt > alertB.thread.lastPost.createdAt) return -1;
    if (alertA.thread.lastPost.createdAt < alertB.thread.lastPost.createdAt) return 1;
    return 0;
  });

  result.alerts = result.alerts.slice((page - 1) * alertsPerPage, page * alertsPerPage);

  return result;
};

export const getAlerts = async (userId: number, page: number = 1, hideNsfw: boolean = false) => {
  const result = (await get(userId, page, hideNsfw, true)) as any;

  result.alerts = result.alerts.map((alert) => ({
    ...alert,
    thread: undefined,
    threadId: alert.id,
    icon_id: alert.thread.iconId,
    threadIcon: alert.thread.iconId,
    threadTitle: alert.thread.title,
    threadUser: alert.thread.user.id,
    threadLocked: alert.thread.locked,
    threadCreatedAt: alert.thread.createdAt,
    threadUpdatedAt: alert.thread.updatedAt,
    threadDeletedAt: alert.thread.deleted,
    threadBackgroundUrl: alert.thread.backgroundUrl,
    threadBackgroundType: alert.thread.backgroundType,
    threadUsername: alert.thread.user.username,
    threadUserAvatarUrl: alert.thread.user.avatarUrl,
    threadUserRoleCode: alert.thread.user.role?.code,
    threadPostCount: alert.thread.postCount,
    lastPost: alert.thread.lastPost,
  }));
  return result;
};

export const listAlerts = async (req: Request, res: Response) => {
  try {
    const hideNsfw = req.query.hideNsfw || false;
    const pageParam: number = req.params.page;

    const results = await getAlerts(req.user!.id, pageParam, hideNsfw);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
