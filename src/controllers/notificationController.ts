import { NotificationType } from 'knockout-schema';
import { Server } from 'socket.io';
import { Transaction } from 'sequelize';
import { notificationsRoom } from '../handlers/notificationHandler';
import NotificationRetriever from '../retriever/notification';
import knex from '../services/knex';
import Notification from '../models/notification';

const NOTIFICATIONS_PER_PAGE = 25;

export const createNotification = async (
  type: NotificationType,
  userId: number,
  dataId: number,
  io?: Server,
  transaction?: Transaction
) => {
  const newNotification = (
    await Notification.upsert(
      {
        type,
        userId,
        dataId,
        read: false,
        createdAt: new Date(),
      },
      { fields: ['read', 'createdAt'], transaction }
    )
  )[0];

  const retriever = new NotificationRetriever(newNotification.id, userId, transaction);
  retriever.invalidate();

  const notification = await retriever.getSingleObject();

  if (io) {
    io.in(notificationsRoom(userId)).emit('notification:new', notification);
  }
};

export const getAll = async (userId: number, page = 1) => {
  // set date cutoff to two weeks ago
  const cutoff = new Date(Date.now() - 1000 * 60 * 60 * 24 * 14);

  const notifications = await knex('Notifications')
    .select('id')
    .where('user_id', userId)
    .andWhere('created_at', '>', cutoff)
    .orderBy('created_at', 'desc')
    .limit(Number(NOTIFICATIONS_PER_PAGE))
    .offset(Number(NOTIFICATIONS_PER_PAGE) * (page - 1));

  return new NotificationRetriever(
    notifications.map((notification) => notification.id),
    userId
  ).getObjectArray();
};

export const markAsRead = async (userId: number, ids: number[]) => {
  await knex('Notifications').where('user_id', userId).whereIn('id', ids).update({ read: true });
  await new NotificationRetriever(ids).invalidate();
  return 'Marked notifications as read.';
};

export const markAllAsRead = async (userId: number) => {
  const notificationIds = (
    await knex('Notifications')
      .select('id')
      .where('user_id', userId)
      .andWhere('created_at', '>', new Date(Date.now() - 1000 * 60 * 60 * 24 * 14))
  ).map((notification) => notification.id);

  // mark all notificationIds as read
  await knex('Notifications').whereIn('id', notificationIds).update({ read: true });
  new NotificationRetriever(notificationIds).invalidate();
  return 'Marked all notifications as read.';
};
