import { Request, Response, User } from 'express';
import httpStatus from 'http-status';
import { BadRequestError, ForbiddenError } from 'routing-controllers';
import { Server } from 'socket.io';
import ms from 'ms';
import {
  CreatePostRequest,
  NotificationType,
  EventType,
  UpdatePostRequest,
  Post as PostSchema,
  THREAD_POST_LIMIT,
} from 'knockout-schema';
import { Transaction } from 'sequelize';
import countryLookup from 'country-code-lookup';
import knex from '../services/knex';

import responseStrategy from '../helpers/responseStrategy';
import { postMentionFinder, postQuoteFinder } from '../helpers/postMentionFinder';
import { getCountryName } from '../helpers/geoipLookup';
import { validateAppName } from '../validations/post';
import PostRetriever, { PostFlag } from '../retriever/post';
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';
import errorHandler from '../services/errorHandler';
import { createNotification } from './notificationController';
import { userCanViewThread, userHasPermissions } from '../helpers/user';

import { createEvent, createSocketEvent } from './eventLogController';
import { getRoleIdsWithPermissions, RoleCode } from '../helpers/permissions';
import { threadPostRoom, ThreadPostRoomSuffix } from '../handlers/threadPostHandler';
import RoleRetriever, { RoleFlag } from '../retriever/role';
import postResponseFinder from '../helpers/postResponseFinder';
import PostResponseRetriever from '../retriever/postResponse';
import UserRetriever from '../retriever/user';
import PostEdit from '../models/postEdit';
import Post from '../models/post';
import IpAddress from '../models/ipAddress';
import PostResponse from '../models/postResponse';
import { getRatingsForPostsWithUsers } from '../models/rating';

const { validateThreadStatus, validatePostLength } = require('../validations/post');
const { validateUserFields } = require('../validations/user');

const storePostEdit = async (
  originalPostId: number,
  userId: number,
  content: string,
  editReason: string = '',
  transaction?: Transaction
) => {
  const originalPost = await new PostRetriever(originalPostId, [
    PostFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();

  const createdAt = new Date(originalPost.createdAt);
  const diff = Date.now() - createdAt.getTime();

  const pastEditBufferTime = diff > ms('5 minutes');

  // if we aren't past the edit buffer time, bail out
  if (!pastEditBufferTime) {
    return;
  }

  // if this is the first update (no corresponding PostEdit records),
  // store the original post as a PostEdit record so we have access to it
  const firstEdit = !(await PostEdit.findOne({
    where: { postId: originalPost.id },
    attributes: ['id'],
  }));

  if (firstEdit) {
    await PostEdit.create(
      {
        postId: originalPost.id,
        userId: originalPost.userId,
        editReason: '',
        content: originalPost.content,
        createdAt: new Date(originalPost.createdAt),
      },
      { transaction }
    );
  }

  // create the post edit record
  await PostEdit.create(
    {
      postId: originalPost.id,
      userId,
      editReason,
      content,
    },
    { transaction }
  );

  // invalidate the posts last edited user
  await new PostRetriever(originalPost.id).invalidateLastEditedUsers();
};

export const store = async (
  body: CreatePostRequest,
  user: User,
  ipInfo: string,
  io?: Server,
  countryCode?: string,
  transaction?: Transaction
): Promise<PostSchema> => {
  const thread = await new ThreadRetriever(body.thread_id, [
    ThreadFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();
  const skipPostValidations = await userHasPermissions(
    [`subforum-${thread.subforumId}-post-bypass-validations`],
    user.id
  );

  // validations for a valid post
  if (
    !skipPostValidations &&
    (!(await validateThreadStatus(body.thread_id, transaction)) ||
      !validatePostLength(body.content))
  ) {
    throw new BadRequestError('Failed validations.');
  }

  if (!validateUserFields(user)) {
    throw new BadRequestError('Invalid user.');
  }

  // client name logic
  let appName: string | undefined;

  if (body.app_name && validateAppName(body.app_name)) {
    appName = body.app_name;
  }

  let post;
  let lastPostId: number;
  let automerge = false;

  // automerge logic
  // if the latest post in a thread is by this
  // user, merge posts instead of creating a new one
  const lastPostQuery = await knex
    .from('Posts as po')
    .select('po.id', 'po.content', 'po.user_id')
    .where(
      knex.raw('po.created_at >= DATE_SUB(NOW(), INTERVAL 3 HOUR) AND po.thread_id = ?', [
        body.thread_id,
      ])
    )
    .limit(1)
    .orderByRaw('po.id DESC');

  // post is automerge
  if (lastPostQuery.length > 0 && lastPostQuery[0].user_id === user.id) {
    lastPostId = lastPostQuery[0].id;
    const resultingContent = `${lastPostQuery[0].content}

[b]Edited:[/b]

${body.content}`;

    if (!validatePostLength(resultingContent)) {
      throw new BadRequestError('Failed validations.');
    }

    await Post.update(
      {
        updatedAt: new Date(),
        content: resultingContent,
        app_name: appName,
      },
      { transaction, where: { id: lastPostId } }
    );
    await storePostEdit(lastPostId, user.id, resultingContent, 'Automerge', transaction);

    await new PostRetriever(lastPostId).invalidate();

    post = await knex('Posts')
      .select('id', 'user_id', 'thread_id')
      .where({ id: lastPostId })
      .first();

    automerge = true;
  } else {
    // country information logic
    let countryName: string | null | undefined;
    if (body.display_country) {
      if (countryCode) {
        countryName = countryLookup.byIso(countryCode)?.country;
      }
      countryName ??= await getCountryName(ipInfo);
    }

    // new post
    post = await Post.create(
      {
        ...body,
        user_id: user.id,
        country_name: countryName,
        app_name: appName,
      },
      { transaction }
    );

    // new ip address
    await IpAddress.create(
      {
        ip_address: ipInfo,
        user_id: user.id,
        post_id: post.id,
      },
      {
        transaction,
      }
    );
    lastPostId = post.id;

    // invalidate caches and emit locked thread event if necessary
    transaction?.afterCommit(async () => {
      const threadRetriever = new ThreadRetriever(post.thread_id);

      // invalidate thread post cache
      await threadRetriever.invalidatePosts();

      // invalidate the thread if the thread has been locked
      if (post.thread_post_number >= THREAD_POST_LIMIT) {
        const roleIds = await getRoleIdsWithPermissions([`subforum-${thread.subforumId}-view`]);

        await createEvent(
          post.user_id,
          EventType.THREAD_POST_LIMIT_REACHED,
          post.thread_id,
          undefined,
          roleIds
        );

        await threadRetriever.invalidate();
      }
    });
  }

  const result: PostSchema = await new PostRetriever(lastPostId!, [], {
    transaction,
  }).getSingleObject();

  // mentions logic
  if (post && post.id) {
    const quotes = postQuoteFinder(body.content);
    const mentions = postMentionFinder(body.content);

    if (quotes.length > 0) {
      // retrieve all quote users
      const quoteUserIds = quotes.filter((userId) => userId !== user.id);
      const quoteUsers = await new UserRetriever(quoteUserIds).get();
      const quoteQueries = quoteUserIds.map(async (userId) => {
        // user must be able to see the thread to get the notification
        const userRole = await new RoleRetriever(quoteUsers.get(userId)!.role!.id, [
          RoleFlag.INCLUDE_PERMISSION_CODES,
        ]).getSingleObject();
        const userPermissions = userRole.permissionCodes || [];
        if (userCanViewThread(userPermissions, thread, userId)) {
          return createNotification(NotificationType.POST_REPLY, userId, post.id, io, transaction);
        }
        return undefined;
      });
      await Promise.all(quoteQueries);
    }

    if (mentions.length > 0) {
      const mentionUserIds = mentions.filter((userId) => userId !== user.id);
      const mentionUsers = await new UserRetriever(mentionUserIds).get();
      const mentionQueries = mentionUserIds.map(async (userId) => {
        // user must be able to see the thread to get the notification
        const userRole = await new RoleRetriever(mentionUsers.get(userId)!.role!.id, [
          RoleFlag.INCLUDE_PERMISSION_CODES,
        ]).getSingleObject();
        const userPermissions = userRole.permissionCodes || [];
        if (userCanViewThread(userPermissions, thread, userId)) {
          return createNotification(
            NotificationType.POST_MENTION,
            userId,
            post.id,
            io,
            transaction
          );
        }
        return undefined;
      });
      await Promise.all(mentionQueries);
    }
  }

  // responses logic
  if (post?.id) {
    const responseOriginalPostIds = postResponseFinder(body.content);
    // only parse the first 100 post responses to avoid misuse
    if (responseOriginalPostIds.length < 100) {
      const postResponses = responseOriginalPostIds
        .filter((postId) => postId !== post.id)
        .map((postId) => ({
          originalPostId: postId,
          responsePostId: post.id,
          createdAt: new Date(),
        }));

      await PostResponse.bulkCreate(postResponses, {
        fields: ['originalPostId', 'responsePostId'],
        ignoreDuplicates: true,
        transaction,
      });

      new PostResponseRetriever(responseOriginalPostIds).invalidate();
    }
  }

  if (!automerge && io) {
    // send socket event to only the roles that can view this post
    const roleIds = await getRoleIdsWithPermissions([`subforum-${thread.subforumId}-view`]);
    createSocketEvent(user.id, EventType.POST_CREATED, post.id, io, {}, undefined, roleIds);
    const subscribedPostData = new PostRetriever(result.id, [
      PostFlag.RETRIEVE_SHALLOW,
      PostFlag.INCLUDE_THREAD,
    ]).getSingleObject();
    const threadPostData = new PostRetriever(result.id).getSingleObject();
    io.in(threadPostRoom(thread.id, ThreadPostRoomSuffix.SUBSCRIBED)).emit(
      'post:new',
      await subscribedPostData
    );
    io.in(threadPostRoom(thread.id, ThreadPostRoomSuffix.POSTS)).emit(
      'threadPost:new',
      await threadPostData
    );
  }

  return result;
};

export const storePost = async (req: Request, res: Response) => {
  try {
    res.status(httpStatus.CREATED);
    return res.json(
      store(
        { ...req.body, display_country: req.body.displayCountryInfo, app_name: req.body.appName },
        req.user!,
        req.ipInfo!,
        req.app.get('io'),
        req.header('CF-IPCountry')
      )
    );
  } catch (exception) {
    if (exception instanceof ForbiddenError) {
      return res.status(httpStatus.FORBIDDEN).json({
        error: exception.message,
      });
    }
    if (exception instanceof BadRequestError) {
      return res.status(httpStatus.BAD_REQUEST).json({
        error: exception.message,
      });
    }
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const update = async (
  id: number,
  body: UpdatePostRequest,
  user: User,
  ipInfo: string,
  io?: Server
) => {
  const postRetriever = new PostRetriever(id, [PostFlag.RETRIEVE_SHALLOW, PostFlag.INCLUDE_THREAD]);
  const post = await postRetriever.getSingleObject();
  const thread = await new ThreadRetriever(post.thread!.id, [
    ThreadFlag.RETRIEVE_SHALLOW,
  ]).getSingleObject();

  const skipPostValidations = await userHasPermissions(
    [`subforum-${thread.subforumId}-post-bypass-validations`],
    user.id
  );

  // validations for a valid post
  if (!skipPostValidations && !validatePostLength(body.content)) {
    throw new BadRequestError('Failed validations.');
  }
  if (!validateUserFields(user)) {
    throw new BadRequestError('Invalid user.');
  }

  // client name logic
  let appName: string | null;

  if (body.app_name) {
    appName = validateAppName(body.app_name) ? body.app_name : null;
  } else {
    appName = null;
  }

  const originalPostContent = post.content;

  await knex('Posts').where({ id }).update({
    content: body.content,
    updated_at: knex.fn.now(),
    app_name: appName,
  });

  // create new IP corresponding to this post
  IpAddress.create({
    ip_address: ipInfo,
    post_id: id,
    user_id: user.id,
  }).catch((e) => console.error(`Could not log IP for post: ${e}`));

  await storePostEdit(id, user.id, body.content, body.editReason);

  postRetriever.invalidate();

  const originalMentions = postMentionFinder(originalPostContent);
  const newMentions = postMentionFinder(body.content);
  const originalResponses = postResponseFinder(originalPostContent);
  const newResponses = postResponseFinder(body.content);

  // if any new mentions are in the edit, create notification for them
  // dont create mentions that were already created in the original body
  if (newMentions.length > 0) {
    const mentionQueries = newMentions.map((userId) => {
      if (!originalMentions.includes(userId) && userId !== user.id) {
        return createNotification(NotificationType.POST_MENTION, userId, post.id, io);
      }
      return undefined;
    });
    await Promise.all(mentionQueries);
  }

  // any PostResponse with an original_post_id of this postID needs to be invalidated
  const postResponseIdsToInvalidate = (
    await PostResponse.findAll({
      attributes: ['originalPostId'],
      where: { responsePostId: id },
    })
  ).map((pr) => pr.originalPostId);

  await new PostResponseRetriever(postResponseIdsToInvalidate).invalidate();

  // remove responses not in newResponses
  const responsesToDelete = originalResponses.filter((r) => !newResponses.includes(r));
  if (responsesToDelete.length > 0) {
    await PostResponse.destroy({
      where: { originalPostId: responsesToDelete, responsePostId: id },
    });
  }

  // create new responses
  const responsesToCreate = newResponses.filter((r) => !originalResponses.includes(r));
  const newPostResponses = responsesToCreate
    .filter((postId) => postId !== post.id)
    .map((postId) => ({
      originalPostId: postId,
      responsePostId: id,
      createdAt: new Date(),
    }));

  if (newPostResponses.length > 0) {
    await PostResponse.bulkCreate(newPostResponses, {
      fields: ['originalPostId', 'responsePostId'],
      ignoreDuplicates: true,
    });
  }

  const originalQuotes = postQuoteFinder(originalPostContent);
  const newQuotes = postQuoteFinder(body.content);

  // if any new replies are in the edit, create notification for them
  // dont create replies that were already created in the original body
  if (newQuotes.length > 0) {
    const replyQueries = newQuotes.map((userId) => {
      if (!originalQuotes.includes(userId) && userId !== user.id) {
        return createNotification(NotificationType.POST_REPLY, userId, post.id, io);
      }
      return undefined;
    });
    await Promise.all(replyQueries);
  }

  return { message: 'Post updated.' };
};

export const updatePost = async (req: Request, res: Response) => {
  try {
    await update(req.body.id, { ...req.body, app_name: req.body.appName }, req.user!, req.ipInfo!);
    res.status(httpStatus.CREATED);
    res.json({ message: 'Post updated' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const withPostsAndCount = async (req: Request, res: Response) => {
  try {
    const queryResults = await knex
      .from('Posts as po')
      .select(
        'po.id as postId',
        'po.content as postContent',
        'po.user_id as postUserId',
        'po.created_at as postCreatedAt',
        'po.updated_at as postUpdatedAt',
        'po.thread_id as postThreadId',
        'us.username as userUsername',
        'us.avatar_url as userAvatar_url',
        'us.background_url as userBackgroundUrl',
        'us.created_at as userCreated_at',
        'us.role_id as userRoleId'
      )
      .join('Users as us', 'us.id', 'po.user_id')
      .where(knex.raw('po.id = ?', req.params.id))
      .limit(1);

    if (queryResults.length === 0) {
      return responseStrategy.send(res, null);
    }

    const postWithUser = queryResults[0];

    const ratings = await getRatingsForPostsWithUsers([postWithUser.postId]);

    const userRole = await new RoleRetriever(postWithUser.userRoleId).getSingleObject();
    const userIsBanned = userRole?.code === RoleCode.BANNED_USER;

    const response = {
      id: postWithUser.postId,
      content: postWithUser.postContent,
      createdAt: postWithUser.postCreatedAt,
      updatedAt: postWithUser.postUpdatedAt,
      user: {
        id: postWithUser.postUserId,
        username: postWithUser.userUsername,
        avatar_url: postWithUser.userAvatar_url,
        backgroundUrl: postWithUser.userBackgroundUrl,
        createdAt: postWithUser.userCreated_at,
        isBanned: userIsBanned,
      },
      ratings: ratings[postWithUser.postId],
    };

    return responseStrategy.send(res, response);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};
