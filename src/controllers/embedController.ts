import { Request, Response } from 'express';
import redis from '../services/redisClient';

const getEmbed = async (tweetUrl: string) => {
  const cachedData = await redis.get(`tweetData:${tweetUrl}`);
  if (cachedData) {
    return JSON.parse(cachedData);
  }

  const path = new URL(tweetUrl).pathname;

  const response = await fetch(`https://api.fxtwitter.com${path}`);
  const json = await response.json();

  await redis.setex(`tweetData:${tweetUrl}`, 60 * 60 * 3, JSON.stringify(json));
  return json;
};

export default async function tweetEmbed(req: Request, res: Response) {
  const json = await getEmbed(req.query.url);
  if (json.tweet?.replying_to_status) {
    json.tweet.replying_to = (
      await getEmbed(
        `https://twitter.com/${json.tweet.replying_to}/status/${json.tweet.replying_to_status}`
      )
    ).tweet;
  }
  return res.json(json);
}
