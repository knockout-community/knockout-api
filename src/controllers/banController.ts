import { Request, Response } from 'express';

import httpStatus from 'http-status';
import { Server } from 'socket.io';
import ms from 'ms';
import { EventType } from 'knockout-schema';
import { Op } from 'sequelize';
import errorHandler from '../services/errorHandler';

import UserRetriever from '../retriever/user';
import { getBannedUserRoleId } from '../helpers/role';
import { createEvent } from './eventLogController';
import { getRoleIdsWithPermissions } from '../helpers/permissions';
import PostRetriever, { PostFlag } from '../retriever/post';
import { userQueue } from '../jobs';
import { UserJob } from '../jobs/processUserJobs';
import { jobInfo } from '../log';
import Ban from '../models/ban';
import User from '../models/user';

const UNBAN_USER_DELAY_OFFSET_MS = 5000;

export const banUser = async (
  length: number,
  userId: number,
  reason: string,
  bannedBy: number,
  io?: Server,
  postId?: number,
  accountWipe = false
) => {
  const expiresAt = new Date();
  const isPermaban = length <= 0;

  if (isPermaban) {
    // If the ban length is 0 or less, perma ban
    expiresAt.setUTCFullYear(2100);
  } else {
    expiresAt.setUTCHours(expiresAt.getUTCHours() + length);
  }

  const banId = (
    await Ban.create({
      user_id: userId,
      expires_at: expiresAt,
      ban_reason: reason,
      post_id: postId,
      banned_by: bannedBy,
    })
  ).id;

  let type: EventType = EventType.USER_BANNED;
  if (accountWipe) type = EventType.USER_WIPED;

  // if the ban is associated with a post, only broadcast
  // the ban event to users that can view the post
  let roleIds;
  if (postId) {
    const postRetriever = new PostRetriever(postId, [PostFlag.INCLUDE_THREAD]);
    const { subforumId } = (await postRetriever.getSingleObject()).thread!;
    roleIds = await getRoleIdsWithPermissions([`subforum-${subforumId}-view`]);

    // invalidate post bans cache
    await postRetriever.invalidateBans();
  }

  createEvent(bannedBy, type, banId, io, {}, roleIds);

  // update user role
  const bannedUserRoleId = await getBannedUserRoleId();
  const user = await User.findOne({ where: { id: userId } });
  await user?.update({ roleId: bannedUserRoleId });
  await user?.save();

  await new UserRetriever(userId).invalidate();

  // enqueue unban job
  // dont enqueue for permabans
  if (isPermaban) {
    jobInfo(`User ${userId} was permabanned. No unban job enqueued.`);
    return expiresAt;
  }

  // check if user has an active ban with a later expiration date
  const activeBan = await Ban.findOne({
    attributes: ['id', 'expires_at'],
    where: { user_id: userId, expires_at: { [Op.gt]: new Date() }, id: { [Op.ne]: banId } },
    order: [['expires_at', 'desc']],
  });

  if (!activeBan || new Date(activeBan.expires_at) < expiresAt) {
    const unbanJobId = `unban-user-${userId}`;
    if (activeBan) {
      jobInfo(`Removing active unban job for ban ID ${activeBan.id} and user ID ${userId}`);
      const activeUnbanJob = await userQueue.getJob(unbanJobId);
      await activeUnbanJob?.remove();
    }

    const delayMs = expiresAt.getTime() - Date.now() + UNBAN_USER_DELAY_OFFSET_MS;
    jobInfo(`Enqueuing user ban job for userId ${userId} with delay ${ms(delayMs)}`);
    await userQueue.add(
      { type: UserJob.UNBAN_USER, userId },
      {
        delay: delayMs,
        attempts: 3,
        backoff: { type: 'exponential', delay: 30000 },
        jobId: unbanJobId,
      }
    );
  }

  return expiresAt;
};

export const store = async (req: Request, res: Response) => {
  try {
    const { banLength } = req.body;

    const parsedBanLength = Number(banLength);

    const expiresAt = await banUser(
      parsedBanLength,
      req.body.userId,
      req.body.banReason,
      req.user!.id,
      req.app.get('io'),
      req.body.postId
    );

    res.status(httpStatus.CREATED);
    res.json({
      message: `User banned. Unban date: ${expiresAt.toUTCString()}`,
      banExpiresAt: expiresAt.getTime(),
    });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
