import { Request, Response } from 'express';
import responseStrategy from '../../helpers/responseStrategy';
import ReportRetriever from '../../retriever/report';

// eslint-disable-next-line import/prefer-default-export
export const get = async (req: Request, res: Response) => {
  const report = await new ReportRetriever(req.params.id).getSingleObject();
  return responseStrategy.send(res, report);
};
