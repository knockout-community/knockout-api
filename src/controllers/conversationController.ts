import { Request, Response } from 'express';
import httpStatus from 'http-status';
import ConversationRetriever, { ConversationFlag } from '../retriever/conversation';
import errorHandler from '../services/errorHandler';
import ConversationUser from '../models/conversationUser';
import Conversation from '../models/conversation';
import UserArchivedConversation from '../models/userArchivedConversation';

export const isUserInConversation = async (conversationId: number, userId: number) =>
  Boolean(
    await ConversationUser.findOne({
      where: { user_id: userId, conversation_id: conversationId },
      attributes: ['user_id'],
    })
  );

/**
 * Shows a conversation
 */
export const show = async (req: Request, res: Response) => {
  try {
    const results = await new ConversationRetriever(req.params.id).getSingleObject();
    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getConversations = async (userId: number) => {
  const converationIds = (
    await Conversation.findAll({
      attributes: ['id'],
      include: {
        model: ConversationUser,
        where: { user_id: userId },
      },
      order: [['created_at', 'desc']],
    })
  ).map((conversation) => conversation.id);

  // remove any user archived conversations
  const userArchivedConversationIds = (
    await UserArchivedConversation.findAll({
      where: { userId },
      attributes: ['conversationId'],
    })
  ).map((conversation) => conversation.conversationId);

  const filteredConversationIds = converationIds.filter(
    (conversationId) => !userArchivedConversationIds.includes(conversationId)
  );

  const results = await new ConversationRetriever(filteredConversationIds, [
    ConversationFlag.RETRIEVE_SHALLOW,
  ]).getObjectArray();

  return results;
};

export const index = async (req: Request, res: Response) => {
  try {
    const results = await getConversations(req.user!.id);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const archiveConversation = async (userId: number, conversationId: number) => {
  await UserArchivedConversation.findOrCreate({
    where: { userId, conversationId },
    defaults: { userId, conversationId },
  });
};

export const destroy = async (req: Request, res: Response) => {
  try {
    await archiveConversation(req.user!.id, req.params.id);
    res.status(httpStatus.OK);
    res.json({ message: 'Conversation archived.' });
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
