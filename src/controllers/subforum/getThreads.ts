import { Request, Response } from 'express';
import { Op, fn, col, where, ScopeOptions } from 'sequelize';
import { ThreadWithLastPost } from 'knockout-schema';
import ResponseStrategy from '../../helpers/responseStrategy';
import ThreadRetriever, { ThreadFlag } from '../../retriever/thread';
import SubforumRetriever, { SubforumFlag } from '../../retriever/subforum';
import { THREADS_PER_SUBFORUM_PAGE } from '../../../config/server';
import {
  getUserPermissionCodes,
  userHasPermissions,
  userHiddenThreadIds,
} from '../../helpers/user';
import nsfwTagName from '../../constants/nsfwTagName';
import Thread from '../../models/thread';
import Tag from '../../models/tag';

const getThreadIds = async (
  subforumId: number,
  userId?: number,
  page: number = 1,
  perPage: number = 40,
  hideNsfw: boolean = false
) => {
  const permissionCodes = await getUserPermissionCodes(userId);
  const hiddenThreads = await userHiddenThreadIds(userId);
  const threadScopes: ScopeOptions[] = [
    { method: ['byUser', userId, permissionCodes, hiddenThreads] },
    { method: ['bySubforum', subforumId] },
  ];

  const whereCondition = {
    [Op.or]: [
      where(fn('FIND_IN_SET', nsfwTagName, fn('GROUP_CONCAT', col('Tags.name'))), '=', 0),
      where(fn('FIND_IN_SET', nsfwTagName, fn('GROUP_CONCAT', col('Tags.name'))), '=', null),
    ],
  };

  const query = await Thread.scope(threadScopes).findAll({
    attributes: ['id'],
    limit: perPage,
    offset: (page - 1) * perPage,
    subQuery: false,
    having: hideNsfw ? whereCondition : undefined,
    include: {
      attributes: ['id'],
      model: Tag,
      required: false,
    },
    order: [
      ['pinned', 'desc'],
      ['updated_at', 'desc'],
    ],
    group: ['Thread.id'],
  });

  return query.map((thread) => thread.id);
};

export const getSubformThreads = async (
  id: number,
  hideNsfw: boolean,
  userId?: number,
  page: number = 1
) => {
  // user must be able to view the entire subforum to show total thread count
  const showTotalThreads = await userHasPermissions([`subforum-${id}-view`], userId);
  const subforum = await new SubforumRetriever(
    id,
    showTotalThreads ? [SubforumFlag.INCLUDE_TOTAL_THREADS] : []
  ).getSingleObject();
  const threadIds = await getThreadIds(
    subforum.id,
    userId,
    page,
    Number(THREADS_PER_SUBFORUM_PAGE),
    hideNsfw
  );

  const flags = [ThreadFlag.INCLUDE_LAST_POST, ThreadFlag.INCLUDE_POST_COUNT];

  const threadRetriever = new ThreadRetriever(threadIds, flags, {
    userId,
  });
  const threads = (await threadRetriever.getObjectArray()) as ThreadWithLastPost[];

  return {
    subforum,
    page,
    threads,
  };
};

export const getThreads = async (req: Request, res: Response) => {
  const hideNsfw = req.query.hideNsfw || false;

  const pageNum = req.params.page ? Number(req.params.page) : 1;

  const { subforum, threads } = await getSubformThreads(
    req.params.id,
    hideNsfw,
    req.user?.id,
    pageNum
  );

  ResponseStrategy.send(res, {
    id: subforum.id,
    name: subforum.name,
    createdAt: subforum.createdAt,
    updatedAt: subforum.updatedAt,
    totalThreads: subforum.totalThreads,
    currentPage: pageNum,
    threads,
  });
};
