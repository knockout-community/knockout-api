import querystring from 'querystring';
import jwt from 'jsonwebtoken';
import ms from 'ms';

import { Response } from 'express';
import httpStatus from 'http-status';
import { JWT_SECRET as jwtSecret } from '../../../config/server';
import knex from '../../services/knex';
import redis from '../../services/redisClient';
import { RAID_MODE_KEY } from '../../constants/adminSettings';
import Scamalytics from '../../services/scamalytics';
import RoleRetriever from '../../retriever/role';
import * as config from '../../../config/server';
import User from '../../models/user';
import IpAddress from '../../models/ipAddress';

export enum Provider {
  TWITTER = 'TWITTER',
  GITHUB = 'GITHUB',
  STEAM = 'STEAM',
  GOOGLE = 'GOOGLE',
}
/**
 * Generate a JWT token with a two-week expiry time
 *
 * @param user The JWT subject
 */
function generateToken(user: { id: number }) {
  const payload = {
    id: user.id,
  };
  return jwt.sign(payload, jwtSecret, {
    algorithm: 'HS256',
    expiresIn: '2 weeks',
  });
}

/**
 * Updates the user token via a cookie
 */
function updateToken(res: Response, token: string) {
  res.cookie('knockoutJwt', token, {
    maxAge: ms('2 weeks'),
    httpOnly: true,
    secure: true,
    sameSite: 'none',
  });
}

/**
 * Creates a new external account entry for a user
 */
async function createExternalAccount(userId: number, externalProvider: string, externalId: string) {
  return knex('ExternalAccounts').insert({
    user_id: userId,
    provider: externalProvider,
    external_id: externalId,
  });
}

/**
 * Create a plain user without a username (pending setup)
 */
async function createUser(externalProvider: string, externalId: string) {
  const newUser = await User.create();

  await createExternalAccount(newUser.id, externalProvider, externalId);

  return newUser;
}

/**
 * Generate and send a token for the user associated with the external provider ID,
 * creating said user if it doesn't already exist
 *
 * @param res Optional response for setting the cookie
 * and sending the final redirect
 * @param ip param to log the IP of the user who is logging in
 * @param jwt Optional param for existing user JWT. Using this will associate the tokens user with
 * the external login provider
 */
async function loginWithExternalId(
  externalProvider: Provider,
  externalId: string,
  res: Response,
  ip: string,
  jwtString?: string,
  redirectUrl?: string
) {
  let user = await knex('Users as user')
    .select('user.id', 'username', 'avatar_url', 'background_url', 'role_id', 'deleted_at')
    .join('ExternalAccounts as acc', 'user.id', 'acc.user_id')
    .where('acc.provider', externalProvider)
    .where('acc.external_id', externalId)
    .first();

  if (jwtString) {
    let userId: number | null = null;
    try {
      // @ts-ignore
      const token: { id: number; iat: number; exp: number } = jwt.verify(jwtString, jwtSecret, {
        algorithms: ['HS256'],
      });
      userId = token.id;
    } catch (error) {
      // noop
    }

    if (userId && user?.id !== userId) {
      if (!user) {
        await createExternalAccount(userId, externalProvider, externalId);
        return res.send(
          `You can now use your ${externalProvider} account to log in to your Knockout account. You can close this window.`
        );
      }
      res.status(httpStatus.UNAUTHORIZED);
      return res.send('A user registered with this login method already exists.');
    }
  }

  if (!user) {
    const raidModeEnabled = await redis.get(RAID_MODE_KEY);

    if (raidModeEnabled) {
      return res.sendStatus(httpStatus.FORBIDDEN);
    }

    if (await new Scamalytics(ip).isRisky()) {
      console.log('VPN detected', ip);
      return res.sendStatus(httpStatus.FORBIDDEN);
    }

    console.log(`Creating new user for account ${externalProvider}:${externalId}`);
    user = (await createUser(externalProvider, externalId)).dataValues;
  }

  if (user.deleted_at) {
    res.status(httpStatus.UNAUTHORIZED);
    return res.send('Invalid account.');
  }

  const token = generateToken(user);

  if (res) {
    updateToken(res, token);

    const userObject = {
      id: user.id,
      username: user.username,
      avatar_url: user.avatar_url,
      background_url: user.background_url,
      role: await new RoleRetriever(user.role_id).getSingleObject(),
    };

    let authorizationCode;

    if (redirectUrl) {
      try {
        const { hostname } = new URL(redirectUrl);
        const authorizationJwt = jwt.sign({ id: user.id }, jwtSecret, {
          algorithm: 'HS256',
          expiresIn: '1 minute',
          audience: hostname,
        });
        authorizationCode = encodeURIComponent(authorizationJwt);
      } catch (error) {
        console.error(error);
      }
    }

    const authUrl =
      externalProvider === Provider.TWITTER ? `${config.CLIENT_URL}/login` : '/auth/finish';

    const queryAuthCode = querystring.stringify({ authorization: authorizationCode });

    const stringifiedUser = JSON.stringify(userObject);
    const base64User = encodeURIComponent(stringifiedUser);
    const queryString = querystring.stringify({ user: base64User });

    res.redirect(redirectUrl ? `${redirectUrl}?${queryAuthCode}` : `${authUrl}?${queryString}`);
  }

  // log IP
  IpAddress.create({
    ip_address: ip,
    user_id: user.id,
  }).catch((e) => console.warn(`Could not log IP for user ${user.id}: ${e}`));

  return { user, token };
}

export { generateToken, updateToken, createUser, loginWithExternalId };
