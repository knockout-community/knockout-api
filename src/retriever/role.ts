import { Role, PermissionCode } from 'knockout-schema';
import knex from '../services/knex';
import AbstractRetriever from './abstractRetriever';
import redis from '../services/redisClient';

export enum RoleFlag {
  INCLUDE_PERMISSION_CODES,
}

export default class RoleRetriever extends AbstractRetriever<Role> {
  protected cacheLifetime = 1800; // 30 minutes

  protected cachePrefix: string = 'role';

  private rolePermissionPrefix: string = 'rolePermission';

  private rolePermissionCacheLifetime = 604800;

  protected async query(ids: Array<number>) {
    return knex
      .from('Roles')
      .select(
        'id',
        'code as roleCode',
        'description as roleDescription',
        'created_at as roleCreatedAt',
        'updated_at as roleUpdatedAt'
      )
      .whereIn('id', ids);
  }

  protected format(data): Role {
    return {
      id: data.id,
      code: data.roleCode,
      description: data.roleDescription,
      permissionCodes: [],
      createdAt: data.roleCreatedAt,
      updatedAt: data.roleUpdatedAt,
    };
  }

  private async getPermissionCodes(roles: Role[]): Promise<Map<number, PermissionCode[]>> {
    if (!this.hasFlag(RoleFlag.INCLUDE_PERMISSION_CODES)) return new Map();
    const roleIds = roles.map((role) => role?.id).filter((id) => id !== null);
    const result = new Map<number, PermissionCode[]>();
    await Promise.all(
      roleIds.map(async (roleId) => {
        const cachedCodes = await redis.get(`${this.rolePermissionPrefix}-${roleId}`);
        if (cachedCodes) {
          result.set(roleId, JSON.parse(cachedCodes));
        } else {
          const permissionCodes = (
            await knex('Permissions as p')
              .select('p.code as code')
              .join('RolePermissions as rp', 'rp.permission_id', 'p.id')
              .where('rp.role_id', roleId)
          ).map((perm) => perm.code);

          result.set(roleId, permissionCodes);
          redis.setex(
            `${this.rolePermissionPrefix}-${roleId}`,
            this.rolePermissionCacheLifetime,
            JSON.stringify(permissionCodes)
          );
        }
      })
    );

    return result;
  }

  public invalidatePermissionCodes = async () => {
    redis.del(this.ids.map((id) => `${this.rolePermissionPrefix}-${id}`));
  };

  protected async populateData(roles: Role[]) {
    // grab data from related caches
    const permissionCodes = await this.getPermissionCodes(roles);

    // merge related data in
    const populatedRoles = roles.map((role) => {
      role.permissionCodes = permissionCodes.get(role.id) || [];
      return role;
    });

    return super.populateData(populatedRoles);
  }
}
