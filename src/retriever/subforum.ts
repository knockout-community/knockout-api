/* eslint-disable import/no-cycle */
import Sequelize from 'sequelize';
import { Subforum, ThreadWithLastPost } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import ThreadRetriever, { ThreadFlag } from './thread';
import { getUserPermissionCodes, userCanViewSubforum, userHiddenThreadIds } from '../helpers/user';
import nsfwTagName from '../constants/nsfwTagName';
import SubforumModel from '../models/subforum';
import Thread from '../models/thread';
import Post from '../models/post';

export enum SubforumFlag {
  INCLUDE_TOTAL_THREADS,
  INCLUDE_TOTAL_POSTS,
  INCLUDE_LAST_POST,
  HIDE_NSFW,
  LIMITED_USER,
}

export default class SubforumRetriever extends AbstractRetriever<Subforum> {
  protected cacheLifetime = 3600; // 1 hour

  protected cachePrefix: string = 'subforum';

  private threadCounterCachePrefix: string = 'subforum-thread-count';

  private threadCounterCacheLifetime = 43200; // 12 hours

  private postCounterCachePrefix: string = 'subforum-post-count';

  protected async query(ids: Array<number>) {
    return SubforumModel.findAll({
      where: { id: ids },
    });
  }

  private async getTotalThreads() {
    if (!this.hasFlag(SubforumFlag.INCLUDE_TOTAL_THREADS)) return {};

    const cachedThreadCounts = await this.cacheGet(this.threadCounterCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedThreadCounts);

    const threadCounts = await Thread.findAll({
      attributes: ['subforum_id', [Sequelize.fn('COUNT', Sequelize.col('id')), 'totalThreads']],
      where: {
        subforum_id: uncachedIds,
        deletedAt: { [Sequelize.Op.eq]: null },
      },
      group: ['subforum_id'],
    });

    const result = threadCounts.reduce((list, count) => {
      const threadSubforumId = count.subforum_id;
      list[threadSubforumId] = { totalThreads: (count.dataValues as any).totalThreads };
      this.cacheSet(
        this.threadCounterCachePrefix,
        threadSubforumId,
        list[threadSubforumId],
        this.threadCounterCacheLifetime
      );
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedThreadCounts.get(id) !== null) result[id] = cachedThreadCounts.get(id);
    });

    return result;
  }

  private async getTotalPosts() {
    if (!this.hasFlag(SubforumFlag.INCLUDE_TOTAL_POSTS)) return {};

    const cachedPostCounts = await this.cacheGet(this.postCounterCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedPostCounts);

    let postCounts: any[] = [];
    if (uncachedIds.length > 0) {
      postCounts = await Post.findAll({
        attributes: [[Sequelize.fn('COUNT', Sequelize.col('Post.id')), 'totalPosts']],
        include: [
          {
            model: Thread,
            attributes: ['subforum_id'],
            where: {
              subforum_id: uncachedIds,
              deleted_at: { [Sequelize.Op.eq]: null },
            },
          },
        ],
        group: ['subforum_id'],
      });
    }

    const result = postCounts.reduce((list, count) => {
      const postSubforumId = count.Thread.subforum_id;
      list[postSubforumId] = { totalPosts: count.dataValues.totalPosts };
      this.cacheSet(this.postCounterCachePrefix, postSubforumId, list[postSubforumId]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedPostCounts.get(id) !== null) result[id] = cachedPostCounts.get(id);
    });

    return result;
  }

  private async getLastPosts() {
    if (!this.hasFlag(SubforumFlag.INCLUDE_LAST_POST)) return {};

    const hideNsfw = this.hasFlag(SubforumFlag.HIDE_NSFW);

    const hiddenThreadIds = this.hasArgument('userId')
      ? await userHiddenThreadIds(this.args['userId'])
      : [];

    const lastThreadIds = await knex
      .raw(
        `SELECT
        t.id as threadId
      FROM (
        SELECT MAX(Threads.updated_at) as maxUpdatedAt
        FROM Threads
        LEFT OUTER JOIN ThreadTags tt
          ON Threads.id = tt.thread_id
        LEFT OUTER JOIN Tags tags
          ON tt.tag_id = tags.id
        WHERE Threads.deleted_at IS NULL AND Threads.subforum_id IN (${this.ids})
        ${hideNsfw ? ` AND coalesce(tags.name, '') != '${nsfwTagName}'` : ''}
        ${hiddenThreadIds.length > 0 ? ` AND Threads.id NOT IN (${hiddenThreadIds})` : ''}
        GROUP BY subforum_id
      ) AS latest_updated_threads
      INNER JOIN Threads t
        ON t.updated_at = latest_updated_threads.maxUpdatedAt`
      )
      .then((records) => records[0].map((record) => record.threadId));

    const lastThreads = (await new ThreadRetriever(
      lastThreadIds,
      [ThreadFlag.INCLUDE_LAST_POST, ThreadFlag.RETRIEVE_SHALLOW],
      { userId: this.args['userId'] }
    ).getObjectArray()) as ThreadWithLastPost[];

    const result = lastThreads.reduce((list, thread) => {
      const postSubforumId = thread.subforumId;
      list[postSubforumId] = {
        lastPost: { ...thread.lastPost, thread },
      };
      return list;
    }, {});

    return result;
  }

  protected async populateData(subforums) {
    // grab data from related caches
    const lastPosts = await this.getLastPosts();
    const totalThreads = await this.getTotalThreads();
    const totalPosts = await this.getTotalPosts();
    const userPermissionCodes = this.hasArgument('userId')
      ? await getUserPermissionCodes(this.args['userId'])
      : [];

    // merge related data in
    const populatedSubforums = subforums.reduce((list, subforum) => {
      if (this.hasArgument('userId') && !userCanViewSubforum(userPermissionCodes, subforum.id)) {
        return list;
      }

      subforum.lastPost = lastPosts[subforum.id]?.lastPost || {};
      subforum.totalThreads = totalThreads[subforum.id]?.totalThreads || 0;
      subforum.totalPosts = totalPosts[subforum.id]?.totalPosts || 0;

      return [...list, subforum];
    }, []);
    return super.populateData(populatedSubforums);
  }

  protected format(data) {
    return {
      id: data.id,
      name: data.name,
      description: data.description,
      icon: data.icon,
      createdAt: data.created_at,
      updatedAt: data.updated_at,
    };
  }
}
