import { Notification, NotificationType } from 'knockout-schema';
import { Transaction } from 'sequelize';
import AbstractRetriever from './abstractRetriever';
import ConversationRetriever, { ConversationFlag } from './conversation';
import PostRetriever, { PostFlag } from './post';
import ProfileCommentRetriever from './profileComment';
import NotificationModel from '../models/notification';

export default class NotificationRetriever extends AbstractRetriever<Notification> {
  protected cachePrefix: string = 'notification';

  private user?: number;

  private transaction?: Transaction;

  constructor(
    ids: Array<number> | number,
    user?: number,
    transaction?: Transaction,
    flags?: Array<number>,
    args?: Object
  ) {
    super(ids, flags, args);
    this.user = user;
    this.transaction = transaction;
  }

  protected async query(ids: Array<number>) {
    return NotificationModel.findAll({
      where: { id: ids },
      transaction: this.transaction,
      raw: true,
    });
  }

  protected format(data): Notification {
    const { dataId, ...notification } = data;
    return {
      ...notification,
      read: Boolean(notification.read),
      data: { id: dataId },
    };
  }

  private async getDataObjects(notifications: Notification[], userId: number) {
    const objects = {};
    await Promise.all(
      notifications.map((notification) =>
        (async () => {
          let object;
          switch (notification.type) {
            case NotificationType.MESSAGE:
              object = await new ConversationRetriever(notification.data.id, [
                ConversationFlag.RETRIEVE_SHALLOW,
              ]).getSingleObject();
              break;
            case NotificationType.POST_MENTION:
            case NotificationType.POST_REPLY:
              object = await new PostRetriever(
                notification.data.id,
                [
                  PostFlag.RETRIEVE_SHALLOW,
                  PostFlag.INCLUDE_USER,
                  PostFlag.INCLUDE_THREAD,
                  PostFlag.TRUNCATE_CONTENT,
                ],
                { userId, transaction: this.transaction }
              ).getSingleObject();
              break;
            case NotificationType.PROFILE_COMMENT:
              object = await new ProfileCommentRetriever(notification.data.id).getSingleObject();
              break;
            case NotificationType.REPORT_RESOLUTION:
              object = {};
              break;
            default:
              break;
          }
          objects[`${notification.type}-${notification.data.id}`] = object;
        })()
      )
    );
    return objects;
  }

  protected async populateData(notifications: Notification[]) {
    const dataObjects = await this.getDataObjects(notifications, this.user!);

    const populatedNotifications = notifications
      .filter((notification) =>
        Boolean(dataObjects[`${notification.type}-${notification.data.id}`])
      )
      .map((notification) => ({
        ...notification,
        data: dataObjects[`${notification.type}-${notification.data.id}`],
      }));

    return super.populateData(populatedNotifications);
  }
}
