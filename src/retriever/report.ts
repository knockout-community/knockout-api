import AbstractRetriever from './abstractRetriever';
import UserRetriever from './user';
import PostRetriever, { PostFlag } from './post';
import knex from '../services/knex';

export default class ReportRetriever extends AbstractRetriever {
  protected cachePrefix: string = 'report';

  protected async query(ids: Array<number>) {
    return knex('Reports')
      .select(
        'id',
        'post_id as postId',
        'reported_by as reportedBy',
        'report_reason as reportReason',
        'solved_by as solvedBy',
        'created_at as createdAt',
        'updated_at as updatedAt'
      )
      .whereIn('id', ids);
  }

  private static async getUsers(reports: Array<any>) {
    const userIds = reports.reduce((list, report) => {
      list.add(report.reportedBy);
      list.add(report.solvedBy);
      return list;
    }, new Set([]));

    return new UserRetriever(Array.from(userIds)).get();
  }

  private static async getPosts(reports: Array<any>) {
    const postIds = reports.map((report) => report.post);

    return new PostRetriever(postIds, [PostFlag.INCLUDE_THREAD]).get();
  }

  protected format(data): Object {
    return {
      id: data.id,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
      reason: data.reportReason,
      post: data.postId,
      reportedBy: data.reportedBy,
      solvedBy: data.solvedBy,
    };
  }

  protected async populateData(reports) {
    // grab data from related caches
    const users = await ReportRetriever.getUsers(reports);
    const posts = await ReportRetriever.getPosts(reports);

    // merge related data in
    const populatedReports = reports.map((report) => ({
      ...report,
      reportedBy: users.get(report.reportedBy) || null,
      solvedBy: users.get(report.solvedBy) || null,
      post: posts.get(report.post) || null,
    }));

    return super.populateData(populatedReports);
  }
}
