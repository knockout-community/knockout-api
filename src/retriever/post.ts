/* eslint-disable import/no-cycle */
import Sequelize from 'sequelize';
import { Post, User } from 'knockout-schema';
import AbstractRetriever from './abstractRetriever';
import UserRetriever, { UserFlag } from './user';
import ThreadRetriever, { ThreadFlag } from './thread';
import PostRatingRetriever, { PostRatingFlag } from './postRating';
import BanRetriever from './ban';
import { postsPerPage } from '../constants/pagination';
import { getCountryCode } from '../helpers/geoipLookup';
import { getUserPermissionCodes, userCanViewThread } from '../helpers/user';
import PostResponseRetriever from './postResponse';
import { postMentionFinder } from '../helpers/postMentionFinder';
import Ban from '../models/ban';
import PostEdit from '../models/postEdit';
import PostModel from '../models/post';

export enum PostFlag {
  INCLUDE_THREAD,
  RETRIEVE_SHALLOW,
  HIDE_DELETED,
  HIDE_NSFW,
  RETRIEVE_RATING_USERNAMES,
  INCLUDE_USER,
  TRUNCATE_CONTENT,
  INCLUDE_POST_RESPONSES,
  INCLUDE_POST_MENTION_USERS,
  INCLUDE_LAST_EDITED_USER,
}

export default class PostRetriever extends AbstractRetriever<Post> {
  protected cachePrefix: string = 'post';

  protected cacheLifetime = 10800; // 3 hours

  private lastEditedUserCachePrefix: string = 'postLastEditedUsers';

  private bansCachePrefix: string = 'postBans';

  protected async query(ids: Array<number>) {
    return PostModel.findAll({
      where: { id: ids },
      transaction: this.args['transaction'],
    });
  }

  private async getRatings(posts: Array<Post>) {
    if (this.hasFlag(PostFlag.RETRIEVE_SHALLOW)) return new Map();
    const postIds = posts.map((post) => post.id);
    const flags = this.hasFlag(PostFlag.RETRIEVE_RATING_USERNAMES)
      ? [PostRatingFlag.USE_USERNAMES]
      : [];
    return new PostRatingRetriever(postIds, flags).get();
  }

  private async getThreads(posts: Array<Post>) {
    if (!this.hasFlag(PostFlag.INCLUDE_THREAD)) return new Map();
    const threadIds = posts
      .map((post) => post.threadId)
      .filter((elem, index, self) => index === self.indexOf(elem));
    const flags = [ThreadFlag.INCLUDE_SUBFORUM, ThreadFlag.RETRIEVE_SHALLOW];

    if (this.hasFlag(PostFlag.HIDE_NSFW)) {
      flags.push(ThreadFlag.INCLUDE_TAGS);
    }

    return new ThreadRetriever(threadIds, flags).get();
  }

  private async getBans() {
    if (this.hasFlag(PostFlag.RETRIEVE_SHALLOW)) return {};

    // grab data from related caches
    const cachedBanIds = await this.cacheGet(this.bansCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedBanIds);

    // merge related data in

    let postBans: any[] = [];

    if (uncachedIds.length > 0) {
      postBans = await Ban.findAll({
        attributes: ['id', 'post_id'],
        where: {
          post_id: uncachedIds,
        },
        raw: true,
      });
    }
    const banIds = postBans.map((ban) => ban.id);

    // build a map of post id to a list of ban IDs
    const result = postBans.reduce((list, ban) => {
      if (typeof list[ban.post_id] === 'undefined') list[ban.post_id] = [];
      list[ban.post_id].push(ban.id);
      return list;
    }, {});

    // add cached ban IDs to the result and cache any new ones
    this.ids.forEach((id) => {
      if (cachedBanIds.get(id) !== null) {
        result[id] = cachedBanIds.get(id);
      } else {
        this.cacheSet(this.bansCachePrefix, id, result[id] || []);
      }
    });

    // convert result into map of post id to an array of ban objects
    // combine banIds and all values of cachedBanIds into a single array
    // to retrieve all bans in one query
    const allBanIds = banIds.concat(...Array.from(cachedBanIds.values()));
    const bans = await new BanRetriever(allBanIds).get();

    return Object.keys(result).reduce((list, postId) => {
      list[postId] = result[postId].map((banId) => bans.get(banId));
      return list;
    }, {});
  }

  private async getPostResponses(posts: Array<Post>) {
    if (!this.hasFlag(PostFlag.INCLUDE_POST_RESPONSES)) return new Map();
    const postIds = posts.map((post) => post.id);
    const responses = await new PostResponseRetriever(postIds).get();

    let responseIds: any[] = [];
    [...responses.values()].forEach((value) => {
      if (value.id) responseIds = responseIds.concat(value.responses);
    });

    const responsePosts = await new PostRetriever(
      responseIds.map((data) => data.responsePostId),
      [PostFlag.RETRIEVE_SHALLOW, PostFlag.INCLUDE_USER, PostFlag.INCLUDE_THREAD],
      { userId: this.args['userId'] }
    ).get();
    const output = new Map<number, Array<Post>>();
    postIds.forEach((id) => {
      output.set(
        id,
        responses.get(id)?.responses.reduce((responseList, response) => {
          if (responsePosts.has(response.responsePostId)) {
            responseList.push(responsePosts.get(response.responsePostId));
          }
          return responseList;
        }, []) ?? []
      );
    });
    return output;
  }

  private async getPostMentionUsers(posts: Array<Post>) {
    if (!this.hasFlag(PostFlag.INCLUDE_POST_MENTION_USERS)) return new Map();

    const postIds = posts.map((post) => post.id);
    let allMentionUserIds: number[] = [];
    const postMentionUserIds = new Map<number, Array<number>>();
    await Promise.all(
      posts.map(async (post) => {
        const userIds = postMentionFinder(post.content).filter(
          (mentionUserId) => mentionUserId !== (post.user as unknown as number)
        );
        allMentionUserIds.push(...userIds);
        postMentionUserIds.set(post.id, userIds);
      })
    );
    allMentionUserIds = Array.from(new Set(allMentionUserIds));
    const allMentionUsers = await new UserRetriever(allMentionUserIds).get();

    const output = new Map<number, Array<User>>();
    postIds.forEach((id) => {
      const mentionUserIds = postMentionUserIds.get(id);
      const mentionUsers: User[] = mentionUserIds
        ? mentionUserIds
            .map((userId) => allMentionUsers.get(userId))
            .filter((user): user is User => Boolean(user))
        : [];
      output.set(id, mentionUsers);
    });
    return output;
  }

  private async getLastEditedUsers() {
    if (!this.hasFlag(PostFlag.INCLUDE_LAST_EDITED_USER)) return {};

    const cachedLastEditedUsers = await this.cacheGet(this.lastEditedUserCachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedLastEditedUsers);

    let lastPostEdits: any[] = [];

    if (uncachedIds.length > 0) {
      lastPostEdits = await PostEdit.findAll({
        attributes: ['id', 'postId', 'userId'],
        where: {
          postId: uncachedIds,
          createdAt: {
            [Sequelize.Op.in]: [
              Sequelize.literal(
                `select MAX(created_at) FROM PostEdits WHERE post_id IN (:postIds)`
              ),
            ],
          },
        },
        replacements: {
          postIds: uncachedIds,
        },
        group: ['postId'],
        raw: true,
      });
    }

    const rawUsers = await new UserRetriever(
      lastPostEdits.map((postEdit) => postEdit.userId)
    ).get();

    const result = lastPostEdits.reduce((list, postEdit) => {
      list[postEdit.postId] = rawUsers.get(postEdit.userId);
      this.cacheSet(this.lastEditedUserCachePrefix, postEdit.postId, list[postEdit.postId]);
      return list;
    }, {});

    this.ids.forEach((id) => {
      if (cachedLastEditedUsers.get(id) !== null) {
        result[id] = cachedLastEditedUsers.get(id);
      }

      if (!result[id]) {
        this.cacheSet(this.lastEditedUserCachePrefix, id, {} as any);
        result[id] = {};
      }
    });
    return result;
  }

  protected format(data: PostModel): Post {
    return {
      id: data.id,
      threadId: data.thread_id,
      page: Math.ceil(data.thread_post_number / postsPerPage),
      content: data.content,
      createdAt: data.createdAt.toISOString(),
      updatedAt: data.updatedAt.toISOString(),
      userId: data.user_id,
      ratings: [],
      bans: [],
      threadPostNumber: data.thread_post_number,
      countryName: data.country_name!,
      countryCode: getCountryCode(data.country_name),
      appName: data.app_name!,
      distinguished: data.distinguished,
    };
  }

  protected async populateData(posts: Post[]) {
    // grab data from related caches
    const users =
      this.hasFlag(PostFlag.RETRIEVE_SHALLOW) && !this.hasFlag(PostFlag.INCLUDE_USER)
        ? new Map<number, User>()
        : await new UserRetriever(
            posts.map((post) => post.userId),
            [UserFlag.INCLUDE_ONLINE]
          ).get();
    const ratings = await this.getRatings(posts);
    const bans = await this.getBans();
    const threads = await this.getThreads(posts);
    const postResponses = await this.getPostResponses(posts);
    const postMentionUsers = await this.getPostMentionUsers(posts);
    const lastEditedUsers = await this.getLastEditedUsers();
    const userIdPresent = this.hasArgument('userId');
    const userPermissionCodes = userIdPresent
      ? await getUserPermissionCodes(this.args['userId'])
      : [];

    // merge related data in
    const postList = posts.reduce((list, post) => {
      const postThread = threads.get(post.threadId);
      if (this.hasFlag(PostFlag.HIDE_NSFW) && postThread === undefined) {
        return list;
      }

      // user must be able to see thread to see the post
      // only applies when we have the INCLUDE_THREAD flag
      if (
        this.hasFlag(PostFlag.INCLUDE_THREAD) &&
        userIdPresent &&
        !userCanViewThread(userPermissionCodes, postThread, this.args['userId'])
      ) {
        return list;
      }

      const MILLISECONDS_UNTIL_RATINGS_ARE_SHOWN = 300000;
      const postIsOldEnoughForRatings =
        new Date().getTime() - new Date(post.createdAt).getTime() >
        MILLISECONDS_UNTIL_RATINGS_ARE_SHOWN;

      const postContent = !this.hasFlag(PostFlag.TRUNCATE_CONTENT) ? post.content : '';

      let postRatings = ratings.get(post.id) || [];
      if (!postIsOldEnoughForRatings) {
        if (!userIdPresent) {
          postRatings = [];
        } else {
          let ratingUser;
          const userRatedRating = postRatings.find((rating) => {
            ratingUser = rating.users.find((user) => user.id === Number(this.args['userId']));
            return Boolean(ratingUser);
          });

          if (userRatedRating) {
            userRatedRating.users = [ratingUser];
            userRatedRating.count = 1;
            postRatings = [userRatedRating];
          } else {
            postRatings = [];
          }
        }
      }

      const lastEditedUser =
        lastEditedUsers[post.id] && Object.keys(lastEditedUsers[post.id]).length > 0
          ? lastEditedUsers[post.id]
          : undefined;

      return [
        ...list,
        {
          ...post,
          user: users.get(post.userId),
          ratings: postRatings,
          bans: bans[post.id] || [],
          thread: postThread,
          content: postContent,
          responses: postResponses.get(post.id),
          mentionUsers: postMentionUsers.get(post.id),
          lastEditedUser,
        },
      ];
    }, []);

    return super.populateData(postList);
  }

  async invalidateLastEditedUsers() {
    this.cacheDrop(this.lastEditedUserCachePrefix, this.ids);
  }

  async invalidateBans() {
    this.cacheDrop(this.bansCachePrefix, this.ids);
  }
}
