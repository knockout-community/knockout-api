import { Server } from 'socket.io';
import { ChainableCommander } from 'ioredis';
import redis from '../services/redisClient';
// eslint-disable-next-line import/no-cycle
import ThreadRetriever, { ThreadFlag } from '../retriever/thread';

const threadViewerPrefix = 'thread-viewers';
const threadRoomPrefix = 'thread';
const viewerCacheLifetime = 360;

const sessionPrefix = 'session';
const sessionCacheLifetime = 86400;

const POPULAR_THREAD_VIEWS_KEY = 'popular-threads-views';

const getRoomKey = (id) => `${threadRoomPrefix}-${id}`;
const getViewerKey = (id) => `${threadViewerPrefix}-${id}`;

export const getMemberCountKey = (id) => `${getViewerKey(id)}-members`;
export const getGuestCountKey = (id) => `${getViewerKey(id)}-guests`;
export const getPopularThreadViewsKey = (subforumId) => `${POPULAR_THREAD_VIEWS_KEY}-${subforumId}`;

export default async (io: Server, socket) => {
  const getSessionKey = (session, id) => `${sessionPrefix}-${session}-${id}`;
  const getSessionCount = async (session, id) => redis.get(getSessionKey(session, id));

  const expireAndEmit = async (key, room) => {
    const pipeline = redis.pipeline();
    pipeline.expire(key, viewerCacheLifetime);
    pipeline.get(key);
    const count = Number((await pipeline.exec())![1][1]);
    io.in(room).emit(`thread:${key.split('-').pop()}`, count);
  };

  const decrement = async (key) => {
    const value = await redis.decr(key);
    if (value < 0) {
      await redis.incr(key);
    }
  };

  const sessionId =
    socket.handshake.headers['cf-connecting-ip'] ??
    socket.handshake.headers['x-forwarded-for'] ??
    socket.handshake.address;

  const updatePopularThreads = async (id) => {
    const counts = await redis.mget([getMemberCountKey(id), getGuestCountKey(id)]);
    const totalViewers = Number(counts[0]) + Number(counts[1]);

    const threadRetriever = new ThreadRetriever(id, [
      ThreadFlag.RETRIEVE_SHALLOW,
      ThreadFlag.EXCLUDE_READ_THREADS,
    ]);

    const { subforumId } = await threadRetriever.getSingleObject();

    const cacheKey = getPopularThreadViewsKey(subforumId);

    const pipeline = redis.pipeline();
    pipeline.zadd(cacheKey, totalViewers, id);
    pipeline.zremrangebyrank(cacheKey, 0, -21);
    await pipeline.exec();
  };

  const joinThread = async (id) => {
    if (!id) return;
    await socket.join(getRoomKey(id));
    const pipeline = redis.pipeline();

    if (sessionId) await redis.incr(getSessionKey(sessionId, id));
    if (!sessionId || Number(await getSessionCount(sessionId, id)) <= 1) {
      const isNotInRoom = socket.request.user?.id
        ? await redis.sadd(getViewerKey(id), socket.request.user?.id)
        : true;

      if (isNotInRoom) {
        if (socket.request.user?.id) {
          await redis.incr(getMemberCountKey(id));
          expireAndEmit(getMemberCountKey(id), getRoomKey(id));
        } else {
          await redis.incr(getGuestCountKey(id));
          expireAndEmit(getGuestCountKey(id), getRoomKey(id));
        }
        updatePopularThreads(id);

        pipeline.expire(getViewerKey(id), viewerCacheLifetime);
      }
      pipeline.expire(getSessionKey(sessionId, id), sessionCacheLifetime);
    }

    await pipeline.exec();
  };

  const leaveThread = async (id, pipeline?: ChainableCommander) => {
    if (!id) return;
    socket.leave(getRoomKey(id));

    if (sessionId) {
      await decrement(getSessionKey(sessionId, id));
      if (Number(await getSessionCount(sessionId, id)) > 0) return;
    }

    const wasInRoom = socket.request.user?.id
      ? await redis.srem(getViewerKey(id), socket.request.user?.id)
      : true;

    if (wasInRoom) {
      if (socket.request.user?.id) {
        await decrement(getMemberCountKey(id));
        expireAndEmit(getMemberCountKey(id), getRoomKey(id));
      } else {
        await decrement(getGuestCountKey(id));
        expireAndEmit(getGuestCountKey(id), getRoomKey(id));
      }
    }

    updatePopularThreads(id);
    if (pipeline) {
      pipeline.expire(getViewerKey(id), viewerCacheLifetime);
    } else {
      redis.expire(getViewerKey(id), viewerCacheLifetime);
    }
  };

  const leaveAllThreads = async () => {
    const pipeline = redis.pipeline();

    socket.rooms.forEach(async (room: string) => {
      if (room.startsWith(threadRoomPrefix)) {
        const thread = room.split('-')[1];
        await leaveThread(thread, pipeline);
      }
    });
    await pipeline.exec();
  };

  socket.on('thread:join', joinThread);
  socket.on('thread:leave', leaveThread);
  socket.on('disconnecting', leaveAllThreads);
};

export const getViewers = async (id: number) => redis.smembers(getViewerKey(id));
export const getViewersPipeline = (id: number, client) => client.smembers(getViewerKey(id));
