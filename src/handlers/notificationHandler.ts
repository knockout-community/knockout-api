export const notificationsRoom = (userId: number) => `notifications:${userId}`;

export default async (socket) => {
  const joinNotifications = async () => {
    if (socket.request.user?.id) {
      socket.join(notificationsRoom(socket.request.user.id));
    }
  };

  const leaveNotifications = async () => {
    if (socket.request.user?.id) {
      socket.leave(notificationsRoom(socket.request.user.id));
    }
  };

  socket.on('notifications:join', joinNotifications);
  socket.on('notifications:leave', leaveNotifications);
};
