import { Server } from 'socket.io';
import { isUserInConversation } from '../controllers/conversationController';

const prefix = 'conversations';

export const ConversationRoom = (conversationId: number) => `${prefix}:${conversationId}`;

export default async (io: Server, socket) => {
  const joinConversation = async (conversationId: number) => {
    if (!socket.request.user?.id) return;
    const canJoinRoom = await isUserInConversation(conversationId, socket.request.user.id);
    if (canJoinRoom) {
      socket.join(ConversationRoom(conversationId));
    }
  };

  const leaveConversation = async (conversationId: number) => {
    if (socket.request.user?.id) {
      socket.leave(ConversationRoom(conversationId));
    }
  };

  const notifyUserTyping = (conversationId: number) => {
    io.in(ConversationRoom(conversationId)).emit(`${prefix}:userTyping`, {
      userId: socket.request.user?.id,
    });
  };

  const notifyUserStoppedTyping = (conversationId: number) => {
    io.in(ConversationRoom(conversationId)).emit(`${prefix}:userTypingStopped`, {
      userId: socket.request.user?.id,
    });
  };

  const sendMessage = (conversationId: number, content: string) => {
    io.in(ConversationRoom(conversationId)).emit(`${prefix}:message`, {
      userId: socket.request.user?.id,
      content,
    });
  };

  socket.on(`${prefix}:join`, ({ conversationId }) => joinConversation(conversationId));
  socket.on(`${prefix}:leave`, ({ conversationId }) => leaveConversation(conversationId));
  socket.on(`${prefix}:typing`, ({ conversationId }) => notifyUserTyping(conversationId));
  socket.on(`${prefix}:typingStopped`, ({ conversationId }) =>
    notifyUserStoppedTyping(conversationId)
  );
  socket.on(`${prefix}:message`, ({ conversationId, content }) =>
    sendMessage(conversationId, content)
  );
};
