import { DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';
import sequelize from './sequelize';

class ConversationUser extends Model<
  InferAttributes<ConversationUser>,
  InferCreationAttributes<ConversationUser>
> {
  declare conversation_id: number;

  declare user_id: number;
}

ConversationUser.init(
  {
    conversation_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      references: {
        model: 'Conversations',
        key: 'id',
      },
    },
    user_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
  },
  {
    timestamps: false,
    sequelize,
  }
);

export default ConversationUser;
