import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class SiteStat extends Model<InferAttributes<SiteStat>, InferCreationAttributes<SiteStat>> {
  declare id: CreationOptional<number>;

  declare fromDate: Date;

  declare toDate: Date;

  declare totalActiveUsers: number;

  declare totalBans: number;

  declare totalNewUsers: number;

  declare totalPosts: number;

  declare totalReports: number;

  declare totalThreads: number;

  declare createdAt: CreationOptional<Date>;
}

SiteStat.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true,
    },
    fromDate: {
      allowNull: false,
      type: DataTypes.DATE,
      field: 'from_date',
    },
    toDate: {
      allowNull: false,
      type: DataTypes.DATE,
      field: 'to_date',
    },
    totalActiveUsers: {
      allowNull: false,
      type: DataTypes.INTEGER,
      field: 'total_active_users',
    },
    totalBans: {
      allowNull: false,
      type: DataTypes.INTEGER,
      field: 'total_bans',
    },
    totalNewUsers: {
      allowNull: false,
      type: DataTypes.INTEGER,
      field: 'total_new_users',
    },
    totalPosts: {
      allowNull: false,
      type: DataTypes.INTEGER,
      field: 'total_posts',
    },
    totalReports: {
      allowNull: false,
      type: DataTypes.INTEGER,
      field: 'total_reports',
    },
    totalThreads: {
      allowNull: false,
      type: DataTypes.INTEGER,
      field: 'total_threads',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
  },
  {
    timestamps: true,
    updatedAt: false,
    sequelize,
  }
);

export default SiteStat;
