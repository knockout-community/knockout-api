import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class Rule extends Model<InferAttributes<Rule>, InferCreationAttributes<Rule>> {
  declare id: CreationOptional<number>;

  declare rulableType?: 'Subforum' | 'Thread';

  declare rulableId?: number;

  declare category: string;

  declare title: string;

  declare cardinality: number;

  declare description: string;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

Rule.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    rulableType: { type: DataTypes.STRING, field: 'rulable_type' },
    rulableId: { type: DataTypes.INTEGER, field: 'rulable_id' },
    category: DataTypes.STRING,
    title: DataTypes.STRING,
    cardinality: DataTypes.INTEGER,
    description: DataTypes.STRING,
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default Rule;
