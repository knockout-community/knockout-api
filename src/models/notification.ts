import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class Notification extends Model<
  InferAttributes<Notification>,
  InferCreationAttributes<Notification>
> {
  declare id: CreationOptional<number>;

  declare type: string;

  declare dataId: number;

  declare userId: number;

  declare read: CreationOptional<boolean>;

  declare createdAt: CreationOptional<Date>;
}

Notification.init(
  {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    type: DataTypes.STRING,
    dataId: { type: DataTypes.BIGINT, field: 'data_id' },
    userId: {
      type: DataTypes.BIGINT,
      field: 'user_id',
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    read: DataTypes.BOOLEAN,
    createdAt: { type: DataTypes.DATE, field: 'created_at', defaultValue: DataTypes.NOW },
  },
  {
    timestamps: false,
    sequelize,
  }
);

export default Notification;
