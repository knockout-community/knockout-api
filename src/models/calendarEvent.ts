import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class CalendarEvent extends Model<
  InferAttributes<CalendarEvent>,
  InferCreationAttributes<CalendarEvent>
> {
  declare id: CreationOptional<number>;

  declare createdBy: number;

  declare title: string;

  declare description: string;

  declare threadId: number;

  declare startsAt: Date;

  declare endsAt: Date;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

CalendarEvent.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      field: 'created_by',
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    threadId: {
      field: 'thread_id',
      allowNull: false,
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: 'Threads',
        key: 'id',
      },
    },
    startsAt: { type: DataTypes.DATE, field: 'starts_at' },
    endsAt: { type: DataTypes.DATE, field: 'ends_at' },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default CalendarEvent;
