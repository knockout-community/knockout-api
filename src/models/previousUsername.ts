import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class PreviousUsername extends Model<
  InferAttributes<PreviousUsername>,
  InferCreationAttributes<PreviousUsername>
> {
  declare user_id: number;

  declare username: string;

  declare createdAt: CreationOptional<Date>;
}

PreviousUsername.init(
  {
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    username: {
      type: DataTypes.STRING,
    },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
  },
  {
    timestamps: true,
    updatedAt: false,
    sequelize,
  }
);

export default PreviousUsername;
