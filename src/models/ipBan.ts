import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class IpBan extends Model<InferAttributes<IpBan>, InferCreationAttributes<IpBan>> {
  declare address?: string;

  declare range?: string;

  declare createdBy: number;

  declare updatedBy?: CreationOptional<number>;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt?: CreationOptional<Date>;
}

IpBan.init(
  {
    address: DataTypes.STRING,
    range: DataTypes.STRING,
    createdBy: { type: DataTypes.INTEGER, field: 'created_by' },
    updatedBy: { type: DataTypes.INTEGER, field: 'updated_by' },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default IpBan;
