import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  literal,
  Model,
  Op,
} from 'sequelize';
import sequelize from './sequelize';

const { ResourceAction, Resource, SubforumPermissionSuffixes } = require('knockout-schema');
const { actionableResourceIds } = require('../helpers/permissions');

class Thread extends Model<InferAttributes<Thread>, InferCreationAttributes<Thread>> {
  declare id: CreationOptional<number>;

  declare title: string;

  declare icon_id: number;

  declare user_id: number;

  declare subforum_id: number;

  declare locked: CreationOptional<boolean>;

  declare pinned: CreationOptional<boolean>;

  declare background_url?: string | null;

  declare background_type?: string | null;

  declare postCount: CreationOptional<number>;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;

  declare deletedAt?: Date | null;
}

Thread.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    title: DataTypes.STRING,
    icon_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    subforum_id: DataTypes.INTEGER,
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
    deletedAt: { type: DataTypes.DATE, field: 'deleted_at' },
    locked: { type: DataTypes.BOOLEAN, defaultValue: false },
    pinned: { type: DataTypes.BOOLEAN, defaultValue: false },
    background_url: DataTypes.STRING,
    background_type: DataTypes.STRING,
    postCount: { field: 'post_count', type: DataTypes.INTEGER },
  },
  {
    timestamps: true,
    whereMergeStrategy: 'and',
    sequelize,
    scopes: {
      notDeleted: {
        where: {
          deletedAt: null,
        },
      },
      notLocked: {
        where: {
          locked: false,
        },
      },
      byCreator(userId) {
        return {
          where: { user_id: userId },
        };
      },
      byUser(userId, userPermissionCodes, hiddenThreadIds = []) {
        const subforumIdsCanView = actionableResourceIds(
          ResourceAction.VIEW,
          Resource.SUBFORUM,
          userPermissionCodes
        );
        const subforumIdsCanViewDeletedThreads = actionableResourceIds(
          SubforumPermissionSuffixes.VIEW_DELETED_THREADS,
          Resource.SUBFORUM,
          userPermissionCodes
        );
        return {
          where: {
            [Op.and]: [
              {
                [Op.or]: [
                  { subforum_id: { [Op.in]: subforumIdsCanViewDeletedThreads } },
                  { deleted_at: null },
                ],
              },
              {
                [Op.or]: [
                  { subforum_id: { [Op.in]: subforumIdsCanView } },
                  { user_id: userId || null },
                ],
              },
            ],
            id: {
              [Op.notIn]: hiddenThreadIds,
            },
          },
        };
      },
      bySubforum(subforumId) {
        return {
          where: {
            subforum_id: subforumId,
          },
        };
      },
      searchByTitle(title) {
        return {
          where: literal('MATCH (title) AGAINST (:title IN NATURAL LANGUAGE MODE)'),
          replacements: {
            title,
          },
        };
      },
    },
  }
);

export default Thread;
