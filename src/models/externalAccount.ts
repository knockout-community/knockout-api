import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class ExternalAccount extends Model<
  InferAttributes<ExternalAccount>,
  InferCreationAttributes<ExternalAccount>
> {
  declare userId: number;

  declare provider: string;

  declare externalId: string;

  declare createdAt: CreationOptional<Date>;
}

ExternalAccount.init(
  {
    userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    provider: { type: DataTypes.STRING },
    externalId: { type: DataTypes.STRING, field: 'external_id' },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
  },
  {
    timestamps: true,
    updatedAt: false,
    sequelize,
  }
);

export default ExternalAccount;
