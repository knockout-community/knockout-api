import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class Tag extends Model<InferAttributes<Tag>, InferCreationAttributes<Tag>> {
  declare id: CreationOptional<number>;

  declare name: string;

  declare createdAt: CreationOptional<Date>;

  declare deletedAt: CreationOptional<Date>;
}

Tag.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    deletedAt: { type: DataTypes.DATE, field: 'deleted_at' },
  },
  {
    timestamps: false,
    sequelize,
  }
);

export default Tag;
