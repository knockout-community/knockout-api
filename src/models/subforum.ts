import {
  CreationOptional,
  DataTypes,
  Model,
  InferAttributes,
  InferCreationAttributes,
} from 'sequelize';
import sequelize from './sequelize';

class Subforum extends Model<InferAttributes<Subforum>, InferCreationAttributes<Subforum>> {
  declare id: CreationOptional<number>;

  declare name: string;

  declare icon: string;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;

  declare description: CreationOptional<string>;
}

Subforum.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    icon: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
      defaultValue: sequelize.literal('NOW()'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
      defaultValue: sequelize.literal('NOW()'),
    },
    description: DataTypes.STRING,
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default Subforum;
