import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class ThreadTag extends Model<InferAttributes<ThreadTag>, InferCreationAttributes<ThreadTag>> {
  declare tag_id: number;

  declare thread_id: number;

  declare createdAt: CreationOptional<Date>;

  declare deletedAt: CreationOptional<Date>;
}

ThreadTag.init(
  {
    tag_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: 'Tags',
        key: 'id',
      },
    },
    thread_id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: 'Threads',
        key: 'id',
      },
    },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    deletedAt: { type: DataTypes.DATE, field: 'deleted_at' },
  },
  {
    timestamps: false,
    sequelize,
  }
);

export default ThreadTag;
