import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class HiddenThread extends Model<
  InferAttributes<HiddenThread>,
  InferCreationAttributes<HiddenThread>
> {
  declare userId: number;

  declare threadId: number;

  declare createdAt: CreationOptional<Date>;
}

HiddenThread.init(
  {
    userId: {
      field: 'user_id',
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    threadId: {
      field: 'thread_id',
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: 'Threads',
        key: 'id',
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
  },
  {
    timestamps: false,
    sequelize,
  }
);

export default HiddenThread;
