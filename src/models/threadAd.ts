import {
  Model,
  InferAttributes,
  InferCreationAttributes,
  CreationOptional,
  DataTypes,
} from 'sequelize';
import sequelize from './sequelize';

// ThreadAd
// A ThreadAd is:
// - description: ad description (usually the thread name or flavor text)
// - query: a query that is used by a search function to lead to the thread
// - imageUrl: an image url that is associated with the ad
class ThreadAd extends Model<InferAttributes<ThreadAd>, InferCreationAttributes<ThreadAd>> {
  declare id: CreationOptional<number>;

  declare description: string;

  declare imageUrl: string;

  declare query: string;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

ThreadAd.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    imageUrl: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    query: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: 'ThreadAds',
    sequelize,
  }
);

export default ThreadAd;
