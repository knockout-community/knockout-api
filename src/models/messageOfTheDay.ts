import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class MessageOfTheDay extends Model<
  InferAttributes<MessageOfTheDay>,
  InferCreationAttributes<MessageOfTheDay>
> {
  declare id: CreationOptional<number>;

  declare message: string;

  declare buttonName?: string;

  declare buttonLink?: string;

  declare createdBy: number;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

MessageOfTheDay.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    buttonName: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    buttonLink: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    createdBy: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      field: 'created_by',
    },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default MessageOfTheDay;
