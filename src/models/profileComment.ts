import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class ProfileComment extends Model<
  InferAttributes<ProfileComment>,
  InferCreationAttributes<ProfileComment>
> {
  declare id: CreationOptional<number>;

  declare user_profile: number;

  declare author: number;

  declare content: string;

  declare deleted: CreationOptional<boolean>;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

ProfileComment.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    user_profile: {
      type: DataTypes.BIGINT,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    author: {
      type: DataTypes.BIGINT,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    content: DataTypes.TEXT,
    deleted: DataTypes.BOOLEAN,
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default ProfileComment;
