import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';
import { getLimitedUserRoleId } from '../helpers/role';

class User extends Model<InferAttributes<User>, InferCreationAttributes<User>> {
  declare id: CreationOptional<number>;

  declare username?: string;

  declare avatar_url?: string;

  declare background_url?: string;

  declare roleId: CreationOptional<number>;

  declare stripeCustomerId?: string | null;

  declare title?: string | null;

  declare pronouns?: string | null;

  declare donationUpgradeExpiresAt?: Date;

  declare lastOnlineAt?: Date;

  declare useBioForTitle?: boolean;

  declare showOnlineStatus?: boolean;

  declare disableIncomingMessages?: boolean;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

User.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    username: DataTypes.STRING,
    avatar_url: DataTypes.STRING,
    background_url: DataTypes.STRING,
    roleId: { type: DataTypes.INTEGER, field: 'role_id' },
    stripeCustomerId: { type: DataTypes.STRING, field: 'stripe_customer_id' },
    title: { type: DataTypes.STRING },
    pronouns: { type: DataTypes.STRING },
    donationUpgradeExpiresAt: { type: DataTypes.DATE, field: 'donation_upgrade_expires_at' },
    lastOnlineAt: { type: DataTypes.DATE, field: 'last_online_at' },
    useBioForTitle: { type: DataTypes.BOOLEAN, field: 'use_bio_for_title' },
    showOnlineStatus: { type: DataTypes.BOOLEAN, field: 'show_online_status' },
    disableIncomingMessages: { type: DataTypes.BOOLEAN, field: 'disable_incoming_messages' },
    createdAt: { type: DataTypes.DATE, field: 'created_at' },
    updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
  },
  {
    timestamps: true,
    sequelize,
    hooks: {
      async beforeCreate(user) {
        // if user has no role, assign role to limited user role
        if (user.roleId) {
          return;
        }

        user.roleId = await getLimitedUserRoleId();
      },
      // if the user's role_id is not the same as their
      // previous role_id, create a new previous user role record
      async afterSave(user) {
        // if the user has no role / previous role
        // or if the user's role was not changed,
        // do not run
        if (user.roleId && user.previous('roleId') && user.previous('roleId') !== user.roleId) {
          // create the previous user role record
          await sequelize.models.PreviousUserRole.create({
            role_id: user.previous('roleId'),
            user_id: user.id,
          });
        }

        if (
          user.username &&
          user.previous('username') &&
          user.previous('username') !== user.username
        ) {
          // create the previous user name record
          await sequelize.models.PreviousUsername.create({
            username: user.previous('username'),
            user_id: user.id,
          });
        }
      },
    },
  }
);

export default User;
