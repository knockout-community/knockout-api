import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class Ban extends Model<InferAttributes<Ban>, InferCreationAttributes<Ban>> {
  declare id: CreationOptional<number>;

  declare post_id?: number;

  declare user_id: number;

  declare banned_by: number;

  declare ban_reason: string;

  declare expires_at: Date;

  declare appealThreadCreated: CreationOptional<boolean>;

  declare createdAt: CreationOptional<Date>;

  declare updatedAt: CreationOptional<Date>;
}

Ban.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    post_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: 'Posts',
        key: 'id',
      },
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    banned_by: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id',
      },
    },
    ban_reason: {
      type: DataTypes.STRING,
    },
    expires_at: {
      type: DataTypes.DATE,
      field: 'expires_at',
    },
    appealThreadCreated: {
      field: 'appeal_thread_created',
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
  },
  {
    timestamps: true,
    sequelize,
  }
);

export default Ban;
