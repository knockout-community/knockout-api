import {
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
} from 'sequelize';
import sequelize from './sequelize';

class IpAddress extends Model<InferAttributes<IpAddress>, InferCreationAttributes<IpAddress>> {
  declare ip_address: string;

  declare user_id: number;

  declare post_id?: CreationOptional<number>;

  declare created_at: CreationOptional<Date>;
}

IpAddress.init(
  {
    ip_address: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    post_id: DataTypes.INTEGER.UNSIGNED,
    created_at: {
      type: DataTypes.DATE,
      field: 'created_at',
    },
  },
  {
    timestamps: false,
    sequelize,
  }
);

export default IpAddress;
