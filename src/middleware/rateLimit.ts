/* eslint-disable no-plusplus */
import DiscordWebhook from 'discord-webhook-ts';
import ms from 'ms';
import {
  RateLimiterMemory,
  IRateLimiterStoreOptions,
  RateLimiterRedis,
} from 'rate-limiter-flexible';
import validOrigins from '../../config/validOrigins';
import client from '../services/redisClient';

// used for POST/PUT
const rateLimiterAggressive = new RateLimiterMemory({
  points: 60,
  duration: 5,
  blockDuration: 120, // Block duration in store
  inmemoryBlockOnConsumed: 30, // If userId or IP consume >30 points per 5s
  inmemoryBlockDuration: 120,
} as IRateLimiterStoreOptions);

const rateLimiterUnauthorizedDomain = new RateLimiterMemory({
  points: 10,
  duration: 1,
  blockDuration: 10, // Block duration in store
  inmemoryBlockOnConsumed: 10, // If userId or IP consume >3 points per 5s
  inmemoryBlockDuration: 10,
} as IRateLimiterStoreOptions);

// used for POST/PUT from logged in users
const rateLimiterUserAggressive = new RateLimiterRedis({
  points: 3,
  duration: 60,
  blockDuration: 1800, // Block duration in store
  storeClient: client,
  inmemoryBlockOnConsumed: 3, // If userId or IP consume >3 points per 60s
  inmemoryBlockDuration: 1800,
} as IRateLimiterStoreOptions);

const discordNotify = async (user: { username: any; id: any }, newAccount: boolean) => {
  const discordClient = new DiscordWebhook(process.env.MODERATION_WEBHOOK!);

  const description = `${
    user.username
  } made three posts in the past 60 seconds, and is restricted from posting for the next ${
    newAccount ? 30 : 5
  } minutes.
User Profile: https://knockout.chat/user/${user.id}
Go get 'em, <@&551783790188691476>!
  `;

  await discordClient.execute({
    embeds: [
      {
        title: 'A user was flagged as a potential spammer!',
        description,
        color: 16527151,
      },
    ],
  });
};

export const rateLimiterUserMiddleware = async (req, res, next) => {
  if (Date.now() - new Date(req.user.createdAt).getTime() < ms('2 weeks')) {
    try {
      await rateLimiterUserAggressive.consume(req.user?.id);
      next();
    } catch (result) {
      if (result.consumedPoints > 0) {
        discordNotify(req.user, true);
      }
      res.status(429).send('Too Many Requests. Restricted.');
    }
  } else {
    next();
  }
};

const rateLimiterMiddleware = async (req, res, next) => {
  if (!validOrigins.includes(req.headers.origin)) {
    rateLimiterUnauthorizedDomain
      .consume(req.connection.remoteAddress)
      .then(() => {
        next();
      })
      .catch(() => {
        res.status(500).send('Application error. Please try again later');
      });
  } else if (req.method === 'POST' || req.method === 'PUT') {
    rateLimiterAggressive
      .consume(req.connection.remoteAddress)
      .then(() => {
        next();
      })
      .catch(() => {
        res.status(429).send('Too Many Requests. Banned.');
      });
  } else {
    next();
  }
};
export default rateLimiterMiddleware;
