import { NextFunction, Request, Response } from 'express';
import ipaddr from 'ipaddr.js';
import httpStatus from 'http-status';
import knex from '../services/knex';
import redis from '../services/redisClient';

const IP_BAN_TTL_SECONDS = 60 * 30;
export const IP_BAN_CACHE_KEY = 'ip-ban';
const RANGE_BAN_TTL_SECONDS = 60 * 60 * 12;
export const RANGE_BAN_CACHE_KEY = 'range-ban';

const exactMatch = async (ip: string) => {
  const matchKey = `${IP_BAN_CACHE_KEY}:${ip}`;
  const result = await redis.get(matchKey);
  if (typeof result === 'string') {
    return JSON.parse(result);
  }
  const results = await knex.from('IpBans').pluck('id').where('address', ip);
  const isMatch = typeof results[0] !== 'undefined';
  redis.setex(matchKey, IP_BAN_TTL_SECONDS, JSON.stringify(isMatch));
  return isMatch;
};

const rangeMatch = async (ip: string) => {
  let address;

  try {
    address = ipaddr.parse(ip);
  } catch (e) {
    console.error(`[ip-ban] Failed to match IP ${ip} against range: ${e.message}`);
    return false;
  }

  const matchKey = `${RANGE_BAN_CACHE_KEY}`;
  let ranges;
  const result = await redis.get(matchKey);
  if (typeof result === 'string') {
    ranges = JSON.parse(result);
  } else {
    ranges = await knex.from('IpBans').pluck('range').whereNotNull('range');
    redis.setex(matchKey, RANGE_BAN_TTL_SECONDS, JSON.stringify(ranges));
  }

  const matches = ranges
    .map((range) => {
      try {
        return address.match(ipaddr.parseCIDR(range));
      } catch (e) {
        return null;
      }
    })
    .filter((match) => match);

  return matches.length > 0;
};

const parseAddress = (ip: string) => {
  try {
    const address = ipaddr.IPv6.parse(ip);
    const mapped = address.isIPv4MappedAddress();
    return mapped ? address.toIPv4Address().toString() : address.toString();
  } catch (e) {
    return ip;
  }
};

export default async (req: Request, res: Response, next: NextFunction) => {
  if (typeof req.ipInfo !== 'undefined') {
    const ip = parseAddress(req.ipInfo);
    if ((await exactMatch(ip)) || (await rangeMatch(ip))) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ message: 'You are banned, go away' });
      return;
    }
  }

  next();
};
