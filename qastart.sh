#!/bin/bash

pm2() {
  sudo env PATH=/home/ec2-user/.yarn/bin:/home/ec2-user/.nvm/versions/node/v10.15.1/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/ec2-user/.local/bin:/home/ec2-user/bin NODE_ENV=development pm2 "$@"
}

pm2 stop notpunch-api
git pull
yarn install
yarn build
yarn sequelize db:migrate
pm2 start dist/index.js --name notpunch-api
