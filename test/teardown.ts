import { Sequelize } from 'sequelize';
import redis from '../src/services/redisClient';

const env = process.env.NODE_ENV || 'test';
// eslint-disable-next-line import/no-dynamic-require
const config = require('../config/config')[env];

export default async () => {
  redis.flushdb();
  const sequelize = new Sequelize(config.database, config.username, config.password, config);
  await sequelize.drop();
};
