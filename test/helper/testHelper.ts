import jwt from 'jsonwebtoken';
import { PermissionCode } from 'knockout-schema';
import request from 'supertest';

import { APP_PORT, JWT_SECRET } from '../../config/server';
import redis from '../../src/services/redisClient';
import Permission from '../../src/models/permission';
import RolePermission from '../../src/models/rolePermission';

const api = request(`http://localhost:${APP_PORT}/v2`);

export const getUserJwtString = (userId: number) =>
  jwt.sign({ id: userId }, JWT_SECRET, {
    algorithm: 'HS256',
    expiresIn: '2 weeks',
  });

const getUserJwtCookie = (userId: number) => `knockoutJwt=${getUserJwtString(userId)}`;

export const apiGet = (path: string, userId?: number) => {
  if (userId) {
    return api.get(path).set('Cookie', getUserJwtCookie(userId));
  }
  return api.get(path);
};

export const apiPost = (path: string, data: Object, userId?: number) => {
  if (userId) {
    return api.post(path).send(data).set('Cookie', getUserJwtCookie(userId));
  }
  return api.post(path).send(data);
};

export const apiPut = (path: string, data: Object, userId?: number) => {
  if (userId) {
    return api.put(path).send(data).set('Cookie', getUserJwtCookie(userId));
  }
  return api.put(path).send(data);
};

export const apiDelete = (path: string, userId?: number, data?: Object) => {
  if (userId) {
    return api.delete(path).send(data).set('Cookie', getUserJwtCookie(userId));
  }
  return api.delete(path).send(data);
};

export const addPermissionCodesToRole = async (
  roleId: number,
  permissionCodes: PermissionCode[]
) => {
  await Promise.all(
    permissionCodes.map(async (code) => {
      const [permission] = await Permission.findOrCreate({ where: { code } });
      await RolePermission.findOrCreate({
        where: { role_id: roleId, permission_id: permission.id },
      });
    })
  );
  return redis.flushall();
};

export const removePermissionCodesFromRole = async (
  roleId: number,
  permissionCodes: PermissionCode[]
) => {
  await Promise.all(
    permissionCodes.map(async (code) => {
      const [permission] = await Permission.findOrCreate({ where: { code } });
      await RolePermission.destroy({
        where: { role_id: roleId, permission_id: permission.id },
      });
    })
  );
  return redis.flushall();
};
