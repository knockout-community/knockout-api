import ms from 'ms';
import { banUser } from '../../src/controllers/banController';
import { initializeQueues, userQueue } from '../../src/jobs';
import { createUser } from '../factories';
import User from '../../src/models/user';

describe('banController', () => {
  let bannedUser: User;
  let banningUser: User;

  beforeAll(async () => {
    initializeQueues();
  });

  beforeEach(async () => {
    bannedUser = await createUser();
    banningUser = await createUser();
  });

  test('does not overwrite unban job if new ban has shorter duration than active ban', async () => {
    await banUser(2, bannedUser.id, 'thing', banningUser.id);

    const job = await userQueue.getJob(`unban-user-${bannedUser.id}`);
    expect(job?.opts.delay).toBeGreaterThan(ms('110 minutes'));

    await banUser(1, bannedUser.id, 'thing', banningUser.id);

    const newJob = await userQueue.getJob(`unban-user-${bannedUser.id}`);
    expect(newJob?.opts.delay).toBeGreaterThan(ms('110 minutes'));
  });

  test('overwrites unban job if new ban has longer duration than active ban', async () => {
    await banUser(1, bannedUser.id, 'thing', banningUser.id);

    const job = await userQueue.getJob(`unban-user-${bannedUser.id}`);
    expect(job?.opts.delay).toBeGreaterThan(ms('50 minutes'));

    await banUser(2, bannedUser.id, 'thing', banningUser.id);

    const newJob = await userQueue.getJob(`unban-user-${bannedUser.id}`);
    expect(newJob?.opts.delay).toBeGreaterThan(ms('110 minutes'));
  });
});
