import { getMockRes } from '@jest-mock/express';
import httpStatus from 'http-status';
import { v4 as uuidv4 } from 'uuid';
import { getUserJwtString } from '../../helper/testHelper';
import knex from '../../../src/services/knex';
import { loginWithExternalId, Provider } from '../../../src/controllers/auth/common';
import { createUser } from '../../factories';
import User from '../../../src/models/user';

describe('common', () => {
  describe('loginWithExternalId', () => {
    let user: User;
    const { res, mockClear } = getMockRes();

    beforeEach(async () => {
      user = await createUser();
      mockClear();
    });

    describe('when a user is not found with the given external account', () => {
      test('creates a user and redirects to the finish endpoint', async () => {
        const newProviderId = uuidv4();
        await loginWithExternalId(Provider.GOOGLE, newProviderId, res as any, '1.1.1.1');
        const newExternalAccount = await knex('ExternalAccounts')
          .orderBy('created_at', 'desc')
          .first();
        expect(newExternalAccount.provider).toBe(Provider.GOOGLE);
        expect(newExternalAccount.external_id).toBe(newProviderId);
        expect(res.redirect).toHaveBeenCalledWith(expect.stringContaining('/auth/finish'));
      });
    });

    describe('when a user is found with the given external account', () => {
      let providerId;

      beforeEach(async () => {
        providerId = uuidv4();
        await knex('ExternalAccounts').insert({
          user_id: user.id,
          provider: 'google',
          external_id: providerId,
        });
      });

      describe('and no jwtString is provided', () => {
        test('finds the user and redirects to the finish endpoint', async () => {
          await loginWithExternalId(Provider.GOOGLE, providerId, res as any, '1.1.1.1');
          expect(res.redirect).toHaveBeenCalledWith(expect.stringContaining('/auth/finish'));
        });
      });

      describe('and a jwtString is provided', () => {
        let jwtString: string;

        beforeEach(() => {
          jwtString = getUserJwtString(user.id);
        });

        test('creates a new external account when no user has been associated with it already', async () => {
          const newProviderId = uuidv4();
          await loginWithExternalId(
            Provider.GOOGLE,
            newProviderId,
            res as any,
            '1.1.1.1',
            jwtString
          );
          const newExternalAccount = await knex('ExternalAccounts')
            .where('external_id', newProviderId)
            .first();
          expect(newExternalAccount.user_id).toBe(user.id);
          expect(newExternalAccount.provider).toBe(Provider.GOOGLE);
          expect(res.send).toHaveBeenCalledWith(
            expect.stringContaining(`You can now use your ${Provider.GOOGLE} account`)
          );
        });

        describe('when a user has already been associated with the external account', () => {
          let usedProviderId;

          beforeEach(async () => {
            const newUser = await createUser();
            usedProviderId = uuidv4();
            await knex('ExternalAccounts').insert({
              user_id: newUser.id,
              provider: Provider.GOOGLE,
              external_id: usedProviderId,
            });
          });

          test('returns unauthorized', async () => {
            await loginWithExternalId(
              Provider.GOOGLE,
              usedProviderId,
              res as any,
              '1.1.1.1',
              jwtString
            );
            expect(res.status).toHaveBeenCalledWith(httpStatus.UNAUTHORIZED);
            expect(res.send).toHaveBeenCalledWith(
              expect.stringContaining('A user registered with this login method already exists.')
            );
          });
        });
      });
    });
  });
});
