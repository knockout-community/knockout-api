import httpStatus from 'http-status';

import { apiGet } from '../helper/testHelper';
import { createUser } from '../factories';
import { getModeratorLevelRoleIds } from '../../src/helpers/role';

describe('/v2/communityTeam endpoint', () => {
  let moderatorRoleIds;

  beforeEach(async () => {
    moderatorRoleIds = await getModeratorLevelRoleIds();
    await createUser({ roleId: moderatorRoleIds[0] });
  });

  describe('GET /', () => {
    test('returns a list of users on the moderator team', async () => {
      const response = await apiGet('/community-team').expect(httpStatus.OK);
      expect(response.body.every((user) => moderatorRoleIds.includes(user.role.id))).toBe(true);
    });
  });
});
