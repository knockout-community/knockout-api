import httpStatus from 'http-status';
import { DonationCheckoutSessionRequest } from 'knockout-schema';

import { apiGet, apiPost } from '../helper/testHelper';
import { createUser } from '../factories';
import deleteStripeCustomer from '../helper/deleteStripeCustomer';
import User from '../../src/models/user';

describe('/v2/donations endpoint', () => {
  let user: User;

  beforeEach(async () => {
    user = await createUser();
  });

  // delete any stripe test customers we have created for our test user
  afterEach(async () => {
    await deleteStripeCustomer(user.id);
  });

  describe('POST /checkout-session', () => {
    const data: DonationCheckoutSessionRequest = {
      successUrl: 'http://localhost:3000/success',
      cancelUrl: 'http://localhost:3000/failure',
    };

    test('rejects logged out users', async () => {
      await apiPost(`/donations/checkout-session`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves a checkout session id and associates a stripe customer with the user', async () => {
      expect(user.stripeCustomerId).toBeUndefined();

      await apiPost(`/donations/checkout-session`, data, user.id).expect(httpStatus.CREATED);

      await user.reload();
      expect(user.stripeCustomerId).toBeDefined();
    });
  });

  describe('GET /upgrade-expiration', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/donations/upgrade-expiration`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves the donation expiration time for the user', async () => {
      const expireTime = new Date();
      await user.update({ donationUpgradeExpiresAt: expireTime });
      await user.save();

      const response = await apiGet(`/donations/upgrade-expiration`, user.id).expect(httpStatus.OK);

      const expires = new Date(response.body.expiresAt).getTime();
      const diff = Math.abs(expireTime.getTime() - expires);
      expect(diff).toBeLessThan(10000);
    });
  });
});
