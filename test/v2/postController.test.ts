import httpStatus from 'http-status';
import os from 'os';
import {
  CreatePostRequest,
  UpdatePostRequest,
  RatePostRequest,
  RatingValue,
  NotificationType,
} from 'knockout-schema';
import knex from '../../src/services/knex';
import redis from '../../src/services/redisClient';
import Ripe from '../../src/services/ripe';
import 'reflect-metadata';

import { apiGet, apiPost, apiPut, apiDelete, addPermissionCodesToRole } from '../helper/testHelper';
import { RAID_MODE_KEY } from '../../src/constants/adminSettings';
import { pollySetup } from '../pollySetup';
import { IP_BAN_CACHE_KEY, RANGE_BAN_CACHE_KEY } from '../../src/middleware/ipBan';
import { getLimitedUserRoleId } from '../../src/helpers/role';
import {
  createSubforum,
  createRole,
  createUser,
  createThread,
  createPost,
  createPostResponse,
  createIspBan,
  createIpBan,
  createRating,
} from '../factories';
import PostRetriever, { PostFlag } from '../../src/retriever/post';
import PostResponse from '../../src/models/postResponse';
import User from '../../src/models/user';
import Thread from '../../src/models/thread';

describe('/v2/posts endpoint', () => {
  let subforum: { id: number };
  let thread: Thread;
  let role;
  let user: User;
  let post;

  beforeEach(async () => {
    subforum = await createSubforum();
    role = await createRole();
    user = await createUser({ roleId: role.id });
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
    post = await createPost({ user_id: user.id, thread_id: thread.id });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/posts/${post.id}`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves posts', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      await apiGet(`/posts/${post.id}`, user.id).expect(httpStatus.OK);
    });

    describe('when multiple users have rated the post', () => {
      let ratingUserOne: User;
      let ratingUserTwo: User;

      beforeEach(async () => {
        ratingUserOne = await createUser({ roleId: role.id });
        ratingUserTwo = await createUser({ roleId: role.id });
        createRating({ post_id: post.id, user_id: ratingUserOne.id, rating_id: 1 });
        createRating({ post_id: post.id, user_id: ratingUserTwo.id, rating_id: 1 });
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      });

      describe('and the post creation date is before the rating visibility theshold', () => {
        test('shows no ratings if the requesting user has not rated the post', async () => {
          const result = await apiGet(`/posts/${post.id}`, user.id);
          expect(result.body.ratings.length).toBe(0);
        });

        test('shows the users rating if the requesting user has rated the post', async () => {
          const response = await apiGet(`/posts/${post.id}`, ratingUserOne.id);
          const agreeRating = response.body.ratings.find((rating) => rating.ratingId === 1);
          expect(agreeRating.users.length).toBe(1);
          expect(agreeRating.users[0].id).toBe(ratingUserOne.id);
          expect(agreeRating.count).toBe(1);
        });
      });

      describe('and the post creation date is after the rating visibility threshold', () => {
        beforeEach(async () => {
          const postDate = new Date();
          postDate.setDate(postDate.getDate() - 2);
          post.changed('createdAt', true);
          post.set('createdAt', postDate, { raw: true });
          await post.save({ silent: true, fields: ['createdAt'] });
        });

        test('shows all ratings', async () => {
          const response = await apiGet(`/posts/${post.id}`, user.id);
          const agreeRating = response.body.ratings.find((rating) => rating.ratingId === 1);
          expect(agreeRating.users.length).toBe(2);
        });
      });
    });
  });

  describe('POST /', () => {
    let data: CreatePostRequest;

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-create`]);
      data = {
        content: 'Created post.',
        thread_id: thread.id,
      };
    });

    test('rejects users who cannot view the subforum', async () => {
      await apiPost('/posts/', data, user.id).expect(httpStatus.FORBIDDEN);
    });

    describe('for users who can view the subforum', () => {
      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      });

      test('creates a post', async () => {
        await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);
      });

      test('rejects logged out users', async () => {
        await apiPost('/posts/', data).expect(httpStatus.UNAUTHORIZED);
      });

      test('rejects limited users when raid mode is on', async () => {
        await redis.set(RAID_MODE_KEY, 'true');

        const limitedUser = await createUser();
        await apiPost('/posts/', data, limitedUser.id).expect(httpStatus.FORBIDDEN);
        await redis.del(RAID_MODE_KEY);
      }, 20000);

      describe('when the last thread post is by the same user', () => {
        let lastPost: any;

        beforeEach(async () => {
          lastPost = await createPost({ user_id: user.id, thread_id: thread.id });
        });

        test('when the last post is created less than 5 minutes ago, does not create post edits', async () => {
          await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);

          const postEdits = await knex('PostEdits').where('post_id', lastPost.id);
          expect(postEdits).toHaveLength(0);
        });

        test('merges into the last post when the last post is created less than 3 hours ago', async () => {
          // set last post creation date to 2 hours ago
          lastPost.set('createdAt', new Date(new Date().getTime() - 2 * 60 * 60 * 1000), {
            raw: true,
          });
          const newContent = 'Updated content.';
          data.content = newContent;
          await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);

          const mergedPost = await knex('Posts').first('content').where('id', lastPost.id);
          expect(mergedPost.content).toEqual(
            `${lastPost.content}\n\n[b]Edited:[/b]\n\n${newContent}`
          );
        });

        test('creates a new post when the last post is created more than 3 hours ago', async () => {
          // create a new thread to ensure we dont merge
          const otherThread = await createThread({ user_id: user.id, subforum_id: subforum.id });

          // create a new post created 4 hours ago
          await createPost({
            user_id: user.id,
            thread_id: otherThread.id,
            createdAt: new Date(new Date().getTime() - 4 * 60 * 60 * 1000),
          });
          data.content = 'Created post.';
          data.thread_id = otherThread.id;
          const response = await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);

          const newPost = await knex('Posts').first('content').where('id', response.body.id);
          expect(newPost.content).toEqual(data.content);
        });
      });

      test('rejects limited users who have posted in the last five minutes', async () => {
        // create post in new thread to ensure we dont merge
        // we can delete this once we have transactional tests
        // set the default async timeout here to 10s because we do two requests that can be
        // slow in the test environment
        const otherThread = await createThread({ user_id: user.id, subforum_id: subforum.id });
        const newRoleId = await getLimitedUserRoleId();
        await addPermissionCodesToRole(newRoleId, [
          `subforum-${subforum.id}-post-create`,
          `subforum-${subforum.id}-view`,
        ]);
        const otherUser = await createUser();
        data.thread_id = otherThread.id;
        await apiPost('/posts/', data, otherUser.id).expect(httpStatus.CREATED);
        await apiPost('/posts/', data, otherUser.id).expect(httpStatus.FORBIDDEN);
      }, 10000);

      test('creates new post mentions', async () => {
        const mentioningUser = await createUser({ roleId: role.id });
        const respondingPost = await createPost({
          user_id: mentioningUser.id,
          thread_id: thread.id,
        });
        data.content = `[quote mentionsUser="${mentioningUser.id}" postId="${respondingPost.id}"]test[/quote]`;
        const { body: createdPost } = await apiPost(`/posts/`, data, user.id).expect(
          httpStatus.CREATED
        );

        const notifs = await knex('Notifications')
          .where({
            type: NotificationType.POST_REPLY,
            user_id: mentioningUser.id,
            data_id: createdPost.id,
          })
          .orderBy('id', 'desc');

        expect(notifs.length).toBeGreaterThan(0);
      });

      test('creates new post responses', async () => {
        const mentioningUser = await createUser({ roleId: role.id });
        const respondingPost = await createPost({
          user_id: mentioningUser.id,
          thread_id: thread.id,
        });
        data.content = `[quote mentionsUser="${mentioningUser.id}" postId="${respondingPost.id}"]test[/quote]`;
        const { body: createdPost } = await apiPost(`/posts/`, data, user.id).expect(
          httpStatus.CREATED
        );

        const responses = await knex('PostResponses').where({
          original_post_id: respondingPost.id,
          response_post_id: createdPost.id,
        });

        expect(responses.length).toBeGreaterThan(0);
      });

      describe('when using Ripe to detect banned ISPs', () => {
        pollySetup();

        test('rejects banned ISPs', async () => {
          const ispBan = await createIspBan({
            createdBy: user.id,
            netname: 'M247-LTD-Frankfurt',
            asn: '9009',
          });

          const ip = '141.98.102.227';
          const isBanned = await new Ripe(ip).isBanned();
          const isParentBanned = await new Ripe(ip).isParentBanned();
          expect(isBanned).toBeTruthy();
          expect(isParentBanned).toBeTruthy();

          await ispBan.destroy();
        });
      });

      test('rejects specifically ip banned users', async () => {
        // figure out which network interfaces are available
        const interfaces = os.networkInterfaces();
        const interfaceNames = Object.keys(interfaces);
        const addresses = interfaceNames.reduce((list, interfaceName) => {
          const netInterface = interfaces[interfaceName];
          list = netInterface!.reduce((l, netAddress) => {
            l.push(netAddress.address);
            return l;
          }, list);
          return list;
        }, [] as string[]);
        // create ipBan records
        const bans = await Promise.all(
          addresses.map((address) =>
            createIpBan({
              address,
              createdBy: user.id,
            })
          )
        );
        // try to create a post
        await apiPost('/posts/', data, user.id).expect(httpStatus.FORBIDDEN);
        // clear up bans
        await Promise.all(
          bans.map(async (ban) => {
            await redis.del(`${IP_BAN_CACHE_KEY}:${ban.address}`);
            ban.destroy();
          })
        );
      });

      test('rejects ip range banned users', async () => {
        // figure out which network interfaces are available
        const interfaces = os.networkInterfaces();
        const interfaceNames = Object.keys(interfaces);
        const ranges = interfaceNames.reduce((list, interfaceName) => {
          const netInterface = interfaces[interfaceName];
          list = netInterface!.reduce((l, netAddress) => {
            l.push(netAddress.cidr!);
            return l;
          }, list);
          return list;
        }, [] as string[]);
        // create ipBan records
        const bans = await Promise.all(
          ranges.map((range) =>
            createIpBan({
              range,
              createdBy: user.id,
            })
          )
        );
        await redis.del(`${RANGE_BAN_CACHE_KEY}`);
        // try to create a post
        await apiPost('/posts/', data, user.id).expect(httpStatus.FORBIDDEN);
        // clear up bans
        await Promise.all(
          bans.map(async (ban) => {
            ban.destroy();
          })
        );
        await redis.del(`${RANGE_BAN_CACHE_KEY}`);
      });

      describe('for locked threads', () => {
        beforeEach(async () => {
          await knex('Threads').update({ locked: true }).where('id', thread.id);
        });

        test('blocks users without the subforum post bypass validations permission from posting', async () => {
          await apiPost('/posts/', data, user.id).expect(httpStatus.BAD_REQUEST);
        });

        test('allows users with the subforum post bypass validations permission to post', async () => {
          await addPermissionCodesToRole(role.id, [
            `subforum-${subforum.id}-post-bypass-validations`,
          ]);

          await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);
        });
      });

      describe('for deleted threads', () => {
        beforeEach(async () => {
          await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
          await knex('Threads').update({ deleted_at: new Date() }).where('id', thread.id);
        });

        test('blocks users without the subforum post bypass validations permission from posting', async () => {
          await apiPost('/posts/', data, user.id).expect(httpStatus.FORBIDDEN);
        });

        test('allows users with the correct permissions to post', async () => {
          await addPermissionCodesToRole(role.id, [
            `subforum-${subforum.id}-view-deleted-threads`,
            `subforum-${subforum.id}-post-bypass-validations`,
          ]);
          await apiPost('/posts/', data, user.id).expect(httpStatus.CREATED);
        });
      });
    });
  });

  describe('PUT /:id', () => {
    const data: UpdatePostRequest = {
      content: 'Updated.',
    };

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [
        `subforum-${subforum.id}-post-create`,
        `subforum-${subforum.id}-view`,
      ]);
    });

    afterEach(async () => {
      await PostResponse.destroy({ where: {} });
    });

    test('updates a post', async () => {
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const newPost = await knex('Posts').first('content').where('id', post.id);
      expect(newPost.content).toEqual(data.content);
    });

    test('before the post edit buffer, creates a post edit record for the original post and the edit', async () => {
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const postEdits = await knex('PostEdits').where('post_id', post.id).orderBy('id', 'desc');
      expect(postEdits[0]).toBeUndefined();
    });

    test('after the post edit buffer, creates a post edit record for the original post and the edit by the editing user', async () => {
      const editingRole = await createRole();
      await addPermissionCodesToRole(editingRole.id, [
        `subforum-${subforum.id}-post-create`,
        `subforum-${subforum.id}-post-update`,
      ]);
      const editingUser = await createUser({ roleId: editingRole.id });

      post.changed('createdAt', true);
      post.set('createdAt', new Date('2000-01-01'), { raw: true });
      await post.save({ silent: true, fields: ['createdAt'] });

      await apiPut(`/posts/${post.id}`, data, editingUser.id).expect(httpStatus.OK);

      const postEdits = await knex('PostEdits').where('post_id', post.id).orderBy('id', 'desc');
      expect(postEdits[0].content).toEqual(data.content);
      expect(postEdits[1].content).toEqual(post.content);

      const newPost: any = await new PostRetriever(post.id, [
        PostFlag.INCLUDE_LAST_EDITED_USER,
      ]).getSingleObject();
      expect(newPost.lastEditedUser.id).toEqual(editingUser.id);
    });

    test('creates new post mentions', async () => {
      const mentioningUser = await createUser({ roleId: role.id });
      data.content = `[quote mentionsUser="${mentioningUser.id}"]test[/quote]`;
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const notifs = await knex('Notifications')
        .where({ type: NotificationType.POST_REPLY, user_id: mentioningUser.id, data_id: post.id })
        .orderBy('id', 'desc');

      expect(notifs.length).toBeGreaterThan(0);
    });

    test('creates new post responses', async () => {
      const mentioningUser = await createUser({ roleId: role.id });
      const respondingPost = await createPost({
        user_id: mentioningUser.id,
        thread_id: thread.id,
      });
      data.content = `[quote mentionsUser="${mentioningUser.id}" postId="${respondingPost.id}"]test[/quote]`;
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const responses = await knex('PostResponses').where({
        original_post_id: respondingPost.id,
        response_post_id: post.id,
      });

      expect(responses.length).toBeGreaterThan(0);
    });

    test('removes invalid post responses', async () => {
      const mentioningUser = await createUser({ roleId: role.id });
      const respondingPost = await createPost({
        user_id: mentioningUser.id,
        thread_id: thread.id,
      });
      await post.update({
        content: `[quote mentionsUser="${mentioningUser.id}" postId="${respondingPost.id}"]test[/quote]`,
      });
      await createPostResponse({ originalPostId: respondingPost.id, responsePostId: post.id });

      data.content = 'helo';
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const responses = await knex('PostResponses').where({
        original_post_id: respondingPost.id,
        response_post_id: post.id,
      });

      expect(responses.length).toEqual(0);
    });

    test('rejects logged out users', async () => {
      await apiPut(`/posts/${post.id}`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('does not let non creators without the subforum post-update permission to update posts', async () => {
      const otherUser = await createUser();
      await apiPut(`/posts/${post.id}`, data, otherUser.id).expect(httpStatus.FORBIDDEN);
    });

    test('lets non creators with the subforum post-update permission to update posts', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-update`]);
      await apiPut(`/posts/${post.id}`, data, user.id).expect(httpStatus.OK);

      const newPost = await knex('Posts').first('content').where('id', post.id);
      expect(newPost.content).toEqual(data.content);
    });
  });

  describe('PUT /:id/ratings', () => {
    const data: RatePostRequest = {
      rating: RatingValue.IDEA,
    };
    let otherUser: User;

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-post-rating-create`]);
      otherUser = await createUser({ roleId: role.id });
      await post.update({ user_id: otherUser.id });
    });

    test('rates a post', async () => {
      await apiPut(`/posts/${post.id}/ratings`, data, user.id).expect(httpStatus.OK);
    });

    test('rejects logged out users', async () => {
      await apiPut(`/posts/${post.id}/ratings`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('does not let users rate their own posts', async () => {
      await apiPut(`/posts/${post.id}/ratings`, data, otherUser.id).expect(httpStatus.BAD_REQUEST);
    });

    test('rejects invalid posts', async () => {
      await apiPut(`/posts/-1/ratings`, data, user.id).expect(httpStatus.INTERNAL_SERVER_ERROR);
    });
  });

  describe('DELETE /:id/ratings', () => {
    test('unrates a post', async () => {
      const otherUser = await createUser();
      await post.update({ user_id: otherUser.id });
      await apiDelete(`/posts/${post.id}/ratings`, user.id).expect(httpStatus.OK);
    });

    test('rejects logged out users', async () => {
      await apiDelete(`/posts/${post.id}/ratings`).expect(httpStatus.UNAUTHORIZED);
    });

    test('does not let users unrate their own posts', async () => {
      await apiDelete(`/posts/${post.id}/ratings`, user.id).expect(httpStatus.BAD_REQUEST);
    });

    test('rejects invalid posts', async () => {
      await apiDelete(`/posts/-1/ratings`, user.id).expect(httpStatus.INTERNAL_SERVER_ERROR);
    });
  });
});
