import httpStatus from 'http-status';
import { NotificationType, MarkNotificationsAsReadRequest } from 'knockout-schema';
import knex from '../../src/services/knex';

import { addPermissionCodesToRole, apiGet, apiPut } from '../helper/testHelper';
import {
  createUser,
  createSubforum,
  createThread,
  createPost,
  createNotification,
  createConversation,
  createMessage,
  createRole,
} from '../factories';
import { createNotification as addNewNotification } from '../../src/controllers/notificationController';
import Notification from '../../src/models/notification';
import User from '../../src/models/user';

describe('/v2/notifications endpoint', () => {
  let user: User;
  let user1: User;
  let subforum;
  let thread;
  let post;
  let notification;
  let notification1;
  let conversation;
  let message;
  let role;

  beforeEach(async () => {
    await Notification.destroy({ where: {} });
    role = await createRole();
    user = await createUser({ roleId: role.id });
    user1 = await createUser();
    subforum = await createSubforum();
    thread = await createThread({ user_id: user1.id, subforum_id: subforum.id });
    post = await createPost({ user_id: user1.id, thread_id: thread.id });
    notification = await createNotification({
      userId: user.id,
      type: NotificationType.POST_REPLY,
      dataId: post.id,
    });
    conversation = await createConversation([user.id, user1.id]);
    message = await createMessage({ user_id: user1.id, conversation_id: conversation.id });
    await conversation.update({ latest_message_id: message.id });
    notification1 = await createNotification({
      userId: user.id,
      type: NotificationType.MESSAGE,
      dataId: conversation.id,
    });
  });

  describe('GET /', () => {
    test('rejects logged out users', async () => {
      await apiGet(`/notifications`).expect(httpStatus.UNAUTHORIZED);
    });

    test('retrieves notifications', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      const response = await apiGet(`/notifications`, user.id).expect(httpStatus.OK);

      expect(response.body.length).toBe(2);
      expect(response.body[0].id).toBe(notification.id);
      expect(response.body[0].type).toBe(NotificationType.POST_REPLY);
      expect(response.body[0].data.id).toBe(post.id);

      expect(response.body[1].id).toBe(notification1.id);
      expect(response.body[1].type).toBe(NotificationType.MESSAGE);
      expect(response.body[1].data.id).toBe(conversation.id);
      expect(response.body[1].data.messages[0].content).toBe(message.content);
    });

    test('hides notifications from threads the user cannot see', async () => {
      await thread.update({ deletedAt: new Date() });
      const response = await apiGet(`/notifications`, user.id).expect(httpStatus.OK);

      expect(response.body.length).toBe(1);
      expect(response.body[0].id).toBe(notification1.id);
      expect(response.body[0].type).toBe(NotificationType.MESSAGE);
      expect(response.body[0].data.id).toBe(conversation.id);
      expect(response.body[0].data.messages[0].content).toBe(message.content);
    });

    test('notifications are moved to the top of the list when updated', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      const response = await apiGet(`/notifications`, user.id).expect(httpStatus.OK);

      expect(response.body.length).toBe(2);
      expect(response.body[0].id).toBe(notification.id);
      expect(response.body[0].type).toBe(NotificationType.POST_REPLY);
      expect(response.body[0].data.id).toBe(post.id);

      expect(response.body[1].id).toBe(notification1.id);
      expect(response.body[1].type).toBe(NotificationType.MESSAGE);
      expect(response.body[1].data.id).toBe(conversation.id);
      expect(response.body[1].data.messages[0].content).toBe(message.content);

      await new Promise((resolve) => {
        setTimeout(resolve, 2000);
      });
      await addNewNotification(NotificationType.MESSAGE, user.id, conversation.id);

      const response1 = await apiGet(`/notifications`, user.id).expect(httpStatus.OK);

      expect(response1.body.length).toBe(2);
      expect(response1.body[0].id).toBe(notification1.id);
      expect(response1.body[0].type).toBe(NotificationType.MESSAGE);
    });
  });

  describe('PUT /', () => {
    let data: MarkNotificationsAsReadRequest;

    beforeEach(() => {
      data = { notificationIds: [notification.id] };
    });

    test('rejects logged out users', async () => {
      await apiPut(`/notifications`, data).expect(httpStatus.UNAUTHORIZED);
    });

    test('marks a single notification as read', async () => {
      await apiPut('/notifications', data, user.id).expect(httpStatus.OK);

      const notifications = await knex('Notifications').select('read').where({
        id: notification.id,
        user_id: user.id,
      });

      expect(notifications[0].read).toBeTruthy();
    });

    test('marks multiple notifications as read', async () => {
      await apiPut(
        '/notifications',
        { ...data, notificationIds: [notification.id, notification1.id] },
        user.id
      ).expect(httpStatus.OK);

      const notifications = await knex('Notifications').select().where({
        user_id: user.id,
      });

      expect(notifications.length).toBe(2);
      expect(notifications[0].read).toBeTruthy();
      expect(notifications[1].read).toBeTruthy();
    });
  });

  describe('PUT /all', () => {
    test('rejects logged out users', async () => {
      await apiPut(`/notifications/all`, {}).expect(httpStatus.UNAUTHORIZED);
    });

    test('marks all notifications as read', async () => {
      await apiPut('/notifications/all', {}, user.id).expect(httpStatus.OK);
      const notifications = await knex('Notifications')
        .select()
        .where({
          user_id: user.id,
        })
        .orderBy('id', 'desc');
      expect(notifications.length).toBe(2);
      expect(Boolean(notifications[0].read)).toBe(true);
      expect(Boolean(notifications[1].read)).toBe(true);
    });
  });
});
