import httpStatus from 'http-status';
import {
  NewThread,
  UpdateThreadRequest,
  UpdateThreadTagsRequest,
  ThreadMetadata,
  ThreadWithPosts,
} from 'knockout-schema';
import { RAID_MODE_KEY } from '../../src/constants/adminSettings';
import { addPermissionCodesToRole, apiGet, apiPost, apiPut } from '../helper/testHelper';
import knex from '../../src/services/knex';
import redis from '../../src/services/redisClient';

import { getGuestUserRoleId } from '../../src/helpers/role';
import {
  createRole,
  createUser,
  createSubforum,
  createThread,
  createPost,
  createPostEdit,
  createPostResponse,
  createTag,
} from '../factories';
import User from '../../src/models/user';
import Thread from '../../src/models/thread';
import ThreadTag from '../../src/models/threadTag';

describe('/v2/threads endpoint', () => {
  let user: User;
  let role;
  let subforum: { id: number; name: string };
  let thread: Thread;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    subforum = await createSubforum();
    thread = await createThread({ user_id: user.id, subforum_id: subforum.id });
  });

  describe('POST /', () => {
    let data;

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-create`]);

      data = {
        title: 'A Thread',
        content: 'thread post content',
        icon_id: 1,
        subforum_id: subforum.id,
      };
    });

    test('creates a thread', async () => {
      let expectedResponse;

      const response = await apiPost('/threads', data, user.id)
        .expect((res: { body: NewThread }) => {
          expectedResponse = {
            id: res.body.id,
            title: data.title,
            iconId: data.icon_id,
            userId: user.id,
            subforumId: subforum.id,
            createdAt: res.body.createdAt,
            updatedAt: res.body.updatedAt,
            deletedAt: null,
            locked: false,
            pinned: false,
          };
          expectedResponse.id = res.body.id;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED);

      expect(response.body).toMatchObject(expectedResponse!);
    });

    test('creates a thread with tags', async () => {
      const tag = await createTag();
      data.tag_ids = [tag.id];

      const response = await apiPost('/threads', data, user.id).expect(httpStatus.CREATED);

      const newThread = await Thread.findOne({ where: { id: response.body.id } });

      const threadTag = await ThreadTag.findOne({
        where: { thread_id: newThread!.id, tag_id: tag.id },
      });

      expect(threadTag).toBeTruthy();

      // destroy the thread tag so it doesn't interfere with other tests
      await threadTag!.destroy();

      // destroy the tag
      await tag.destroy();
    });

    test('creates a thread with a post response in the content', async () => {
      const mentioningUser = await createUser();
      const respondingPost = await createPost({
        user_id: mentioningUser.id,
        thread_id: thread.id,
      });
      data.content = `[quote mentionsUser="${mentioningUser.id}" postId="${respondingPost.id}"]test[/quote]`;
      await apiPost('/threads', data, user.id).expect(httpStatus.CREATED);
    });

    test('rejects an invalid user', async () => {
      await knex('Users').update('username', null).where('id', user.id);
      await apiPost('/threads', data, user.id).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects a logged out user', async () => {
      await apiPost('/threads', data).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects invalid thread content', async () => {
      data.content = '';
      await apiPost('/threads', data, user.id).expect(httpStatus.BAD_REQUEST);
    });

    test('rejects invalid thread title', async () => {
      data.title = '';
      await apiPost('/threads', data, user.id).expect(httpStatus.BAD_REQUEST);
    });

    test('blocks users without the correct permission from adding backgrounds', async () => {
      data.background_type = 'cover';
      data.background_url = 'none.webp';
      await apiPost('/threads', data, user.id).expect(httpStatus.FORBIDDEN);
    });

    test('allows users with the correct permission to add backgrounds', async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-background-update`]);

      data.background_type = 'cover';
      data.background_url = 'none.webp';

      let expectedResponse;

      const response = await apiPost('/threads', data, user.id)
        .expect((res: { body: NewThread }) => {
          expectedResponse = {
            id: res.body.id,
            title: 'A Thread',
            iconId: 1,
            userId: user.id,
            subforumId: subforum.id,
            createdAt: res.body.createdAt,
            updatedAt: res.body.updatedAt,
            deletedAt: null,
            locked: false,
            pinned: false,
            backgroundType: 'cover',
            backgroundUrl: 'none.webp',
          };
        })
        .expect(httpStatus.CREATED);

      expect(response.body).toMatchObject(expectedResponse!);
    });

    describe('blocks new users from creating threads', () => {
      test('when raid mode is on', async () => {
        const newUser = await createUser();
        await redis.set(RAID_MODE_KEY, 'true');
        await apiPost('/threads', data, newUser.id).expect(httpStatus.FORBIDDEN);
        await redis.del(RAID_MODE_KEY);
      }, 10000);
    });
  });

  describe('GET /:id/page?', () => {
    let post;

    beforeEach(async () => {
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view`]);
      await addPermissionCodesToRole(await getGuestUserRoleId(), [`subforum-${subforum.id}-view`]);
      post = await createPost({
        user_id: user.id,
        thread_id: thread.id,
      });
    });

    test('retrieves a thread', async () => {
      const expectedResponse = {
        id: thread.id,
        title: thread.title,
        iconId: 1,
        subforumId: subforum.id,
        deleted: false,
        locked: false,
        pinned: false,
        lastPost: {
          id: post.id,
          threadId: thread.id,
          page: 1,
          content: '',
          user: {
            id: user.id,
          },
          ratings: [],
          bans: [],
          threadPostNumber: 1,
          countryName: null,
          appName: 'knockout.chat',
        },
        backgroundUrl: '',
        backgroundType: '',
        user: {
          id: user.id,
          username: user.username,
          avatarUrl: 'none.webp',
          backgroundUrl: '',
          banned: false,
          isBanned: false,
          title: null,
        },
        postCount: 1,
        recentPostCount: 0,
        subforum: {
          id: subforum.id,
        },
        tags: [],
        totalPosts: 1,
        currentPage: 1,
        posts: [
          {
            id: post.id,
            threadId: thread.id,
            page: 1,
            content: post.content,
            user: {
              id: user.id,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 1,
            countryName: null,
            appName: 'knockout.chat',
          },
        ],
      };

      const response = await apiGet(`/threads/${thread.id}`, user.id).expect(httpStatus.OK);

      expect(response.body).toMatchObject(expectedResponse);
    });

    test("retrieves a post's responses", async () => {
      const user1 = await createUser({ roleId: role.id });
      const post2 = await createPost({
        user_id: user1.id,
        thread_id: thread.id,
      });
      await createPostResponse({ originalPostId: post.id, responsePostId: post2.id });

      const response: { body: ThreadWithPosts } = await apiGet(
        `/threads/${thread.id}`,
        user.id
      ).expect(httpStatus.OK);

      expect(response.body.posts).toHaveLength(2);
      expect(response.body.posts[0].responses).toHaveLength(1);
      expect(response.body.posts[0].responses?.[0].id).toBe(post2.id);
    });

    test("does not retreive a post's responses that are not viewable to the current user", async () => {
      const user1 = await createUser({ roleId: role.id });

      const subforum1 = await createSubforum();
      const thread2 = await createThread({ user_id: user1.id, subforum_id: subforum1.id });
      const post2 = await createPost({
        user_id: user1.id,
        thread_id: thread2.id,
      });

      await createPostResponse({ originalPostId: post.id, responsePostId: post2.id });

      const response: { body: ThreadWithPosts } = await apiGet(
        `/threads/${thread.id}`,
        user.id
      ).expect(httpStatus.OK);

      expect(response.body.posts).toHaveLength(1);
      expect(response.body.posts[0].responses).toHaveLength(0);
    });

    test('retrieves a thread for a logged-out user', async () => {
      const expectedResponse = {
        id: thread.id,
        title: thread.title,
        iconId: 1,
        subforumId: subforum.id,
        deleted: false,
        locked: false,
        pinned: false,
        lastPost: {
          id: post.id,
          threadId: thread.id,
          page: 1,
          content: '',
          user: {
            id: user.id,
          },
          ratings: [],
          bans: [],
          threadPostNumber: 1,
          countryName: null,
          appName: 'knockout.chat',
        },
        backgroundUrl: '',
        backgroundType: '',
        user: {
          id: user.id,
          username: user.username,
          avatarUrl: 'none.webp',
          backgroundUrl: '',
          banned: false,
          isBanned: false,
          title: null,
        },
        postCount: 1,
        recentPostCount: 0,
        subforum: {
          id: subforum.id,
        },
        tags: [],
        totalPosts: 1,
        currentPage: 1,
        posts: [
          {
            id: post.id,
            threadId: thread.id,
            page: 1,
            content: post.content,
            user: {
              id: user.id,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 1,
            countryName: null,
            appName: 'knockout.chat',
          },
        ],
      };

      const response = await apiGet(`/threads/${thread.id}`).expect(httpStatus.OK);

      expect(response.body).toMatchObject(expectedResponse);
    });

    test('does not retrieve deleted threads', async () => {
      await knex('Threads').update({ deleted_at: new Date() }).where('id', thread.id);
      await apiGet(`/threads/${thread.id}`).expect(httpStatus.FORBIDDEN);
    });

    test('retrieves deleted threads if the user has the correct permission', async () => {
      await knex('Threads').update({ deleted_at: new Date() }).where('id', thread.id);
      await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-view-deleted-threads`]);
      await apiGet(`/threads/${thread.id}`, user.id).expect(httpStatus.OK);
    });

    test('retrieves a thread and includes last edited user in edited post', async () => {
      await createPostEdit({ userId: user.id, postId: post.id });

      const expectedResponse = {
        id: thread.id,
        title: thread.title,
        iconId: 1,
        subforumId: subforum.id,
        deleted: false,
        locked: false,
        pinned: false,
        lastPost: {
          id: post.id,
          threadId: thread.id,
          page: 1,
          content: '',
          user: {
            id: user.id,
          },
          ratings: [],
          bans: [],
          threadPostNumber: 1,
          countryName: null,
          appName: 'knockout.chat',
        },
        backgroundUrl: '',
        backgroundType: '',
        user: {
          id: user.id,
          username: user.username,
          avatarUrl: 'none.webp',
          backgroundUrl: '',
          banned: false,
          isBanned: false,
          title: null,
        },
        postCount: 1,
        recentPostCount: 0,
        subforum: {
          id: subforum.id,
        },
        tags: [],
        totalPosts: 1,
        currentPage: 1,
        posts: [
          {
            id: post.id,
            threadId: thread.id,
            page: 1,
            content: post.content,
            user: {
              id: user.id,
            },
            lastEditedUser: {
              id: user.id,
              username: user.username,
              avatarUrl: 'none.webp',
              backgroundUrl: '',
              banned: false,
              isBanned: false,
              title: null,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 1,
            countryName: null,
            appName: 'knockout.chat',
          },
        ],
      };

      const response = await apiGet(`/threads/${thread.id}`, user.id).expect(httpStatus.OK);

      expect(response.body).toMatchObject(expectedResponse);
    });

    test('retrieves a thread for a logged-out user and includes last edited user in edited post', async () => {
      await createPostEdit({ userId: user.id, postId: post.id });

      const expectedResponse = {
        id: thread.id,
        title: thread.title,
        iconId: 1,
        subforumId: subforum.id,
        deleted: false,
        locked: false,
        pinned: false,
        lastPost: {
          id: post.id,
          threadId: thread.id,
          page: 1,
          content: '',
          user: {
            id: user.id,
          },
          ratings: [],
          bans: [],
          threadPostNumber: 1,
          countryName: null,
          appName: 'knockout.chat',
        },
        backgroundUrl: '',
        backgroundType: '',
        user: {
          id: user.id,
          username: user.username,
          avatarUrl: 'none.webp',
          backgroundUrl: '',
          banned: false,
          isBanned: false,
          title: null,
        },
        postCount: 1,
        subforum: {
          id: subforum.id,
        },
        tags: [],
        totalPosts: 1,
        currentPage: 1,
        posts: [
          {
            id: post.id,
            threadId: thread.id,
            page: 1,
            content: post.content,
            user: {
              id: user.id,
            },
            lastEditedUser: {
              id: user.id,
              username: user.username,
              avatarUrl: 'none.webp',
              backgroundUrl: '',
              banned: false,
              isBanned: false,
              title: null,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 1,
            countryName: null,
            appName: 'knockout.chat',
          },
        ],
      };

      const response = await apiGet(`/threads/${thread.id}`).expect(httpStatus.OK);

      expect(response.body).toMatchObject(expectedResponse);
    });

    test('does not retrieve a thread with an invalid id', async () => {
      await apiGet('/threads/undefined`, user.id').expect(httpStatus.NOT_FOUND);
    });
  });

  describe('latest and popular endpoints', () => {
    test('GET /latest retrieves latest threads', async () => {
      await apiGet('/threads/latest').expect(httpStatus.OK);
    });

    test('GET /popular retrieves popular threads', async () => {
      await apiGet('/threads/popular', user.id).expect(httpStatus.OK);
    });
  });

  describe('PUT', () => {
    describe('/:id', () => {
      test('blocks logged out users', async () => {
        const data: UpdateThreadRequest = {
          title: 'New title',
        };

        await apiPut(`/threads/${thread.id}`, data).expect(httpStatus.UNAUTHORIZED);
      });

      describe('the thread creator', () => {
        test('can update the title of a thread with the create permission', async () => {
          await addPermissionCodesToRole(role.id, [`subforum-${subforum.id}-thread-create`]);

          const data: UpdateThreadRequest = {
            title: 'New title',
          };

          const response = await apiPut(`/threads/${thread.id}`, data, user.id).expect(
            httpStatus.OK
          );

          expect(response.body.title).toEqual('New title');
        });

        test('cannot update a locked thread', async () => {
          await knex('Threads').update({ locked: true }).where('id', thread.id);

          const data: UpdateThreadRequest = {
            title: 'New title',
          };

          await apiPut(`/threads/${thread.id}`, data, user.id).expect(httpStatus.FORBIDDEN);
        });

        test('cannot move the thread to a different subforum', async () => {
          const data: UpdateThreadRequest = {
            subforum_id: 2,
          };

          await apiPut(`/threads/${thread.id}`, data, user.id).expect(httpStatus.FORBIDDEN);
        });

        test('cannot update the background of the thread', async () => {
          const data: UpdateThreadRequest = {
            background_url: 'none.webp',
            background_type: 'cover',
          };

          await apiPut(`/threads/${thread.id}`, data, user.id).expect(httpStatus.FORBIDDEN);
        });
      });
    });

    describe('PUT /:id/tags', () => {
      test('blocks logged out users', async () => {
        const data: UpdateThreadTagsRequest = {
          tag_ids: [1, 2],
        };

        await apiPut(`/threads/${thread.id}/tags`, data).expect(httpStatus.UNAUTHORIZED);
      });
    });

    describe('GET /:id/metadata', () => {
      beforeEach(async () => {
        const guestRoleId = await getGuestUserRoleId();
        await addPermissionCodesToRole(guestRoleId, [`subforum-${subforum.id}-view`]);
        redis.del(`rolePermission-${guestRoleId}`);
      });

      test('retrieves the metadata for a thread', async () => {
        let expectedResponse: ThreadMetadata;

        const response = await apiGet(`/threads/${thread.id}/metadata`)
          .expect((res: { body: ThreadMetadata }) => {
            expectedResponse = {
              title: thread.title,
              iconId: 1,
              subforumName: subforum.name,
              username: user.username!,
              createdAt: res.body.createdAt,
              updatedAt: res.body.updatedAt,
            };
          })
          .expect(httpStatus.OK);

        expect(response.body).toMatchObject(expectedResponse!);
      });
    });
  });
});
