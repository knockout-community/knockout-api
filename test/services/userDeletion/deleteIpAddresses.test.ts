import IpAddress from '../../../src/models/ipAddress';
import User from '../../../src/models/user';
import deleteIpAddresses from '../../../src/services/userDeletion/deleteIpAddresses';
import { createUser, createIpAddress } from '../../factories';

describe('deleteIpAddresss', () => {
  let user: User;

  beforeEach(async () => {
    user = await createUser();
    await createIpAddress({ user_id: user.id });
  });

  test('the users alerts are deleted', async () => {
    const alertsCountBefore = (
      await IpAddress.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(alertsCountBefore).toEqual(1);

    await deleteIpAddresses(user.id);

    const alertsCountAfter = (
      await IpAddress.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(alertsCountAfter).toEqual(0);
  });
});
