import ReadThread from '../../../src/models/readThread';
import User from '../../../src/models/user';
import deleteReadThreads from '../../../src/services/userDeletion/deleteReadThreads';
import { createUser, createSubforum, createThread, createReadThread } from '../../factories';

describe('deleteReadThreads', () => {
  let user: User;

  beforeEach(async () => {
    user = await createUser();
    const subforum = await createSubforum();
    const thread = await createThread({ subforum_id: subforum.id, user_id: user.id });
    await createReadThread({ user_id: user.id, thread_id: thread.id });
  });

  test('the users read threads are deleted', async () => {
    const rtCountBefore = (
      await ReadThread.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(rtCountBefore).toEqual(1);

    await deleteReadThreads(user.id);

    const rtCountAfter = (
      await ReadThread.findAll({
        where: { user_id: user.id },
      })
    ).length;

    expect(rtCountAfter).toEqual(0);
  });
});
