import { postMentionFinder, postQuoteFinder } from '../../src/helpers/postMentionFinder';

describe('postMentionFinder', () => {
  test('returns mention user IDs in a post with direct mentions', () => {
    const content = 'hello @<123;user>';
    const mentions = postMentionFinder(content);
    expect(mentions[0]).toEqual(123);
  });

  test('does not return user IDs when mention is in a quote', () => {
    const content = 'hello @<789;user2>[quote mentionsUser="123"]@<456;user1> hi[/quote]';
    const mentions = postMentionFinder(content);
    expect(mentions.length).toEqual(1);
    expect(mentions[0]).toEqual(789);
  });

  test('returns a unique list of user IDs', () => {
    const content = 'hello @<123;user> @<123;user>';
    const mentions = postMentionFinder(content);
    expect(mentions.length).toEqual(1);
    expect(mentions[0]).toEqual(123);
  });
});

describe('postQuoteFinder', () => {
  test('returns quote user IDs in a post with quotes', () => {
    const content = '[quote mentionsUser="123"]nice[/quote]no';
    const mentions = postQuoteFinder(content);
    expect(mentions[0]).toEqual(123);
  });

  test('returns a unique list of user IDs', () => {
    const content = '[quote mentionsUser="123"]nice[/quote][quote mentionsUser="123"]nice[/quote]';
    const mentions = postQuoteFinder(content);
    expect(mentions.length).toEqual(1);
    expect(mentions[0]).toEqual(123);
  });
});
