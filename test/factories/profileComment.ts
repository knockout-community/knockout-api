import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import ProfileComment from '../../src/models/profileComment';

interface CreateProfileCommentData {
  user_profile: number;
  author: number;
  content?: string;
}

const data = async (props: CreateProfileCommentData) => {
  const defaultData: CreationAttributes<ProfileComment> = {
    user_profile: props.user_profile,
    author: props.author,
    content: faker.lorem.sentence(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateProfileCommentData) => ProfileComment.create(await data(props));
