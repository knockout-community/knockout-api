import { CreationAttributes } from 'sequelize';
import IpAddress from '../../src/models/ipAddress';

interface CreateIpAddressData {
  user_id: number;
  ip_address?: string;
  post_id?: number;
  createdAt?: Date;
}

const data = async (props: CreateIpAddressData) => {
  const defaultData: CreationAttributes<IpAddress> = {
    user_id: props.user_id,
    ip_address: props.ip_address || '127.0.0.1',
    post_id: props.post_id,
    created_at: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateIpAddressData) => IpAddress.create(await data(props));
