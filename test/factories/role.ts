import faker from 'faker';
import { v4 as uuidv4 } from 'uuid';
import { CreationAttributes } from 'sequelize';
import Role from '../../src/models/role';

interface CreateRoleData {
  code?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateRoleData) => {
  const defaultData: CreationAttributes<Role> = {
    code: `${faker.hacker.verb()}-${uuidv4()}`,
    description: faker.hacker.noun(),
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateRoleData = {}) => Role.create(await data(props));
