import { CreationAttributes } from 'sequelize';
import Rating from '../../src/models/rating';

interface CreateRatingData {
  user_id: number;
  post_id: number;
  rating_id?: number;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateRatingData) => {
  const defaultData: CreationAttributes<Rating> = {
    user_id: props.user_id,
    post_id: props.post_id,
    rating_id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateRatingData) => Rating.create(await data(props));
