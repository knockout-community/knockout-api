import IspBan from '../../src/models/ispBan';

interface CreateIspBanData {
  netname: string;
  asn: string;
  createdBy: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateIspBanData) => {
  const defaultData: Partial<CreateIspBanData> = {
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateIspBanData) => IspBan.create(await data(props));
