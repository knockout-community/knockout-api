import { CreationAttributes } from 'sequelize';
import RolePermission from '../../src/models/rolePermission';

interface CreateRolePermissionData extends CreationAttributes<any> {
  role_id: number;
  permission_id: number;
}

const data = async (props: CreateRolePermissionData) => props;

export default async (props: CreateRolePermissionData) => RolePermission.create(await data(props));
