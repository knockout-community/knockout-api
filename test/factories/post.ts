import faker from 'faker';
import { CreationAttributes } from 'sequelize';
import Post from '../../src/models/post';

interface CreatePostData {
  user_id: number;
  thread_id: number;
  content?: string;
  app_name?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = (props: CreatePostData) => {
  const defaultData: CreationAttributes<Post> = {
    content: faker.lorem.sentence(),
    user_id: props.user_id,
    thread_id: props.thread_id,
    app_name: 'knockout.chat',
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreatePostData) => Post.create(data(props));
