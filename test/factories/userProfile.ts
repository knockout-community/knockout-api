import faker from 'faker';
import UserProfile from '../../src/models/userProfile';

interface CreateUserProfileData {
  user_id: number;
  heading_text?: string;
  personal_site?: string;
  background_url?: string;
  background_type?: string;
  header?: string;
  steam?: string;
  discord?: string;
  github?: string;
  youtube?: string;
  twitter?: string;
  fediverse?: string;
  twitch?: string;
  gitlab?: string;
  tumblr?: string;
  bluesky?: string;
  disable_comments?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

const data = async (props: CreateUserProfileData) => {
  const defaultData: CreateUserProfileData = {
    user_id: props.user_id,
    heading_text: faker.lorem.sentence(),
    personal_site: faker.internet.url(),
    background_url: '',
    background_type: '',
    header: faker.lorem.word(),
    steam: faker.lorem.word(),
    discord: faker.lorem.word(),
    github: faker.lorem.word(),
    youtube: faker.lorem.word(),
    twitter: faker.lorem.word(),
    fediverse: `${faker.lorem.word()}@${faker.lorem.word()}`,
    twitch: faker.lorem.word(),
    gitlab: faker.lorem.word(),
    tumblr: faker.lorem.word(),
    bluesky: faker.lorem.word(),
    disable_comments: false,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateUserProfileData) => UserProfile.create(await data(props));
