import { CreationAttributes } from 'sequelize';
import SiteStat from '../../src/models/siteStat';

interface CreateSiteStatData {
  fromDate?: Date;
  toDate?: Date;
  totalActiveUsers?: number;
  totalBans?: number;
  totalNewUsers?: number;
  totalPosts?: number;
  totalReports?: number;
  totalThreads?: number;
  createdAt?: Date;
}

const data = async (props: CreateSiteStatData) => {
  const defaultData: CreationAttributes<SiteStat> = {
    fromDate: new Date(),
    toDate: new Date(),
    totalActiveUsers: 0,
    totalBans: 0,
    totalNewUsers: 0,
    totalPosts: 0,
    totalReports: 0,
    totalThreads: 0,
    createdAt: new Date(),
  };
  return { ...defaultData, ...props };
};

export default async (props: CreateSiteStatData = {}) => SiteStat.create(await data(props));
