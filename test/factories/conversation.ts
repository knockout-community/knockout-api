import Conversation from '../../src/models/conversation';
import ConversationUser from '../../src/models/conversationUser';

export default async (user_ids: number[]) => {
  const conversation = await Conversation.create();
  await Promise.all(
    user_ids.map((user_id) =>
      ConversationUser.create({
        conversation_id: conversation.id,
        user_id,
      })
    )
  );
  return conversation;
};
