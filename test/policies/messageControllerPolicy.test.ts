import { Response } from 'express';
import { ForbiddenError } from 'routing-controllers';
import { store, update } from '../../src/policies/messageControllerPolicy';
import { getLimitedUserRoleId } from '../../src/helpers/role';
import { addPermissionCodesToRole } from '../helper/testHelper';
import { createRole, createUser, createConversation, createMessage } from '../factories';
import User from '../../src/models/user';

describe('messageControllerPolicy', () => {
  let mockRequest;
  let user: User;
  let role;
  let conversation;
  let message;
  const mockResponse = {} as Response;
  mockResponse.status = jest.fn().mockReturnValue(mockResponse);
  mockResponse.json = jest.fn().mockReturnValue(mockResponse);
  let mockNext;

  beforeEach(async () => {
    role = await createRole();
    user = await createUser({ roleId: role.id });
    mockNext = jest.fn();
  });

  describe('store', () => {
    test('user with message-create permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['message-create']);

      mockRequest = {
        body: {},
        user: {
          id: user.id,
        },
      };

      await store(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user with limited user role is not authorized', async () => {
      const limitedUserRoleId = await getLimitedUserRoleId();
      await user.update({ roleId: limitedUserRoleId });

      mockRequest = {
        body: {},
        user: {
          id: user.id,
        },
      };

      await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    test('user in conversation with message-create permission is authorized', async () => {
      await addPermissionCodesToRole(role.id, ['message-create']);
      conversation = await createConversation([user.id]);

      mockRequest = {
        body: {
          conversationId: conversation.id,
        },
        user: {
          id: user.id,
        },
      };

      await store(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user not in conversation is not authorized', async () => {
      const newUser = await createUser();
      conversation = await createConversation([user.id]);

      mockRequest = {
        body: {
          conversationId: conversation.id,
        },
        user: {
          id: newUser.id,
        },
      };

      await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });

    describe('when receiving user has disabled incoming messages', () => {
      let receivingUser: User;

      beforeEach(async () => {
        await addPermissionCodesToRole(role.id, ['message-create']);
        receivingUser = await createUser({ disableIncomingMessages: true });
      });

      test('user without message-create-override permissions is not authorized', async () => {
        mockRequest = {
          body: { receivingUserId: receivingUser.id },
          user: {
            id: user.id,
          },
        };

        await expect(store(mockRequest, mockResponse, mockNext)).rejects.toThrow(
          new ForbiddenError('Insufficient user permissions.')
        );
      });

      test('user with message-create-override permissions is authorized', async () => {
        await addPermissionCodesToRole(role.id, ['message-create-override']);

        mockRequest = {
          body: { receivingUserId: receivingUser.id },
          user: {
            id: user.id,
          },
        };

        await store(mockRequest, mockResponse, mockNext);
        expect(mockNext).toHaveBeenCalled();
      });
    });
  });

  describe('update', () => {
    test('user in messages conversation is authorized', async () => {
      conversation = await createConversation([user.id]);
      message = await createMessage({ user_id: user.id, conversation_id: conversation.id });

      mockRequest = {
        params: {
          id: message.id,
        },
        user: {
          id: user.id,
        },
      };

      await update(mockRequest, mockResponse, mockNext);
      expect(mockNext).toHaveBeenCalled();
    });

    test('user not in conversation is not authorized', async () => {
      conversation = await createConversation([user.id]);
      const newUser = await createUser();
      message = await createMessage({ user_id: user.id, conversation_id: conversation.id });

      mockRequest = {
        params: {
          id: message.id,
        },
        user: {
          id: newUser.id,
        },
      };

      await expect(update(mockRequest, mockResponse, mockNext)).rejects.toThrow(
        new ForbiddenError('Insufficient user permissions.')
      );
    });
  });
});
