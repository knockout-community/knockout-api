FROM node:20.10.0

WORKDIR /usr/src/server
COPY package.json .
RUN yarn set version berry
COPY .yarn ./.yarn
COPY yarn.lock .yarnrc.yml ./
RUN yarn
COPY . .
RUN mv .env.example .env
CMD sleep 10 && yarn sequelize db:migrate && yarn build && node dist/index.js