'use strict';

const { RoleCode } = require("../src/helpers/permissions");
const {default: RoleRetriever}  = require("../src/retriever/role");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.transaction(async (transaction) => {
    const roleCodes = [
      RoleCode.MODERATOR_IN_TRAINING,
      RoleCode.MODERATOR,
      RoleCode.SUPER_MODERATOR,
      RoleCode.ADMIN,
      RoleCode.BANNED_USER
    ];

    const roleIds = (await queryInterface.sequelize.query(
      `SELECT id FROM Roles WHERE code IN (:codes)`,
      {
        type: Sequelize.QueryTypes.SELECT,
        replacements: { codes: roleCodes }
      }
    ));

      const subforumId = (await queryInterface.sequelize.query(
        `SELECT id FROM Subforums WHERE name = :name LIMIT 1`,
        { type: Sequelize.QueryTypes.SELECT, replacements: { name: 'Whack-Ass Crystal Prison' } }
      )
    )[0].id;
    
    const permissionId = (await queryInterface.sequelize.query(
      `SELECT id FROM Permissions WHERE code = :code`,
      {
        type: Sequelize.QueryTypes.SELECT, 
        replacements: { code: `subforum-${subforumId}-post-rating-create` }
      }
    ))[0].id;

    await queryInterface.bulkDelete('RolePermissions', {
      permission_id: permissionId
    }, { transaction });

    await new RoleRetriever(roleIds.map(roleId => roleId.id)).invalidatePermissionCodes();
  });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.transaction(async (transaction) => {
    const subforumId = (await queryInterface.sequelize.query(
      `SELECT id FROM Subforums WHERE name = 'Whack-Ass Crystal Prison'`,
    ))[0][0].id;

    const roleCodes = [
      RoleCode.MODERATOR_IN_TRAINING,
      RoleCode.MODERATOR,
      RoleCode.SUPER_MODERATOR,
      RoleCode.ADMIN,
      RoleCode.BANNED_USER
    ];

    const roleIds = (await queryInterface.sequelize.query(
      `SELECT id FROM Roles WHERE code IN ('${roleCodes.join("','")}')`,
    ))[0];

    const permissionId = (await queryInterface.sequelize.query(
      `SELECT id FROM Permissions WHERE code = 'subforum-${subforumId}-post-rating-create'`,
    ))[0][0].id;

    await queryInterface.bulkInsert('RolePermissions', roleIds.map(roleId => ({
      role_id: roleId.id,
      permission_id: permissionId
    })), { transaction });

    await new RoleRetriever(roleIds.map(roleId => roleId.id)).invalidatePermissionCodes();
  });
}
}
