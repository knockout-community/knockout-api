'use strict';

import ThreadRetriever from '../src/retriever/thread';
import knex from '../src/services/knex';

export async function up(queryInterface, Sequelize) {
  // merge all threads in 'Facepunch Games' to 'Source'
  const fpGamesSubforumId = (await queryInterface.sequelize.query(
    `SELECT id FROM Subforums WHERE name = :name LIMIT 1`,
    { type: Sequelize.QueryTypes.SELECT, replacements: { name: 'Facepunch Games' } }
  )
  )[0]?.id;

  const sourceSubforumId = (await queryInterface.sequelize.query(
    `SELECT id FROM Subforums WHERE name = :name LIMIT 1`,
    { type: Sequelize.QueryTypes.SELECT, replacements: { name: 'Source' } }
  )
  )[0]?.id;

  if (fpGamesSubforumId && sourceSubforumId) {
    // all threads with subforum_id = 12 update to subforum_id = 14
    const fpThreadIds = (await knex('Threads').select('id').where('subforum_id', fpGamesSubforumId)).map((thread) => thread.id)

    await knex('Threads').whereIn('id', fpThreadIds).update({ subforum_id: sourceSubforumId })

    // invalidate threads
    await new ThreadRetriever(fpThreadIds).invalidate()

    // destroy subforum
    await knex('Subforums').where('id', fpGamesSubforumId).del()
  }
}

export async function down(queryInterface, Sequelize) {
  // noop
}
