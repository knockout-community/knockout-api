'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('CalendarEvents', 'thread_id', {
    allowNull: false,
    type: Sequelize.INTEGER.UNSIGNED,
    references: {
      model: 'Threads',
      key: 'id',
    },
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('CalendarEvents', 'thread_id');
}
