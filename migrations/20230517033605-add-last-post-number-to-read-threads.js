'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    return queryInterface.addColumn('ReadThreads', 'last_post_number', {
      allowNull: true,
      type: Sequelize.INTEGER,
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('ReadThreads', 'last_post_number');
  }
};
