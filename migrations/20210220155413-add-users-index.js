'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addIndex('Users', 
    {
      fields: [
        {
          attribute: 'deleted_at',
          order: 'DESC'
        },
      ]
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeIndex(
      'Users',
      'users_deleted_at'
    );
  }
};
