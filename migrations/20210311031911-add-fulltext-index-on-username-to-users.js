'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      'Users',
      'users_deleted_at'
    );
    await queryInterface.addIndex('Users', 
    {
      fields: [
        {
          attribute: 'username',
          type: 'FULLTEXT',
        },
        {
          attribute: 'deleted_at',
          order: 'DESC'
        },
      ]
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      'Users',
      'users_username_deleted_at'
    );
    return queryInterface.addIndex('Users', 
    {
      fields: [
        {
          attribute: 'deleted_at',
          order: 'DESC'
        },
      ]
    });
  }
};
