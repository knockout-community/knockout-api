'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Threads', 
    {
      type: 'FULLTEXT',
      fields: ['title'],
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      'Threads',
      'threads_title'
    );
  }
};
