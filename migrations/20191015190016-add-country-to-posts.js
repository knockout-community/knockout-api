// @ts-check
'use strict';

/**
 * @param {import('sequelize').QueryInterface} queryInterface 
 * @param {string} subforumName 
 * @param {string} description 
 */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Posts', 'country_name', {
      allowNull: true,
      type: Sequelize.STRING
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Posts', 'country_name');
  }
};
