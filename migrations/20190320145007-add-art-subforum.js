'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Fill table
    await queryInterface.bulkInsert(
      'Subforums',
      [
        {
          name: 'Creativity and Art',
          icon: 'https://img.icons8.com/color/96/000000/paint-bucket.png',
          description: 'Show everyone the cool stuff you make'
        }
      ],
      {}
    );
  },
  down: (queryInterface, Sequelize) => {
  }
};
