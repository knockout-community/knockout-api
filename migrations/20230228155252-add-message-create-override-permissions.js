'use strict';

import knex from '../src/services/knex';
import { RoleCode } from '../src/helpers/permissions';

// Adds the message-create-override permission, which allows users to send
// messages to users who have disabled incoming messages. we apply this permission
// to the moderator roles
export async function up() {
  const newPermissionCode = 'message-create-override';

  // if we've already inserted the permission, bail out
  const permissionsExist = (await knex('Permissions').where('code', newPermissionCode)).length > 0;

  if (permissionsExist) return;

  await knex('Permissions').insert({ code: newPermissionCode });

  const overridePermissionId = (
    await knex('Permissions').select('id').where({ code: newPermissionCode })
  )[0].id;

  const modAndUpRolesCodes = [
    RoleCode.MODERATOR_IN_TRAINING,
    RoleCode.MODERATOR,
    RoleCode.SUPER_MODERATOR,
    RoleCode.ADMIN,
  ];

  const roleIds = await knex('Roles')
    .select('id')
    .whereIn('code', modAndUpRolesCodes)
    .then((roles) => roles.map((role) => role.id));

  roleIds.forEach(async (roleId) =>
    knex('RolePermissions').insert({ role_id: roleId, permission_id: overridePermissionId })
  );
}

export async function down() {
  // noop
}
