'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addIndex('Mentions', ['mentioned_by', 'post_id', 'mentions_user', 'inactive'], { unique: true });
  await queryInterface.removeIndex('Mentions', 'mentions_mentioned_by_post_id_mentions_user_unique')
}
  
export async function down(queryInterface, Sequelize) {
  await queryInterface.addIndex('Mentions', ['mentioned_by', 'post_id', 'mentions_user'], { name: 'mentions_mentioned_by_post_id_mentions_user_unique', unique: true });
  await queryInterface.removeIndex('Mentions', ['mentioned_by', 'post_id', 'mentions_user', 'inactive']);
}

