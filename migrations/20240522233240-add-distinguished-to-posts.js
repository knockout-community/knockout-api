'use strict';
import Post from '../src/models/post';
import User from '../src/models/user';
import initializeAssociations from '../src/models/associations';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Posts', 'distinguished', {
      allowNull: true,
      type: Sequelize.BOOLEAN,
    });
    initializeAssociations();
    const BATCH_SIZE = 10000;
    const posts = await Post.findAll({
      include: {
        model: User,
        as: 'user',
        where: {
          roleId: {
            [Sequelize.Op.in]: [6, 7, 8, 9]
          }
        },
        required: true
      },
    })

    while (posts.length > 0) {
      await Post.update({ distinguished: true }, {
        where: {
          id: {
            [Sequelize.Op.in]: posts.splice(0, BATCH_SIZE).map(post => post.id)
          }
        }
      });
    }
  },
  down: async (queryInterface) => {
    await queryInterface.removeColumn('Posts', 'distinguished');
  },
};
