'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Users', 'role_id', {
      allowNull: true,
      type: Sequelize.BIGINT,
      references: {
        model: 'Roles',
        key: 'id'
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Users', 'role_id');
  }
};
