// @ts-check
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('Mentions', 'thread_id', {
      allowNull: false,
      type: Sequelize.INTEGER.UNSIGNED,
      references: {
        model: 'Threads',
        key: 'id'
      }
    });
    await queryInterface.addColumn('Mentions', 'page', {
      allowNull: false,
      type: Sequelize.INTEGER,
    });
    await queryInterface.addColumn('Mentions', 'mentioned_by', {
      allowNull: false,
      type: Sequelize.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Mentions', 'thread_id');
    await queryInterface.removeColumn('Mentions', 'page');
    return queryInterface.removeColumn('Mentions', 'mentioned_by');
  }
};
