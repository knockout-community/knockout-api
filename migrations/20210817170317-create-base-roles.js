'use strict';

const ROLES = [
  {
    code: 'guest',
    description: 'A non-logged in user with the least forum access',
  },
  {
    code: 'banned-user',
    description: 'A banned user with restricted forum access'
  },
  {
    code: 'limited-user',
    description: 'A limited user with limited forum access'
  },
  {
    code: 'basic-user',
    description: 'A basic user with normal forum access'
  },
  {
    code: 'gold-user',
    description: 'An elevated user with special forum priveleges'
  },
  {
    code: 'moderator-in-training',
    description: 'An elevated user with moderator in training priveleges'
  },
  {
    code: 'moderator',
    description: 'An elevated user with moderator priveleges'
  },
  {
    code: 'super-moderator',
    description: 'An elevated user with super-moderator priveleges'
  },
  {
    code: 'admin',
    description: 'An elevated user with administrative priveleges'
  }
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // do not run if we already have roles in the DB
    const roleCount = await queryInterface.sequelize.query(
      'SELECT COUNT(*) FROM Roles',
      { type: Sequelize.QueryTypes.SELECT }
    ).then((result) => result[0].count);

    if (roleCount > 0) {
      return true;
    }

    return queryInterface.bulkInsert('Roles', ROLES);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Roles', {});
  }
};
