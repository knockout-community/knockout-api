'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Users', 'use_bio_for_title', {
    allowNull: true,
    type: Sequelize.BOOLEAN,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Users', 'use_bio_for_title');
}
