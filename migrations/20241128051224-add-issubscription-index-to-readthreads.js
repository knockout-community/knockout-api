'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('ReadThreads', ['user_id', 'is_subscription']);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('ReadThreads', ['user_id', 'is_subscription']);
  }
};
