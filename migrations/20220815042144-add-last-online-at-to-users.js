'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addColumn('Users', 'last_online_at', {
    allowNull: true,
    type: Sequelize.DATE,
  });
}

export async function down(queryInterface, Sequelize) {
  return queryInterface.removeColumn('Users', 'last_online_at');
}
