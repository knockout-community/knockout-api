'use strict';

import knex from '../src/services/knex';

// Populates all threads with a `postCount` attribute
export async function up(queryInterface, Sequelize) {
  const updateBatchSize = 1000;

  const threadCount = (await knex('Threads').count('id as count').where('post_count', null))[0].count

  let updatedRows = 0
  while (updatedRows < threadCount) {
    await knex.raw(`
    UPDATE Threads th SET post_count = (SELECT COUNT(*) from Posts p WHERE p.thread_id = th.id)
    WHERE th.post_count IS NULL
    LIMIT ${updateBatchSize}`)
    updatedRows += updateBatchSize
  }
}

export async function down(queryInterface, Sequelize) {
  // noop
}
