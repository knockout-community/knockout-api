'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addIndex('Threads', ['created_at']);
}

export async function down(queryInterface, Sequelize) {
  await queryInterface.removeIndex('Threads', 'threads_created_at');
}
