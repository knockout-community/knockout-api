'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Ratings', {
        post_id: {
          type: Sequelize.INTEGER.UNSIGNED          
        },
        user_id: {
          type: Sequelize.INTEGER,
        },
        rating_id: {
          allowNull: false,
          type: Sequelize.INTEGER.UNSIGNED
        },
        created_at: {
          allowNull: true,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('NOW')
        },
        updated_at: {
          allowNull: true,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('NOW')
        }
      })
      .then(() => {
        return queryInterface.sequelize.query(
          'ALTER TABLE `Ratings` ADD CONSTRAINT `ratingkeys` PRIMARY KEY (`post_id`, `user_id`)'
        ).then(()=> {
          'ALTER TABLE `Ratings` ADD CONSTRAINT `Ratings_ibfk_1` REFERENCES Users (`user_id`)'
        }).then(()=> {
          'ALTER TABLE `Posts` ADD CONSTRAINT `Ratings_ibfk_2` REFERENCES Posts (`post_id`)'
        });
      });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Ratings');
  }
};
