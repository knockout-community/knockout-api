'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('IpAddresses', ['ip_address', 'created_at'], { concurrently: true })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('IpAddresses', ['ip_address', 'created_at']);
  }
};
