'use strict';

const { RoleCode } = require("../src/helpers/permissions");

// Removes Meta access from Guest, Banned, and Limited Users

const SUBFORUM_PERMISSIONS_TO_REMOVE = [
  'subforum-9-view',
  'subforum-9-thread-create',
  'subforum-9-post-create',
  'subforum-9-post-report-create',
  'subforum-9-post-rating-create'
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const permissionIds = await queryInterface.sequelize.query(
      `SELECT id FROM Permissions WHERE code IN (:permissionCodes)`,
      {
        type: Sequelize.QueryTypes.SELECT,
        replacements: { permissionCodes: SUBFORUM_PERMISSIONS_TO_REMOVE },
      }
    ).then((records) => records.map((record) => record.id));

    const roleIds = await queryInterface.sequelize.query(
      `SELECT id FROM Roles WHERE code IN (:roleCodes)`,
      {
        type: Sequelize.QueryTypes.SELECT,
        replacements: { roleCodes: [RoleCode.LIMITED_USER, RoleCode.BANNED_USER, RoleCode.GUEST] },
      }
    ).then((records) => records.map((record) => record.id));

    return queryInterface.sequelize.query(
      `DELETE FROM RolePermissions WHERE role_id IN (:roleIds) AND permission_id IN (:permissionIds)`,
      {
        type: Sequelize.QueryTypes.DELETE,
        replacements: { roleIds, permissionIds }
      }
    )
  },

  down: async (queryInterface, Sequelize) => {
    // noop
  }
};
