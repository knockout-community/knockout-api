'use strict';

import knex from '../src/services/knex';
import ReadThread from '../src/models/readThread';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const BATCH_SIZE = 10000;
    const alerts = await knex('Alerts').select('user_id', 'thread_id', 
    'last_post_number as lastPostNumber', 'last_seen as lastSeen', 
    'created_at as createdAt', 'updated_at as updatedAt');
    while (alerts.length > 0) {
      await ReadThread.bulkCreate(alerts.splice(0, BATCH_SIZE).map(alert => ({...alert, isSubscription: true})), {
        updateOnDuplicate: ['lastPostNumber', 'lastSeen', 'createdAt', 'updatedAt', 'isSubscription']
      });
    }
  },

  down: async (queryInterface, Sequelize) => {
    // noop
  }
};
