'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Notifications', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      type: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      data_id: {
        allowNull: false,
        type: Sequelize.BIGINT,
      },
      user_id: {
        allowNull: false,
        type: Sequelize.BIGINT,
      },
      read: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      }
    });
    await queryInterface.addIndex('Notifications',
      {
        fields: [
          {
            attribute: 'created_at',
            order: 'DESC'
          },
        ]
      });

    return queryInterface.addConstraint('Notifications', {
      fields: ['type', 'data_id', 'user_id'],
      type: 'unique',
      name: 'unique_notification'
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('Notifications', 'unique_notification');
    await queryInterface.removeIndex(
      'Notifications',
      'notifications_created_at'
    );
    return queryInterface.dropTable('Notifications');
  }
};
