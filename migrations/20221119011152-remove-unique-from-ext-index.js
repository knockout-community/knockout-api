'use strict';

export async function up(queryInterface, Sequelize) {
  await queryInterface.addIndex('ExternalAccounts', {
    fields: ['provider', 'user_id'],
    name: 'ExternalAccounts_provider_user_id'
  })
  await queryInterface.removeConstraint('ExternalAccounts', 'ExternalAccounts_unique_provider_entry_per_user')
}

export async function down(queryInterface, Sequelize) {
  queryInterface.removeinex('ExternalAccounts', 'ExternalAccounts_provider_user_id')
}
