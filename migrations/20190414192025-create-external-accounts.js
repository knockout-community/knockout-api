"use strict";

module.exports = {
  /**
   * @param {import("sequelize").QueryInterface} queryInterface
   * @param {import("sequelize").SequelizeStatic} Sequelize
   */
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("ExternalAccounts", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      provider: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "Users",
          key: "id"
        }
      },
      external_id: {
        allowNull: false,
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      }
    });
    return queryInterface.addConstraint("ExternalAccounts", {
        fields: [ "provider", "user_id" ],
        type: "unique",
        name: "ExternalAccounts_unique_provider_entry_per_user"
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("ExternalAccounts");
  }
};
