'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('RolePermissions', {
      role_id: {
        allowNull: false,
        type: Sequelize.BIGINT,
        references: {
          model: "Roles",
          key: "id"
        }
      },
      permission_id: {
        allowNull: false,
        type: Sequelize.BIGINT,
        references: {
          model: "Permissions",
          key: "id"
        }
      }
    }).then(() => {
      return queryInterface.sequelize.query(
        'ALTER TABLE `RolePermissions` ADD CONSTRAINT `rolepermissions_pk` PRIMARY KEY (`role_id`, `permission_id`)'
      )
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('RolePermissions');
  }
};
