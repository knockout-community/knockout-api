'use strict';

import knex from '../src/services/knex';

export async function up(queryInterface, Sequelize) {
  await knex.schema.alterTable('Users', function (table) {
    table.string('avatar_url').defaultTo('none.webp').alter();
  });
}
export async function down(queryInterface, Sequelize) {
  await knex.schema.alterTable('Users', function (table) {
    table.string('avatar_url').defaultTo('').alter();
  });
}
