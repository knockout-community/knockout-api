# knockout-api

## Express API for the Knockout Forums

This is the API for the Knockout.chat Forums.

It's written in Typescript, uses Express to serve web requests, MariaDB for data storage, and Redis for in-memory storage.

For the official frontend, see the [Knockout Web Client.](https://gitlab.com/knockout-community/knockout-front/)

## Dependencies

- Node 20.10.0
- yarn
- Docker
- Docker Compose

## Local Development

1. Run a `yarn install`
1. Create a `.env` file in the root directory with the contents of `.env.example`
1. Run `docker-compose up -d`
1. Run `yarn sequelize db:migrate` to create the tables
1. Run `yarn sequelize db:seed:all` to create sample data
1. Run `yarn start`
1. Open `http://localhost:3000/thread` on your browser to test that the API is working

### Logging in with OAuth Locally

**Google**

<img style="margin: auto; width: 100%;" src="https://cdn.knockout.chat/assets/ko-readme-google-oauth.png" alt="Google OAuth configuration">

1. Set up your [OAuth2 Settings](https://support.google.com/cloud/answer/6158849?hl=en) for
a local GCP project. The configuration should look similar to the one above.
1. Set the `GOOGLE_CLIENT_ID` and `GOOGLE_CLIENT_SECRET` values in your top-level `.env` file to the corresponding values in your configuration.
1. Reboot your server, and you should now be able to login via Google.


## Deployment

Deployments are handled automatically via Gitlab CI/CD.

When the `master` branch is updated, a script is ran on each web server to deploy the newest API.

# Contributing

Branches should be based off of the `qa` branch.
Once you've written your code and pushed your branch, you can create a Merge Request.
Once type-checking and tests have completed on your branch, and at least 1 maintainer
has approved your branch, it can be merged into the `qa` branch. Your fix or feature
will then be included in the next release (when `qa` is merged into `master`).

## Adding new routes

All new routes should be created in `src/v2`. New routes should have unit test coverage.

# License

See LICENSE file.
