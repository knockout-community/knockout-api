'use strict';

import { lorem, address } from 'faker';

const THREAD_POST_COUNT_MAX = 500;

const randomInt = (max) => {
  return Math.floor(Math.random() * (max - 1) + 1);
}

const addHours = (date, hour) => {
  const newDate = new Date(date);
  newDate.setTime(date.getTime() + (hour*60*60*1000));
  return newDate;
}

const randomPost = (content, userId, threadId, createdAt, threadPostNumber) => {
  return {
    content: content,
    user_id: userId,
    thread_id: threadId,
    created_at: createdAt,
    updated_at: createdAt,
    country_name: address.country(),
    app_name: 'knockout.chat',
    thread_post_number: threadPostNumber
  }
}

const insertPosts = async (queryInterface, threads, userIds) => {
  const now = new Date();
  // create posts in random threads by random users
  let i, j, temparr, chunkSize = 250;
  for (i = 0, j = threads.length; i < j; i+=chunkSize) {
    temparr = threads.slice(i, i+chunkSize);

    const posts = temparr.map((thread) => {
      const postCount = randomInt(THREAD_POST_COUNT_MAX);
      const threadId = thread.id;
      const threadCreatedAt = thread.createdAt;
      let thread_post_number = 1;
      let post_created_at = new Date(threadCreatedAt);

      return Array.from({ length: postCount }).map(() => {
        const userId = userIds[randomInt(userIds.length)];
        const post = randomPost(
          lorem.paragraph(randomInt(10)),
          userId,
          threadId, 
          post_created_at, 
          thread_post_number++
        )

        // increment post_created_at by an hour
        post_created_at = addHours(post_created_at, 1);
        
        // clamp post created_at to be at most now
        if (post_created_at > now) {
          post_created_at = new Date();
        }

        return post;
      })
    })

    await queryInterface.bulkInsert('Posts', [].concat(...posts));
  }
  return true;
}

export async function up(queryInterface, Sequelize) {
  const threads = (await queryInterface.sequelize.query(
    `SELECT id, created_at FROM Threads`,
    { type: Sequelize.QueryTypes.SELECT }
  )).map((record) => { 
    return {
      id: record.id, 
      createdAt: record.created_at
    }
  });

  const userIds = (await queryInterface.sequelize.query(
    `SELECT id FROM Users`,
    { type: Sequelize.QueryTypes.SELECT }
  )).map((record) => record.id);

  return insertPosts(queryInterface, threads, userIds)
}
export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('Posts', null, {});
}
