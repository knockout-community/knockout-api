import { internet } from 'faker';
import {
  getAdminRoleId,
  getBasicUserRoleId,
  getModeratorRoleId,
  getPaidGoldUserRoleId,
} from '../src/helpers/role';

const STANDARD_USER_COUNT = 5000;
const GOLD_MEMBER_USER_COUNT = 5000;
const MOD_USER_COUNT = 5;
const ADMIN_COUNT = 2;

const userAttributes = (username, email, role_id) => ({
  username,
  email,
  role_id,
  avatar_url: '',
  background_url: '',
  external_type: 'Google',
  external_id: 'GoogleID',
  created_at: new Date(),
  updated_at: new Date(),
});
const basicUserId = getBasicUserRoleId();
const adminUserId = getAdminRoleId();
const moderatorUserId = getModeratorRoleId();
const paidGoldUserId = getPaidGoldUserRoleId();

const standardUsers = Array.from({ length: STANDARD_USER_COUNT }).map(async () =>
  userAttributes(internet.userName(), internet.email(), await basicUserId)
);

const goldMemberUsers = Array.from({ length: GOLD_MEMBER_USER_COUNT }).map(async () =>
  userAttributes(internet.userName(), internet.email(), await paidGoldUserId)
);

const moderatorUsers = Array.from({ length: MOD_USER_COUNT }).map(async () =>
  userAttributes(internet.userName(), internet.email(), await moderatorUserId)
);

const adminUsers = Array.from({ length: ADMIN_COUNT }).map(async () =>
  userAttributes(internet.userName(), internet.email(), await adminUserId)
);

const randomUserArray = [].concat.apply(
  [],
  [standardUsers, goldMemberUsers, moderatorUsers, adminUsers]
);

export async function up(queryInterface) {
  return queryInterface.bulkInsert('Users', await Promise.all(randomUserArray));
}
export function down(queryInterface) {
  return queryInterface.bulkDelete('Users', null, {});
}
